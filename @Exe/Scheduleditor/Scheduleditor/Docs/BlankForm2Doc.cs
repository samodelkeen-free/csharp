﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using Scheduleditor.Lib.Config;
using Sfo.Sys.Doc;

#endregion

namespace Scheduleditor.Docs
{
  #region Class CBlankForm2Doc

  /// <summary>
  /// Документ формы
  /// </summary>
  public class CBlankForm2Doc: CDoc
  {
    #region Variable declarations

    /// <summary>
    /// Конфигуратор
    /// </summary>
    private readonly CScheduleditorConfig m_pAppConfig;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    public CBlankForm2Doc(CScheduleditorConfig pAppConfig)
    {
      m_pAppConfig = pAppConfig;
    }

    #endregion
    
    #region Public methods

    #endregion

    #region Public properties

    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CScheduleditorConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }

    #endregion

    #region CDoc methods

    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion

  }

  #endregion
}