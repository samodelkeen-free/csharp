﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System.Windows.Forms;
using Scheduleditor.Lib.Config;
using Scheduleditor.Lib;
using Sfo.Sys;
using Sfo.Sys.Doc;
using Sfo.Sys.Log;
using Sfo.Sys.Service;

#endregion

namespace Scheduleditor.Docs
{
  #region CMainFormDoc class
  /// <summary>
  /// Документ главной формы
  /// </summary>
  public class CMainFormDoc : CDoc
  {
    #region CMainFormDoc variable declarations
    /// <summary>
    /// Конфигуратор
    /// </summary>
    private CScheduleditorConfig m_pAppConfig;
    /// <summary>
    /// Сервис
    /// </summary>
    private CServiceItem m_pServiceItem;
    /// <summary>
    /// Документ базы данных
    /// </summary>
    private CDbDoc m_pDbDoc;

    #endregion

    #region Public constants
    /// <summary>
    /// Адрес продукта на сайте разработчика
    /// </summary>
    public string ProductUrl = "http://www.aistkem.ru";

    #endregion

    #region CMainFormDoc constructors
    /// <summary>
    /// Конструктор по заданным форме и имени
    /// </summary>
    /// <param name="pMainForm"></param>
    /// <param name="sInstanceName"></param>
    public CMainFormDoc(Form pMainForm, string sInstanceName)
    {
      //throw new CException("Тестируем ошибку создания документа формы");
      m_pAppConfig = new CScheduleditorConfig(pMainForm, sInstanceName);
      m_pLogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
      m_pDbDoc = new CDbDoc(m_pAppConfig, LogWriter);
      m_pServiceItem = m_pAppConfig.ServiceItem;
      InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(new System.Globalization.CultureInfo("ru-RU"));
      //InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(new System.Globalization.CultureInfo("en-US"));
    }

    #endregion

    #region CDoc methods
    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
      if (pUserData != null) {
        throw new CException("Параметр для данного класса ({0}) должен быть задан в конструкторе",
          this.GetType());
      }
      m_pDbDoc.Open();
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
      m_pDbDoc.Close();
    }

    /// <summary>
    /// Сохранение настроек
    /// </summary>
    public void Save()
    {
      m_pAppConfig.CheckParameters();
      m_pAppConfig.SaveAppParameters();
    }

    #endregion

    #region CMainFormDoc public/internal methods
    ///<summary>
    /// Проверяет, установлен ли сервис?
    ///</summary>
    ///<returns></returns>
    public bool IsServiceInstalled()
    {
      return m_pServiceItem.IsServiceInstalled();
    }

    ///<summary>
    /// Проверяет, был ли запуск сервиса?
    ///</summary>
    ///<returns></returns>
    public bool IsStarted()
    {
      return IsServiceInstalled() ? m_pServiceItem.IsServiceStarted(): false;
    }
 
    /// <summary>
    /// Осуществляет отладочную запись в лог-файл
    /// </summary>
    /// <param name="sMessage">Текст сообщения</param>
    /// <param name="arParams">Список параметров</param>
    internal void WriteLog(string sMessage, params object[] arParams)
    {
      h_writeLog(sMessage, arParams);
    }

    #endregion

    #region CMainFormDoc public properties
    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CScheduleditorConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }
    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }
    ///<summary>
    /// Получает сервис
    ///</summary>
    public CServiceItem Service
    {
      get
      {
        return m_pServiceItem;
      }
    }

    #endregion

    #region CMainFormDoc private methods

    private void h_writeLog(string sMessage, params object[] arParams)
    {
      m_pLogWriter.Write(ELogType.Debug, m_pAppConfig.InstanceName, sMessage, arParams);
    }

    #endregion

  }

  #endregion
}
