﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System;
using Scheduleditor.Lib;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Config;
using Sfo.Sys.Doc;
using Sfo.Sys.Log;

#endregion

namespace Scheduleditor.Docs
{
  #region Class CTaskEditFormDoc

  /// <summary>
  /// Документ формы
  /// </summary>
  public class CTaskEditFormDoc : CDoc
  {
    #region Variable declarations

    /// <summary>
    /// Конфигуратор
    /// </summary>
    private readonly CScheduleditorConfig m_pAppConfig;
    /// <summary>
    /// Документ базы данных
    /// </summary>
    private readonly CDbDoc m_pDbDoc;
    /// <summary>
    /// Объект описания задачи Cron
    /// </summary>
    private readonly CCronTask m_pCronTask;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pTaskIdentifier">Идентификатор задачи</param>
    public CTaskEditFormDoc(CScheduleditorConfig pAppConfig, object pTaskIdentifier)
    {
      m_pAppConfig = pAppConfig;
      LogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
      m_pDbDoc = new CDbDoc(m_pAppConfig, LogWriter);

      if (pTaskIdentifier != null) {
        string sMessage;
        m_pCronTask = m_pDbDoc.GetTask(pTaskIdentifier, out sMessage);
        if (m_pCronTask == null) {
          throw new Exception(sMessage);
        }
      }
      else
        m_pCronTask = m_pDbDoc.NewTask();
    }

    #endregion
    
    #region Public methods

    /// <summary>
    /// Удаление всех одиночных временных меток в поле Cron
    /// </summary>
    /// <param name="iField">Номер поля Cron</param>
    internal void RemoveAlone(int iField)
    {
      CCronFieldInterval pFound;
      while ((pFound = m_pCronTask.Period[iField].Find(delegate(CCronFieldInterval pInterval) { return pInterval.End == null; })) != null) {
        m_pCronTask.Period[iField].Remove(pFound);
      }
    }

    /// <summary>
    /// Удаление всех интервалов в поле Cron
    /// </summary>
    /// <param name="iField">Номер поля Cron</param>
    internal void RemoveInterval(int iField)
    {
      CCronFieldInterval pFound;
      while ((pFound = m_pCronTask.Period[iField].Find(delegate(CCronFieldInterval pInterval) { return pInterval.End != null; })) != null) {
        m_pCronTask.Period[iField].Remove(pFound);
      }
    }

    /// <summary>
    /// Сохраняет изменения задачи в Хранилище
    /// </summary>
    /// <param name="sMessage">Сообщение о результатах сохранения</param>
    /// <returns>Флаг успешности операции сохранения</returns>
    internal bool Save(out string sMessage)
    {
      bool bSave = m_pDbDoc.SaveTask(out sMessage);
      if (bSave) {
        m_pCronTask.Modified = false;
        sMessage = string.Format("Изменения успешно сохранены.");
      }
      return bSave;
    }

    #endregion

    #region Public properties

    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CScheduleditorConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }

    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }

    /// <summary>
    /// Получает объект редактируемой задачи
    /// </summary>
    public CCronTask CronTask
    {
      get
      {
        return m_pCronTask;
      }
    }

    #endregion

    #region CDoc methods

    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion

  }

  #endregion
}