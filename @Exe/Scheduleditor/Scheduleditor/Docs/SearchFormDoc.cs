﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System.Collections.Generic;
using Scheduleditor.Lib;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Config;
using Sfo.Sys.Doc;
using Sfo.Sys.Log;

#endregion

namespace Scheduleditor.Docs
{
  #region CSearchFormDoc class
  /// <summary>
  /// Документ формы
  /// </summary>
  public class CSearchFormDoc: CDoc
  {
    #region Variable declarations
    /// <summary>
    /// Конфигуратор
    /// </summary>
    private readonly CScheduleditorConfig m_pAppConfig;
    /// <summary>
    /// Документ базы данных
    /// </summary>
    private readonly CDbDoc m_pDbDoc;
    /// <summary>
    /// Текущее значение фильтра выборки из Хранилища
    /// </summary>
    private CTaskFilter m_pTaskFilter = null;
    /// <summary>
    /// Список найденных записей типа CSearchSpareStoreItem
    /// </summary>
    private List<CFoundTaskItem> m_pFoundTaskList;
    /// <summary>
    /// Название формы поиска по БД
    /// </summary>
    private string m_sFormName;
    /// <summary>
    /// Текст текущего запроса на выборку данных из базы
    /// </summary>
    internal string SqlText;
    /// <summary>
    /// "Человекочитаемое" представление фильтра текущего SQL-запроса
    /// </summary>
    internal string FilterDescription;

    #endregion

    #region Class constructors
    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConfig"></param>
    public CSearchFormDoc(CScheduleditorConfig pAppConfig)
    {
      m_pAppConfig = pAppConfig;
      //TODO: Разобраться с m_pLogWriter
      m_pLogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
      m_pDbDoc = new CDbDoc(m_pAppConfig, LogWriter);

      m_pFoundTaskList = new List<CFoundTaskItem>();
    }

    #endregion
    
    #region CSearchFormDoc public/internal methods

    /// <summary>
    /// Метод для выборки данных из базы
    /// </summary>
    /// <param name="iSize">Размер страницы (в записях)</param>
    /// <param name="iPage">Номер страницы в выборке (возвращаемый)</param>
    /// <returns>Общее количество страниц в выборке</returns>
    /// <param name="sMessage">Ответ Хранилища</param>
    internal int DbSelect(int iSize, ref int iPage, out string sMessage)
    {
      int ii = m_pDbDoc.SearchTaskCount(m_pTaskFilter, out sMessage);
      FormName = ii.ToString();
      while (((1 + iSize * (iPage - 1)) > ii) && iPage > 1) {
        iPage--;
      }
      m_pDbDoc.SearchTask(m_pFoundTaskList, m_pTaskFilter, 1 + iSize * (iPage - 1), iSize, out sMessage);
      return ii / iSize + (((ii % iSize) > 0) ? 1 : 0);
    }

    /// <summary>
    /// Удаление задачи из Хранилища задач
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи в Хранилище</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Результат операции (true/false)</returns>
    internal bool Delete(object pIdentifier, out string sMessage)
    {
      return m_pDbDoc.DeleteTask(pIdentifier, out sMessage);
    }

    /// <summary>
    /// Осуществляет отладочную запись в лог-файл
    /// </summary>
    /// <param name="sMessage">Текст сообщения</param>
    /// <param name="arParams">Список параметров</param>
    internal void WriteLog(string sMessage, params object[] arParams)
    {
      h_writeLog(sMessage, arParams);
    }

    #endregion

    #region CSearchFormDoc private methods


    private void h_writeLog(string sMessage, params object[] arParams)
    {
      m_pLogWriter.Write(ELogType.Debug, m_pAppConfig.InstanceName, sMessage, arParams);
    }

    #endregion

    #region CSearchFormDoc public properties
    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CScheduleditorConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }
    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }
    /// <summary>
    /// Получает список найденных записей
    /// </summary>
    public List<CFoundTaskItem> FoundTaskList
    {
      get
      {
        return m_pFoundTaskList;
      }
    }
    /// <summary>
    /// Название формы поиска по БД
    /// </summary>
    public string FormName
    {
      get { return string.Format("Поиск комплектующих по БД. Найдено записей: {0}", m_sFormName); }
      set { m_sFormName = value; }
    }

    #endregion

    #region CDoc methods
    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
      m_pDbDoc.Open();
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
      m_pDbDoc.Close();
    }

    #endregion
  }

  #endregion
}
