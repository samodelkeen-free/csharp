﻿using Aist.Engine.Windows.Controls;

namespace Scheduleditor.Forms
{
  partial class CSearchForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.splSearch = new System.Windows.Forms.SplitContainer();
      this.chbNearest = new System.Windows.Forms.CheckBox();
      this.dtpNearest = new Aist.Engine.Windows.Controls.CAistDateTimePicker();
      this.label25 = new System.Windows.Forms.Label();
      this.chbNextStart = new System.Windows.Forms.CheckBox();
      this.chbSearchString = new System.Windows.Forms.CheckBox();
      this.label6 = new System.Windows.Forms.Label();
      this.txtSearchString = new System.Windows.Forms.TextBox();
      this.dtpNextStartEnd = new Aist.Engine.Windows.Controls.CAistDateTimePicker();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.dtpNextStartBegin = new Aist.Engine.Windows.Controls.CAistDateTimePicker();
      this.btnSearch = new System.Windows.Forms.Button();
      this.gridFind = new System.Windows.Forms.DataGridView();
      this.panMain = new System.Windows.Forms.Panel();
      this.btnPrint = new System.Windows.Forms.Button();
      this.btnDelete = new System.Windows.Forms.Button();
      this.btnEdit = new System.Windows.Forms.Button();
      this.btnSwitch = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.panPage = new System.Windows.Forms.Panel();
      this.lnkEnd = new System.Windows.Forms.LinkLabel();
      this.lnkBack = new System.Windows.Forms.LinkLabel();
      this.lnkBegin = new System.Windows.Forms.LinkLabel();
      this.lnkForward = new System.Windows.Forms.LinkLabel();
      this.lnkOne = new System.Windows.Forms.LinkLabel();
      this.lnkOther = new System.Windows.Forms.LinkLabel();
      this.lnkTwo = new System.Windows.Forms.LinkLabel();
      this.lnkThree = new System.Windows.Forms.LinkLabel();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.RowRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TaskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TaskIdentifier = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TaskDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TaskPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TaskDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.NextStartTime = new Aist.Engine.Windows.Controls.CDataGridViewDateTimeColumn();
      this.LastStartTime = new Aist.Engine.Windows.Controls.CDataGridViewDateTimeColumn();
      this.ModifyDate = new Aist.Engine.Windows.Controls.CDataGridViewDateTimeColumn();
      ((System.ComponentModel.ISupportInitialize)(this.splSearch)).BeginInit();
      this.splSearch.Panel1.SuspendLayout();
      this.splSearch.Panel2.SuspendLayout();
      this.splSearch.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridFind)).BeginInit();
      this.panMain.SuspendLayout();
      this.panPage.SuspendLayout();
      this.SuspendLayout();
      // 
      // splSearch
      // 
      this.splSearch.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splSearch.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splSearch.Location = new System.Drawing.Point(0, 0);
      this.splSearch.Name = "splSearch";
      // 
      // splSearch.Panel1
      // 
      this.splSearch.Panel1.AutoScroll = true;
      this.splSearch.Panel1.Controls.Add(this.chbNearest);
      this.splSearch.Panel1.Controls.Add(this.dtpNearest);
      this.splSearch.Panel1.Controls.Add(this.label25);
      this.splSearch.Panel1.Controls.Add(this.chbNextStart);
      this.splSearch.Panel1.Controls.Add(this.chbSearchString);
      this.splSearch.Panel1.Controls.Add(this.label6);
      this.splSearch.Panel1.Controls.Add(this.txtSearchString);
      this.splSearch.Panel1.Controls.Add(this.dtpNextStartEnd);
      this.splSearch.Panel1.Controls.Add(this.label5);
      this.splSearch.Panel1.Controls.Add(this.label4);
      this.splSearch.Panel1.Controls.Add(this.label3);
      this.splSearch.Panel1.Controls.Add(this.dtpNextStartBegin);
      this.splSearch.Panel1.Controls.Add(this.btnSearch);
      // 
      // splSearch.Panel2
      // 
      this.splSearch.Panel2.Controls.Add(this.gridFind);
      this.splSearch.Panel2.Controls.Add(this.panMain);
      this.splSearch.Panel2.Controls.Add(this.panPage);
      this.splSearch.Size = new System.Drawing.Size(712, 503);
      this.splSearch.SplitterDistance = 330;
      this.splSearch.TabIndex = 1;
      // 
      // chbNearest
      // 
      this.chbNearest.AutoSize = true;
      this.chbNearest.Location = new System.Drawing.Point(299, 178);
      this.chbNearest.Name = "chbNearest";
      this.chbNearest.Size = new System.Drawing.Size(15, 14);
      this.chbNearest.TabIndex = 14;
      this.chbNearest.UseVisualStyleBackColor = true;
      this.chbNearest.CheckedChanged += new System.EventHandler(this.chbAll_CheckedChanged);
      // 
      // dtpNearest
      // 
      this.dtpNearest.BackDisabledColor = System.Drawing.SystemColors.Control;
      this.dtpNearest.CustomFormat = "dd.MM.yyyy  HH:mm";
      this.dtpNearest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtpNearest.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpNearest.Location = new System.Drawing.Point(147, 173);
      this.dtpNearest.Name = "dtpNearest";
      this.dtpNearest.ShowCheckBox = true;
      this.dtpNearest.Size = new System.Drawing.Size(142, 22);
      this.dtpNearest.TabIndex = 13;
      this.dtpNearest.Tag = "chbNearest";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label25.Location = new System.Drawing.Point(10, 149);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(219, 13);
      this.label25.TabIndex = 61;
      this.label25.Text = "Ближайшие к указанному времени:";
      // 
      // chbNextStart
      // 
      this.chbNextStart.AutoSize = true;
      this.chbNextStart.Location = new System.Drawing.Point(299, 112);
      this.chbNextStart.Name = "chbNextStart";
      this.chbNextStart.Size = new System.Drawing.Size(15, 14);
      this.chbNextStart.TabIndex = 11;
      this.chbNextStart.UseVisualStyleBackColor = true;
      this.chbNextStart.CheckedChanged += new System.EventHandler(this.chbAll_CheckedChanged);
      // 
      // chbSearchString
      // 
      this.chbSearchString.AutoSize = true;
      this.chbSearchString.Location = new System.Drawing.Point(299, 42);
      this.chbSearchString.Name = "chbSearchString";
      this.chbSearchString.Size = new System.Drawing.Size(15, 14);
      this.chbSearchString.TabIndex = 8;
      this.chbSearchString.Tag = "";
      this.chbSearchString.UseVisualStyleBackColor = true;
      this.chbSearchString.CheckedChanged += new System.EventHandler(this.chbAll_CheckedChanged);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label6.Location = new System.Drawing.Point(10, 14);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(124, 13);
      this.label6.TabIndex = 100;
      this.label6.Text = "Строка для поиска:";
      // 
      // txtSearchString
      // 
      this.txtSearchString.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtSearchString.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txtSearchString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtSearchString.Location = new System.Drawing.Point(13, 38);
      this.txtSearchString.Name = "txtSearchString";
      this.txtSearchString.Size = new System.Drawing.Size(276, 22);
      this.txtSearchString.TabIndex = 7;
      this.txtSearchString.Tag = "chbSearchString";
      // 
      // dtpNextStartEnd
      // 
      this.dtpNextStartEnd.BackDisabledColor = System.Drawing.SystemColors.Control;
      this.dtpNextStartEnd.CustomFormat = "dd.MM.yyyy  HH:mm";
      this.dtpNextStartEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtpNextStartEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpNextStartEnd.Location = new System.Drawing.Point(147, 107);
      this.dtpNextStartEnd.Name = "dtpNextStartEnd";
      this.dtpNextStartEnd.ShowCheckBox = true;
      this.dtpNextStartEnd.Size = new System.Drawing.Size(142, 22);
      this.dtpNextStartEnd.TabIndex = 10;
      this.dtpNextStartEnd.Tag = "chbNextStart";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label5.Location = new System.Drawing.Point(122, 107);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(21, 13);
      this.label5.TabIndex = 100;
      this.label5.Text = "по";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.Location = new System.Drawing.Point(129, 81);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(14, 13);
      this.label4.TabIndex = 100;
      this.label4.Text = "с";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.Location = new System.Drawing.Point(10, 81);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(97, 13);
      this.label3.TabIndex = 100;
      this.label3.Text = "Время запуска";
      // 
      // dtpNextStartBegin
      // 
      this.dtpNextStartBegin.BackDisabledColor = System.Drawing.SystemColors.Control;
      this.dtpNextStartBegin.CustomFormat = "dd.MM.yyyy  HH:mm";
      this.dtpNextStartBegin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtpNextStartBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpNextStartBegin.Location = new System.Drawing.Point(147, 76);
      this.dtpNextStartBegin.Name = "dtpNextStartBegin";
      this.dtpNextStartBegin.ShowCheckBox = true;
      this.dtpNextStartBegin.Size = new System.Drawing.Size(142, 22);
      this.dtpNextStartBegin.TabIndex = 9;
      this.dtpNextStartBegin.Tag = "chbNextStart";
      // 
      // btnSearch
      // 
      this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSearch.Image = global::Scheduleditor.Properties.Resources.search_p04;
      this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSearch.Location = new System.Drawing.Point(224, 453);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(90, 25);
      this.btnSearch.TabIndex = 34;
      this.btnSearch.Text = "Найти в БД";
      this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnSearch.UseVisualStyleBackColor = true;
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // gridFind
      // 
      this.gridFind.AllowUserToAddRows = false;
      this.gridFind.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
      this.gridFind.BorderStyle = System.Windows.Forms.BorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.gridFind.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.gridFind.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.gridFind.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowRank,
            this.TaskName,
            this.TaskIdentifier,
            this.TaskDescription,
            this.TaskPeriod,
            this.TaskDuration,
            this.NextStartTime,
            this.LastStartTime,
            this.ModifyDate});
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.gridFind.DefaultCellStyle = dataGridViewCellStyle3;
      this.gridFind.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gridFind.Location = new System.Drawing.Point(0, 60);
      this.gridFind.MultiSelect = false;
      this.gridFind.Name = "gridFind";
      this.gridFind.ReadOnly = true;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.gridFind.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
      this.gridFind.RowHeadersVisible = false;
      this.gridFind.RowHeadersWidth = 25;
      this.gridFind.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.gridFind.RowsDefaultCellStyle = dataGridViewCellStyle5;
      this.gridFind.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.gridFind.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.gridFind.Size = new System.Drawing.Size(378, 383);
      this.gridFind.TabIndex = 8;
      this.gridFind.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridFind_CellDoubleClick);
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnPrint);
      this.panMain.Controls.Add(this.btnDelete);
      this.panMain.Controls.Add(this.btnEdit);
      this.panMain.Controls.Add(this.btnSwitch);
      this.panMain.Controls.Add(this.btnExit);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panMain.Location = new System.Drawing.Point(0, 443);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(378, 60);
      this.panMain.TabIndex = 1;
      // 
      // btnPrint
      // 
      this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnPrint.Image = global::Scheduleditor.Properties.Resources.fileprint_p03;
      this.btnPrint.Location = new System.Drawing.Point(173, 10);
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.Size = new System.Drawing.Size(40, 40);
      this.btnPrint.TabIndex = 10;
      this.btnPrint.UseVisualStyleBackColor = true;
      this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
      // 
      // btnDelete
      // 
      this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnDelete.Image = global::Scheduleditor.Properties.Resources.paper_drop_p03;
      this.btnDelete.Location = new System.Drawing.Point(265, 10);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.Size = new System.Drawing.Size(40, 40);
      this.btnDelete.TabIndex = 12;
      this.btnDelete.UseVisualStyleBackColor = true;
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      // 
      // btnEdit
      // 
      this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnEdit.Image = global::Scheduleditor.Properties.Resources.paper_edit_p03;
      this.btnEdit.Location = new System.Drawing.Point(219, 10);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new System.Drawing.Size(40, 40);
      this.btnEdit.TabIndex = 11;
      this.btnEdit.UseVisualStyleBackColor = true;
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // btnSwitch
      // 
      this.btnSwitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSwitch.Image = global::Scheduleditor.Properties.Resources.search_n_table_p03;
      this.btnSwitch.Location = new System.Drawing.Point(10, 10);
      this.btnSwitch.Name = "btnSwitch";
      this.btnSwitch.Size = new System.Drawing.Size(40, 40);
      this.btnSwitch.TabIndex = 9;
      this.btnSwitch.UseVisualStyleBackColor = true;
      this.btnSwitch.Click += new System.EventHandler(this.btnShow_Click);
      // 
      // btnExit
      // 
      this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnExit.Image = global::Scheduleditor.Properties.Resources.exit_p03;
      this.btnExit.Location = new System.Drawing.Point(328, 10);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(40, 40);
      this.btnExit.TabIndex = 13;
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // panPage
      // 
      this.panPage.Controls.Add(this.lnkEnd);
      this.panPage.Controls.Add(this.lnkBack);
      this.panPage.Controls.Add(this.lnkBegin);
      this.panPage.Controls.Add(this.lnkForward);
      this.panPage.Controls.Add(this.lnkOne);
      this.panPage.Controls.Add(this.lnkOther);
      this.panPage.Controls.Add(this.lnkTwo);
      this.panPage.Controls.Add(this.lnkThree);
      this.panPage.Dock = System.Windows.Forms.DockStyle.Top;
      this.panPage.Location = new System.Drawing.Point(0, 0);
      this.panPage.Name = "panPage";
      this.panPage.Size = new System.Drawing.Size(378, 60);
      this.panPage.TabIndex = 0;
      // 
      // lnkEnd
      // 
      this.lnkEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkEnd.AutoSize = true;
      this.lnkEnd.LinkVisited = true;
      this.lnkEnd.Location = new System.Drawing.Point(330, 10);
      this.lnkEnd.Name = "lnkEnd";
      this.lnkEnd.Size = new System.Drawing.Size(38, 13);
      this.lnkEnd.TabIndex = 17;
      this.lnkEnd.TabStop = true;
      this.lnkEnd.Text = "Конец";
      this.lnkEnd.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkBack
      // 
      this.lnkBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkBack.AutoSize = true;
      this.lnkBack.LinkVisited = true;
      this.lnkBack.Location = new System.Drawing.Point(201, 10);
      this.lnkBack.Name = "lnkBack";
      this.lnkBack.Size = new System.Drawing.Size(19, 13);
      this.lnkBack.TabIndex = 11;
      this.lnkBack.TabStop = true;
      this.lnkBack.Text = "<<";
      this.lnkBack.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkBegin
      // 
      this.lnkBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkBegin.AutoSize = true;
      this.lnkBegin.LinkVisited = true;
      this.lnkBegin.Location = new System.Drawing.Point(151, 10);
      this.lnkBegin.Name = "lnkBegin";
      this.lnkBegin.Size = new System.Drawing.Size(44, 13);
      this.lnkBegin.TabIndex = 10;
      this.lnkBegin.TabStop = true;
      this.lnkBegin.Text = "Начало";
      this.lnkBegin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkForward
      // 
      this.lnkForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkForward.AutoSize = true;
      this.lnkForward.LinkVisited = true;
      this.lnkForward.Location = new System.Drawing.Point(305, 10);
      this.lnkForward.Name = "lnkForward";
      this.lnkForward.Size = new System.Drawing.Size(19, 13);
      this.lnkForward.TabIndex = 16;
      this.lnkForward.TabStop = true;
      this.lnkForward.Text = ">>";
      this.lnkForward.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkOne
      // 
      this.lnkOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkOne.AutoSize = true;
      this.lnkOne.LinkVisited = true;
      this.lnkOne.Location = new System.Drawing.Point(226, 10);
      this.lnkOne.Name = "lnkOne";
      this.lnkOne.Size = new System.Drawing.Size(13, 13);
      this.lnkOne.TabIndex = 12;
      this.lnkOne.TabStop = true;
      this.lnkOne.Text = "1";
      this.lnkOne.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkOther
      // 
      this.lnkOther.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkOther.AutoSize = true;
      this.lnkOther.LinkVisited = true;
      this.lnkOther.Location = new System.Drawing.Point(283, 10);
      this.lnkOther.Name = "lnkOther";
      this.lnkOther.Size = new System.Drawing.Size(16, 13);
      this.lnkOther.TabIndex = 15;
      this.lnkOther.TabStop = true;
      this.lnkOther.Text = "...";
      this.lnkOther.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkTwo
      // 
      this.lnkTwo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkTwo.AutoSize = true;
      this.lnkTwo.LinkVisited = true;
      this.lnkTwo.Location = new System.Drawing.Point(245, 10);
      this.lnkTwo.Name = "lnkTwo";
      this.lnkTwo.Size = new System.Drawing.Size(13, 13);
      this.lnkTwo.TabIndex = 13;
      this.lnkTwo.TabStop = true;
      this.lnkTwo.Text = "2";
      this.lnkTwo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkThree
      // 
      this.lnkThree.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkThree.AutoSize = true;
      this.lnkThree.LinkVisited = true;
      this.lnkThree.Location = new System.Drawing.Point(264, 10);
      this.lnkThree.Name = "lnkThree";
      this.lnkThree.Size = new System.Drawing.Size(13, 13);
      this.lnkThree.TabIndex = 14;
      this.lnkThree.TabStop = true;
      this.lnkThree.Text = "3";
      this.lnkThree.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.DataPropertyName = "RowRank";
      dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
      this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
      this.dataGridViewTextBoxColumn1.Frozen = true;
      this.dataGridViewTextBoxColumn1.HeaderText = "№, п/п";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 30;
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.DataPropertyName = "SpareName";
      this.dataGridViewTextBoxColumn2.HeaderText = "Наименование";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn3
      // 
      this.dataGridViewTextBoxColumn3.DataPropertyName = "SpareGost";
      this.dataGridViewTextBoxColumn3.HeaderText = "Обозначение";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      this.dataGridViewTextBoxColumn3.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn4
      // 
      this.dataGridViewTextBoxColumn4.DataPropertyName = "PlantName";
      this.dataGridViewTextBoxColumn4.HeaderText = "Завод";
      this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
      this.dataGridViewTextBoxColumn4.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn5
      // 
      this.dataGridViewTextBoxColumn5.DataPropertyName = "SpareNumber";
      this.dataGridViewTextBoxColumn5.HeaderText = "Номер";
      this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
      this.dataGridViewTextBoxColumn5.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn6
      // 
      this.dataGridViewTextBoxColumn6.DataPropertyName = "ProduceDate";
      this.dataGridViewTextBoxColumn6.HeaderText = "Дата выпуска";
      this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
      this.dataGridViewTextBoxColumn6.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn7
      // 
      this.dataGridViewTextBoxColumn7.DataPropertyName = "EntryDate";
      this.dataGridViewTextBoxColumn7.HeaderText = "Ввод в эксплуатацию";
      this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
      this.dataGridViewTextBoxColumn7.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn8
      // 
      this.dataGridViewTextBoxColumn8.DataPropertyName = "ChangeFlag";
      this.dataGridViewTextBoxColumn8.HeaderText = "Заменено";
      this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
      this.dataGridViewTextBoxColumn8.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn9
      // 
      this.dataGridViewTextBoxColumn9.DataPropertyName = "CustomerName";
      this.dataGridViewTextBoxColumn9.HeaderText = "Собственник";
      this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
      this.dataGridViewTextBoxColumn9.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn10
      // 
      this.dataGridViewTextBoxColumn10.DataPropertyName = "ContractNumber";
      this.dataGridViewTextBoxColumn10.HeaderText = "Номер договора";
      this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
      this.dataGridViewTextBoxColumn10.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn11
      // 
      this.dataGridViewTextBoxColumn11.DataPropertyName = "ReceiveDate";
      this.dataGridViewTextBoxColumn11.HeaderText = "Дата поступления на склад";
      this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
      this.dataGridViewTextBoxColumn11.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn12
      // 
      this.dataGridViewTextBoxColumn12.DataPropertyName = "CertNumber";
      this.dataGridViewTextBoxColumn12.HeaderText = "Номер свидетельства";
      this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
      this.dataGridViewTextBoxColumn12.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn13
      // 
      this.dataGridViewTextBoxColumn13.DataPropertyName = "CertDate";
      this.dataGridViewTextBoxColumn13.HeaderText = "Дата проверки";
      this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
      this.dataGridViewTextBoxColumn13.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn14
      // 
      this.dataGridViewTextBoxColumn14.DataPropertyName = "NextDate";
      this.dataGridViewTextBoxColumn14.HeaderText = "Дата следующей проверки";
      this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
      this.dataGridViewTextBoxColumn14.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn15
      // 
      this.dataGridViewTextBoxColumn15.DataPropertyName = "ReturnDate";
      this.dataGridViewTextBoxColumn15.HeaderText = "Дата выдачи со склада";
      this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
      this.dataGridViewTextBoxColumn15.ReadOnly = true;
      // 
      // RowRank
      // 
      this.RowRank.DataPropertyName = "RowRank";
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
      dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.RowRank.DefaultCellStyle = dataGridViewCellStyle2;
      this.RowRank.Frozen = true;
      this.RowRank.HeaderText = "№, п/п";
      this.RowRank.Name = "RowRank";
      this.RowRank.ReadOnly = true;
      this.RowRank.Width = 30;
      // 
      // TaskName
      // 
      this.TaskName.DataPropertyName = "Name";
      this.TaskName.HeaderText = "Наименование";
      this.TaskName.Name = "TaskName";
      this.TaskName.ReadOnly = true;
      // 
      // TaskIdentifier
      // 
      this.TaskIdentifier.DataPropertyName = "Identifier";
      this.TaskIdentifier.HeaderText = "Идентификатор";
      this.TaskIdentifier.Name = "TaskIdentifier";
      this.TaskIdentifier.ReadOnly = true;
      // 
      // TaskDescription
      // 
      this.TaskDescription.DataPropertyName = "Description";
      this.TaskDescription.HeaderText = "Описание задачи";
      this.TaskDescription.Name = "TaskDescription";
      this.TaskDescription.ReadOnly = true;
      // 
      // TaskPeriod
      // 
      this.TaskPeriod.DataPropertyName = "Period";
      this.TaskPeriod.HeaderText = "Периодичность";
      this.TaskPeriod.Name = "TaskPeriod";
      this.TaskPeriod.ReadOnly = true;
      // 
      // TaskDuration
      // 
      this.TaskDuration.DataPropertyName = "Duration";
      this.TaskDuration.HeaderText = "Максимальная длительность";
      this.TaskDuration.Name = "TaskDuration";
      this.TaskDuration.ReadOnly = true;
      // 
      // NextStartTime
      // 
      this.NextStartTime.DataPropertyName = "NextStartTime";
      this.NextStartTime.HeaderText = "Время следующего запуска";
      this.NextStartTime.Name = "NextStartTime";
      this.NextStartTime.ReadOnly = true;
      this.NextStartTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.NextStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
      // 
      // LastStartTime
      // 
      this.LastStartTime.DataPropertyName = "LastStartTime";
      this.LastStartTime.HeaderText = "Время последнего запуска";
      this.LastStartTime.Name = "LastStartTime";
      this.LastStartTime.ReadOnly = true;
      this.LastStartTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.LastStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
      // 
      // ModifyDate
      // 
      this.ModifyDate.DataPropertyName = "ModifyDate";
      this.ModifyDate.HeaderText = "Дата модификации задачи";
      this.ModifyDate.Name = "ModifyDate";
      this.ModifyDate.ReadOnly = true;
      this.ModifyDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.ModifyDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
      // 
      // CSearchForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(712, 503);
      this.Controls.Add(this.splSearch);
      this.KeyPreview = true;
      this.MinimumSize = new System.Drawing.Size(720, 530);
      this.Name = "CSearchForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "SearchForm";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CSearchForm_FormClosed);
      this.Load += new System.EventHandler(this.CSearchForm_Load);
      this.splSearch.Panel1.ResumeLayout(false);
      this.splSearch.Panel1.PerformLayout();
      this.splSearch.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splSearch)).EndInit();
      this.splSearch.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.gridFind)).EndInit();
      this.panMain.ResumeLayout(false);
      this.panPage.ResumeLayout(false);
      this.panPage.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.SplitContainer splSearch;
    private System.Windows.Forms.Button btnSwitch;
    private System.Windows.Forms.Panel panMain;
    private System.Windows.Forms.Button btnEdit;
    private System.Windows.Forms.Button btnSearch;
    private System.Windows.Forms.Button btnDelete;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox txtSearchString;
    private CAistDateTimePicker dtpNextStartEnd;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private CAistDateTimePicker dtpNextStartBegin;
    private System.Windows.Forms.CheckBox chbNextStart;
    private System.Windows.Forms.CheckBox chbSearchString;
    private System.Windows.Forms.CheckBox chbNearest;
    private CAistDateTimePicker dtpNearest;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.Panel panPage;
    private System.Windows.Forms.LinkLabel lnkEnd;
    private System.Windows.Forms.LinkLabel lnkBack;
    private System.Windows.Forms.LinkLabel lnkBegin;
    private System.Windows.Forms.LinkLabel lnkForward;
    private System.Windows.Forms.LinkLabel lnkOne;
    private System.Windows.Forms.LinkLabel lnkOther;
    private System.Windows.Forms.LinkLabel lnkTwo;
    private System.Windows.Forms.LinkLabel lnkThree;
    private System.Windows.Forms.DataGridView gridFind;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
    private System.Windows.Forms.Button btnPrint;
    private System.Windows.Forms.DataGridViewTextBoxColumn RowRank;
    private System.Windows.Forms.DataGridViewTextBoxColumn TaskName;
    private System.Windows.Forms.DataGridViewTextBoxColumn TaskIdentifier;
    private System.Windows.Forms.DataGridViewTextBoxColumn TaskDescription;
    private System.Windows.Forms.DataGridViewTextBoxColumn TaskPeriod;
    private System.Windows.Forms.DataGridViewTextBoxColumn TaskDuration;
    private CDataGridViewDateTimeColumn NextStartTime;
    private CDataGridViewDateTimeColumn LastStartTime;
    private CDataGridViewDateTimeColumn ModifyDate;
  }
}