﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using
using System;
using System.Windows.Forms;
using Scheduleditor.Docs;
using Scheduleditor.Lib.Config;

#endregion

namespace Scheduleditor.Forms
{
  /// <summary>
  /// Форма
  /// </summary>
  public partial class CBlankForm2 : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private readonly CBlankForm2Doc m_pFormDoc;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    public CBlankForm2(CScheduleditorConfig pAppConfig)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CBlankForm2Doc(pAppConfig);
        m_pFormDoc.Open();
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods

    #endregion

    #region Control handlers

    private void CBlankForm2_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
    }

    private void btnOk_Click(object sender, EventArgs e)
    {
      // Подтвердили изменения:

    }

    #endregion
  }
}
