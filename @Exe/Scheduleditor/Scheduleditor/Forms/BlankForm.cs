﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System;
using System.Windows.Forms;
using Scheduleditor.Docs;
using Scheduleditor.Lib.Config;

#endregion

namespace Scheduleditor.Forms
{
  /// <summary>
  /// Форма
  /// </summary>
  public partial class CBlankForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private readonly CBlankFormDoc m_pFormDoc;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    public CBlankForm(CScheduleditorConfig pAppConfig)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CBlankFormDoc(pAppConfig);
        m_pFormDoc.Open();
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods

    #endregion

    #region Control handlers
    private void btnExit_Click(object sender, System.EventArgs e)
    {
      Close();
    }

    private void CBlankForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
    }

    #endregion
  }
}
