﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#define CORRECTION_20170112_EVV_BINDING // Привязка элементов управления через DataBinding

#region Using
using System;
using System.Windows.Forms;
using Aist.Engine.Windows;
using Aist.Engine.Windows.Controls;
using Aist.Engine.Windows.Forms;
using Scheduleditor.Docs;
using Scheduleditor.Lib;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Config;
using Sfo.Sys.Windows;

#endregion

namespace Scheduleditor.Forms
{
  /// <summary>
  /// Форма редактирования задачи планировщика
  /// </summary>
  public partial class CTaskEditForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private readonly CTaskEditFormDoc m_pFormDoc;
    /// <summary>
    /// Флаг отслеживания изменений контролов
    /// </summary>
    private bool m_bHandleChanging = false;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pTaskIdentifier">Идентификатор задачи</param>
    public CTaskEditForm(CScheduleditorConfig pAppConfig, object pTaskIdentifier)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CTaskEditFormDoc(pAppConfig, pTaskIdentifier);
        m_pFormDoc.Open();
        h_init();
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods

    /// <summary>
    /// Инициализация формы редактирования задачи.
    /// </summary>
    private void h_init()
    {
#if CORRECTION_20170112_EVV_BINDING
      txtName.DataBindings.Add(new Binding("Text", m_pFormDoc.CronTask, "Name"));
      txtIdentifier.DataBindings.Add(new Binding("Text", m_pFormDoc.CronTask, "Identifier"));
      txtDuration.DataBindings.Add(new Binding("Text", m_pFormDoc.CronTask, "Duration"));

      txtName.Text = m_pFormDoc.CronTask.Name;
      txtIdentifier.Text = m_pFormDoc.CronTask.Identifier.ToString();
      txtDuration.Text = m_pFormDoc.CronTask.Duration.ToString();
#else
#endif
      h_refresh();
    }

    /// <summary>
    /// Инициализация комбобокса для выбора одиночных временнЫх меток.
    /// </summary>
    private void h_ccbAloneFill()
    {
      ccbAlone.Items.Clear();
      switch (tcField.SelectedIndex) {
        case 0:
          // CheckedComboBox для минут
          for (int ii = 0; ii < 60; ++ii) {
            CCBoxItem item = new CCBoxItem(ii.ToString(), ii);
            ccbAlone.Items.Add(item);
          }
          ccbAlone.ColumnWidth = 50;
          ccbAlone.MaxDropDownItems = 6;
          ccbAlone.DisplayMember = "Text";
          ccbAlone.ValueSeparator = ", ";
          break;
        case 1:
          // CheckedComboBox для часов
          for (int ii = 0; ii < 24; ++ii) {
            CCBoxItem item = new CCBoxItem(ii.ToString(), ii);
            ccbAlone.Items.Add(item);
          }
          ccbAlone.ColumnWidth = 50;
          ccbAlone.MaxDropDownItems = 6;
          ccbAlone.DisplayMember = "Text";
          ccbAlone.ValueSeparator = ", ";
          break;
        case 2:
          // CheckedComboBox для дней месяца
          for (int ii = 0; ii < 31; ++ii) {
            CCBoxItem item = new CCBoxItem((ii+1).ToString(), ii);
            ccbAlone.Items.Add(item);
          }
          ccbAlone.ColumnWidth = 50;
          ccbAlone.MaxDropDownItems = 10;
          ccbAlone.DisplayMember = "Text";
          ccbAlone.ValueSeparator = ", ";
          break;
        case 3:
          // CheckedComboBox для месяцев
          for (int ii = 0; ii < CScheduleditorLibConst.MonthArray.Length; ++ii) {
            CCBoxItem item = new CCBoxItem(CScheduleditorLibConst.MonthArray[ii], ii);
            ccbAlone.Items.Add(item);
          }
          ccbAlone.ColumnWidth = 100;
          ccbAlone.MaxDropDownItems = 6;
          ccbAlone.DisplayMember = "Text";
          ccbAlone.ValueSeparator = ", ";
          break;
        case 4:
          // CheckedComboBox для дней недели
          for (int ii = 0; ii < CScheduleditorLibConst.WeekArray.Length; ++ii) {
            CCBoxItem item = new CCBoxItem(CScheduleditorLibConst.WeekArray[ii], ii);
            ccbAlone.Items.Add(item);
          }
          ccbAlone.ColumnWidth = 100;
          ccbAlone.MaxDropDownItems = 7;
          ccbAlone.DisplayMember = "Text";
          ccbAlone.ValueSeparator = ", ";
          break;
      }
      //ccbAlone.SetItemChecked(1, true);
      //ccbAlone.SetItemCheckState(1, CheckState.Indeterminate);
    }

    /// <summary>
    /// Перерисовка формы редактирования задачи.
    /// </summary>
    private void h_refresh()
    {
      int iField = tcField.SelectedIndex;
      if (iField < 5) { // Если открыта вкладка для редактирования поля:
        m_bHandleChanging = false; // Отключаем обработчики изменений значений контролов
        rbtnSeparate.Checked = m_pFormDoc.CronTask.Period[iField].Separate;
        rbtnIntegral.Checked = !rbtnSeparate.Checked;
        h_ccbAloneFill();
        string sInterval = String.Empty;
        string sAlone = String.Empty;
        string sRepeat = String.Empty;
        foreach (CCronFieldInterval pCronFieldInterval in m_pFormDoc.CronTask.Period[iField]) {
          if (pCronFieldInterval.End != null) // Это интервалы:
            switch (iField) {
              case 0:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + pCronFieldInterval.Begin + "-" + pCronFieldInterval.End + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step + " мин." : String.Empty);
                break;
              case 1:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + pCronFieldInterval.Begin + "-" + pCronFieldInterval.End + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step + " ч." : String.Empty);
                break;
              case 2:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + (pCronFieldInterval.Begin + 1) + "-" + (pCronFieldInterval.End + 1) + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step + " дн." : String.Empty);
                break;
              case 3:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + CScheduleditorLibConst.MonthArray[(int) pCronFieldInterval.Begin] + "-" + 
                  CScheduleditorLibConst.MonthArray[(int) pCronFieldInterval.End] + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step + " мес." : String.Empty);
                break;
              case 4:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + CScheduleditorLibConst.WeekArray[(int) pCronFieldInterval.Begin] + "-" +
                  CScheduleditorLibConst.WeekArray[(int)pCronFieldInterval.End] + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step + " дн." : String.Empty);
                break;
              default:
                sInterval += (!String.IsNullOrEmpty(sInterval) ? ", " : String.Empty) + pCronFieldInterval.Begin + "-" + pCronFieldInterval.End + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step : String.Empty);
                break;
            }
          else { // Это не интервалы:
            if (pCronFieldInterval.Begin == null) { // Это '*' в Cron
              sRepeat = pCronFieldInterval.Step.ToString();
            }
            else { // Это единичные временнЫе позиции:
              switch (iField) {
                case 2:
                  sAlone += (!String.IsNullOrEmpty(sAlone) ? ", " : String.Empty) + (pCronFieldInterval.Begin + 1);
                  break;
                case 3:
                  sAlone += (!String.IsNullOrEmpty(sAlone) ? ", " : String.Empty) + CScheduleditorLibConst.MonthArray[(int)pCronFieldInterval.Begin];
                  break;
                case 4:
                  sAlone += (!String.IsNullOrEmpty(sAlone) ? ", " : String.Empty) + CScheduleditorLibConst.WeekArray[(int)pCronFieldInterval.Begin];
                  break;
                default:
                  sAlone += (!String.IsNullOrEmpty(sAlone) ? ", " : String.Empty) + pCronFieldInterval.Begin;
                  break;
              }
              // Выставляем флажки в CheckedComboBox:
              for (int ii = 0; ii < ccbAlone.Items.Count; ++ii) {
                if ((ccbAlone.Items[ii] as CCBoxItem).Value == pCronFieldInterval.Begin)
                  ccbAlone.SetItemChecked(ii, true);
              }
            }
          }
        }
        txtInterval.Text = sInterval;
        ccbAlone.Text = sAlone;
        txtRepeat.Text = sRepeat;

        m_bHandleChanging = true; // Включаем обработчики изменений значений контролов
        ccbAlone.Enabled = m_pFormDoc.CronTask.Period[iField].Separate;
        txtInterval.Enabled = m_pFormDoc.CronTask.Period[iField].Separate;
        btnInterval.Enabled = m_pFormDoc.CronTask.Period[iField].Separate;
        txtRepeat.Enabled = !m_pFormDoc.CronTask.Period[iField].Separate;
      }
      txtRemark.Text = "Время следующего запуска: " + CCronTask.GetNextStartTime(m_pFormDoc.CronTask);
    }

    /// <summary>
    /// Вызов формы редактора интервалов
    /// </summary>
    /// <param name="iSourceInterval">Идентификатор источника интервалов</param>
    private void h_interval(int iSourceInterval)
    {
      CIntervalGridDescription pIntervalGridDescription = CScheduleditorLib.GetIntervalGridDescription(m_pFormDoc.DbDoc, iSourceInterval);
      CListForm pForm = new CListForm(m_pFormDoc.AppConfig, pIntervalGridDescription, new CChildFormProcessorImplementation());
      if (!pForm.IsDisposed) {
        pForm.ShowDialog();
        h_refresh();
      }
      /*
      CChildFormProcessorImplementation pChildFormProcessor = new CChildFormProcessorImplementation();
      Form pForm = pChildFormProcessor.CreateFormByClass(m_pFormDoc.TreeDescription.ListFormClass,
                                                         new Type[] { typeof(CAppConfig), typeof(CGridDescription), typeof(CChildFormProcessor) },
                                                         new object[] { m_pFormDoc.AppConfig, m_pFormDoc.TreeDescription.GridDescription, m_pChildFormProcessor }
        );
      if (pForm != null && !pForm.IsDisposed)
      {
        DialogResult pDialogResult = pForm.ShowDialog();
        if (pDialogResult == DialogResult.OK)
        {
          m_pFormDoc.TreeDescription.Changed = true;
          h_fillTree();
          //h_refreshTree();
        }
        else
        {

        }
      }
      */
    }

    #endregion

    #region Control handlers

    private void ccbAlone_DropDownClosed(object sender, EventArgs e)
    {
      /* EVV: Для тестирования
      txtOut.AppendText(string.Format("value changed: {0}\r\n", ccbAlone.ValueChanged));
      txtOut.AppendText(string.Format("value: {0}\r\n", ccbAlone.Text));

      StringBuilder sb = new StringBuilder("Items checked: ");
      foreach (CCBoxItem item in ccbAlone.CheckedItems) {
        sb.Append(item.Text).Append(ccbAlone.ValueSeparator);
      }
      sb.Remove(sb.Length - ccbAlone.ValueSeparator.Length, ccbAlone.ValueSeparator.Length);
      txtOut.AppendText(sb.ToString());
      */
    }

    private void ccbAlone_ItemCheck(object sender, ItemCheckEventArgs e)
    {
      /* EVV: Для тестирования
      CCBoxItem item = ccbAlone.Items[e.Index] as CCBoxItem;
      txtOut.AppendText(string.Format("Item '{0}' is about to be {1}\r\n", item.Text, e.NewValue.ToString()));
      */
    }

    private void CTaskEditForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
      // Обновление гридов форм поиска по Хранилищу, если таковые открыты:
      foreach (Form pChildForm in MdiParent.MdiChildren) {
        if (pChildForm.Name == "CSearchForm") {
          (pChildForm as CSearchForm).Search();
        }
      }
    }

    private void btnOk_Click(object sender, EventArgs e)
    {
      // Подтвердили изменения (попытка сохранения задачи в Хранилище):
      if (!m_pFormDoc.CronTask.Modified) Close();
      else {
        string sRemark;
        bool bResult = m_pFormDoc.Save(out sRemark);
        if (!bResult) txtRemark.Text = sRemark;
        else Close();
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      // Отменили изменения (будет запрос на сохранение задачи в Хранилище):
      Close();
    }

    private void btnInterval_Click(object sender, EventArgs e)
    {
      int iSourceInterval = tcField.SelectedIndex;
      h_interval(iSourceInterval);
    }

    private void tcField_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (tcField.SelectedIndex < 5) panCommon.Parent = tcField.SelectedTab;
      switch (tcField.SelectedIndex) {
        case 0:
          labMeasure.Text = "Минуты";
          labRepeatPre.Text = "Повторять каждую минуту,";
          labRepeatPost.Text = "мин.";
          break;
        case 1:
          labMeasure.Text = "Часы";
          labRepeatPre.Text = "Повторять каждый час,";
          labRepeatPost.Text = "час.";
          break;
        case 2:
          labMeasure.Text = "Дни месяца";
          labRepeatPre.Text = "Повторять каждый день,";
          labRepeatPost.Text = "дн.";
          break;
        case 3:
          labMeasure.Text = "Месяцы";
          labRepeatPre.Text = "Повторять каждый месяц,";
          labRepeatPost.Text = "мес.";
          break;
        case 4:
          labMeasure.Text = "Дни недели";
          labRepeatPre.Text = "Повторять каждый день,";
          labRepeatPost.Text = "дн.";
          break;
        default:
          break;
      }
      h_refresh();
    }

    private void rbtnSeparate_Click(object sender, EventArgs e)
    {
#if CORRECTION_20170112_EVV_BINDING
#else
      m_pFormDoc.CronTask.Modified = true;
      int iField = tcField.SelectedIndex;
      m_pFormDoc.CronTask.Period[iField].Clear();
      switch ((sender as Control).Name) {
        case "rbtnSeparate":
          m_pFormDoc.CronTask.Period[iField].Separate = true;
          break;
        case "rbtnIntegral":
          m_pFormDoc.CronTask.Period[iField].Separate = false;
          m_pFormDoc.CronTask.Period[iField].Add(new CCronFieldInterval());
          break;
      }
      h_refresh();
#endif
    }

    private void rbtnSeparate_CheckedChanged(object sender, EventArgs e)
    {
      if (!m_bHandleChanging) return;
      m_pFormDoc.CronTask.Modified = true;
      int iField = tcField.SelectedIndex;
      m_pFormDoc.CronTask.Period[iField].Clear();
      m_pFormDoc.CronTask.Period[iField].Separate = (sender as RadioButton).Checked;
      if (!m_pFormDoc.CronTask.Period[iField].Separate) m_pFormDoc.CronTask.Period[iField].Add(new CCronFieldInterval());
      h_refresh();
    }

    /// <summary>
    /// Общий метод обработки изменения значений контролов формы редактирования
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void formControl_ValueChanged(object sender, EventArgs e)
    {
      if (!m_bHandleChanging) return;
      m_pFormDoc.CronTask.Modified = true;
      int iField = tcField.SelectedIndex;
      switch ((sender as Control).Name) {
#if CORRECTION_20170112_EVV_BINDING

#else
        case "txtName":
          m_pFormDoc.CronTask.Name = txtName.Text;
          break;
        case "txtIdentifier":
          m_pFormDoc.CronTask.Identifier = txtIdentifier.Text;
          break;
#endif
        case "txtRepeat":
          if (!m_pFormDoc.CronTask.Period[iField].Separate) {
            m_pFormDoc.CronTask.Period[iField].Clear();
            if (!String.IsNullOrEmpty(txtRepeat.Text.Trim()))
              try {
                m_pFormDoc.CronTask.Period[iField].Add(new CCronFieldInterval {Step = Convert.ToInt32(txtRepeat.Text.Trim())} );
              }
              catch (FormatException ex) {
                //txtRepeat.Text = String.Empty;
              }
            else m_pFormDoc.CronTask.Period[iField].Add(new CCronFieldInterval());
          }
          break;
#if CORRECTION_20170112_EVV_BINDING

#else
        case "txtDuration":
          if (!String.IsNullOrEmpty(txtDuration.Text.Trim()))
            try {
              m_pFormDoc.CronTask.Duration = Convert.ToInt32(txtDuration.Text.Trim());
            }
            catch (FormatException ex) {
              //txtDuration.Text = String.Empty;
            }
          //else m_pFormDoc.CronTask.Duration = 0;
          break;
#endif
        case "ccbAlone":
          if (m_pFormDoc.CronTask.Period[iField].Separate) {
            m_pFormDoc.RemoveAlone(iField);
            foreach (CCBoxItem pCBoxItem in ccbAlone.CheckedItems)
              m_pFormDoc.CronTask.Period[iField].Add(new CCronFieldInterval { Begin = pCBoxItem.Value });
          }
          break;
      }
      // Обновление информационного поля:
      txtRemark.Text = "Время следующего запуска: " + CCronTask.GetNextStartTime(m_pFormDoc.CronTask);
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (!m_pFormDoc.CronTask.Modified) {
        txtRemark.Text = "Отсутствуют изменения для сохранения";
        return;
      }
      string sRemark;
      m_pFormDoc.Save(out sRemark);
      txtRemark.Text = sRemark;
    }

    private void CTaskEditForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (!m_pFormDoc.CronTask.Modified) return;
      switch (CMessageBox.ShowYesNoCancel(string.Format("Сохранить изменения для {0}?",
                                                        string.Format("{0} [{1}]", m_pFormDoc.CronTask.Name,
                                                                                   m_pFormDoc.CronTask.Identifier)))) {
        case DialogResult.Yes:
          string sRemark;
          e.Cancel = !m_pFormDoc.Save(out sRemark);
          txtRemark.Text = sRemark;
          break;
        case DialogResult.No:
          txtRemark.Text = "Изменения не были сохранены в БД.";
          break;
        case DialogResult.Cancel:
          e.Cancel = true;
          break;
      }
    }

    #endregion

  }
}
