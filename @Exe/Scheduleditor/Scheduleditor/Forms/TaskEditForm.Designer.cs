﻿namespace Scheduleditor.Forms
{
  partial class CTaskEditForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panMain = new System.Windows.Forms.Panel();
      this.btnSave = new System.Windows.Forms.Button();
      this.txtRemark = new System.Windows.Forms.TextBox();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.tcField = new System.Windows.Forms.TabControl();
      this.tpMinute = new System.Windows.Forms.TabPage();
      this.panCommon = new System.Windows.Forms.Panel();
      this.ccbAlone = new Aist.Engine.Windows.Controls.CheckedComboBox();
      this.labMeasure = new System.Windows.Forms.Label();
      this.labRepeatPre = new System.Windows.Forms.Label();
      this.rbtnIntegral = new System.Windows.Forms.RadioButton();
      this.rbtnSeparate = new System.Windows.Forms.RadioButton();
      this.label8 = new System.Windows.Forms.Label();
      this.labRepeatPost = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.txtRepeat = new System.Windows.Forms.TextBox();
      this.txtInterval = new System.Windows.Forms.TextBox();
      this.btnInterval = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.tpHour = new System.Windows.Forms.TabPage();
      this.tpDay = new System.Windows.Forms.TabPage();
      this.tpMonth = new System.Windows.Forms.TabPage();
      this.tpWeek = new System.Windows.Forms.TabPage();
      this.tpOther = new System.Windows.Forms.TabPage();
      this.label11 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.txtDuration = new System.Windows.Forms.TextBox();
      this.panHeader = new System.Windows.Forms.Panel();
      this.txtIdentifier = new System.Windows.Forms.TextBox();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.panMain.SuspendLayout();
      this.tcField.SuspendLayout();
      this.tpMinute.SuspendLayout();
      this.panCommon.SuspendLayout();
      this.tpOther.SuspendLayout();
      this.panHeader.SuspendLayout();
      this.SuspendLayout();
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnSave);
      this.panMain.Controls.Add(this.txtRemark);
      this.panMain.Controls.Add(this.btnOk);
      this.panMain.Controls.Add(this.btnCancel);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panMain.Location = new System.Drawing.Point(0, 313);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(592, 60);
      this.panMain.TabIndex = 100;
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSave.Image = global::Scheduleditor.Properties.Resources.filesave_p03;
      this.btnSave.Location = new System.Drawing.Point(386, 10);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(40, 40);
      this.btnSave.TabIndex = 0;
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // txtRemark
      // 
      this.txtRemark.BackColor = System.Drawing.SystemColors.Control;
      this.txtRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtRemark.Dock = System.Windows.Forms.DockStyle.Left;
      this.txtRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtRemark.Location = new System.Drawing.Point(0, 0);
      this.txtRemark.Multiline = true;
      this.txtRemark.Name = "txtRemark";
      this.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtRemark.Size = new System.Drawing.Size(376, 60);
      this.txtRemark.TabIndex = 2;
      this.txtRemark.TabStop = false;
      this.txtRemark.Tag = "";
      this.txtRemark.Text = "Планировщик не настроен";
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnOk.Image = global::Scheduleditor.Properties.Resources.button_ok_p03;
      this.btnOk.Location = new System.Drawing.Point(496, 10);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(40, 40);
      this.btnOk.TabIndex = 1;
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Image = global::Scheduleditor.Properties.Resources.button_cancel_p03;
      this.btnCancel.Location = new System.Drawing.Point(542, 10);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(40, 40);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // tcField
      // 
      this.tcField.Controls.Add(this.tpMinute);
      this.tcField.Controls.Add(this.tpHour);
      this.tcField.Controls.Add(this.tpDay);
      this.tcField.Controls.Add(this.tpMonth);
      this.tcField.Controls.Add(this.tpWeek);
      this.tcField.Controls.Add(this.tpOther);
      this.tcField.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tcField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tcField.Location = new System.Drawing.Point(0, 79);
      this.tcField.Name = "tcField";
      this.tcField.SelectedIndex = 0;
      this.tcField.Size = new System.Drawing.Size(592, 234);
      this.tcField.TabIndex = 1;
      this.tcField.SelectedIndexChanged += new System.EventHandler(this.tcField_SelectedIndexChanged);
      // 
      // tpMinute
      // 
      this.tpMinute.Controls.Add(this.panCommon);
      this.tpMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tpMinute.Location = new System.Drawing.Point(4, 25);
      this.tpMinute.Name = "tpMinute";
      this.tpMinute.Padding = new System.Windows.Forms.Padding(3);
      this.tpMinute.Size = new System.Drawing.Size(584, 205);
      this.tpMinute.TabIndex = 1;
      this.tpMinute.Text = "Минуты";
      this.tpMinute.UseVisualStyleBackColor = true;
      // 
      // panCommon
      // 
      this.panCommon.Controls.Add(this.ccbAlone);
      this.panCommon.Controls.Add(this.labMeasure);
      this.panCommon.Controls.Add(this.labRepeatPre);
      this.panCommon.Controls.Add(this.rbtnIntegral);
      this.panCommon.Controls.Add(this.rbtnSeparate);
      this.panCommon.Controls.Add(this.label8);
      this.panCommon.Controls.Add(this.labRepeatPost);
      this.panCommon.Controls.Add(this.label7);
      this.panCommon.Controls.Add(this.txtRepeat);
      this.panCommon.Controls.Add(this.txtInterval);
      this.panCommon.Controls.Add(this.btnInterval);
      this.panCommon.Controls.Add(this.label6);
      this.panCommon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panCommon.Location = new System.Drawing.Point(3, 3);
      this.panCommon.Name = "panCommon";
      this.panCommon.Size = new System.Drawing.Size(578, 199);
      this.panCommon.TabIndex = 107;
      // 
      // ccbAlone
      // 
      this.ccbAlone.BackColor = System.Drawing.Color.PowderBlue;
      this.ccbAlone.CheckOnClick = true;
      this.ccbAlone.ColumnWidth = 50;
      this.ccbAlone.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.ccbAlone.DropDownHeight = 1;
      this.ccbAlone.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.ccbAlone.ForeColor = System.Drawing.SystemColors.WindowText;
      this.ccbAlone.FormattingEnabled = true;
      this.ccbAlone.IntegralHeight = false;
      this.ccbAlone.Location = new System.Drawing.Point(130, 12);
      this.ccbAlone.Name = "ccbAlone";
      this.ccbAlone.Size = new System.Drawing.Size(360, 23);
      this.ccbAlone.TabIndex = 2;
      this.ccbAlone.ValueSeparator = ", ";
      this.ccbAlone.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // labMeasure
      // 
      this.labMeasure.AutoSize = true;
      this.labMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labMeasure.Location = new System.Drawing.Point(43, 17);
      this.labMeasure.Name = "labMeasure";
      this.labMeasure.Size = new System.Drawing.Size(52, 13);
      this.labMeasure.TabIndex = 103;
      this.labMeasure.Text = "Минуты";
      // 
      // labRepeatPre
      // 
      this.labRepeatPre.AutoSize = true;
      this.labRepeatPre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labRepeatPre.Location = new System.Drawing.Point(43, 106);
      this.labRepeatPre.Name = "labRepeatPre";
      this.labRepeatPre.Size = new System.Drawing.Size(168, 13);
      this.labRepeatPre.TabIndex = 113;
      this.labRepeatPre.Text = "Повторять каждую минуту,";
      // 
      // rbtnIntegral
      // 
      this.rbtnIntegral.AutoSize = true;
      this.rbtnIntegral.Location = new System.Drawing.Point(17, 106);
      this.rbtnIntegral.Name = "rbtnIntegral";
      this.rbtnIntegral.Size = new System.Drawing.Size(14, 13);
      this.rbtnIntegral.TabIndex = 1;
      this.rbtnIntegral.TabStop = true;
      this.rbtnIntegral.UseVisualStyleBackColor = true;
      this.rbtnIntegral.Click += new System.EventHandler(this.rbtnSeparate_Click);
      // 
      // rbtnSeparate
      // 
      this.rbtnSeparate.AutoSize = true;
      this.rbtnSeparate.Checked = true;
      this.rbtnSeparate.Location = new System.Drawing.Point(17, 34);
      this.rbtnSeparate.Name = "rbtnSeparate";
      this.rbtnSeparate.Size = new System.Drawing.Size(14, 13);
      this.rbtnSeparate.TabIndex = 0;
      this.rbtnSeparate.TabStop = true;
      this.rbtnSeparate.UseVisualStyleBackColor = true;
      this.rbtnSeparate.CheckedChanged += new System.EventHandler(this.rbtnSeparate_CheckedChanged);
      this.rbtnSeparate.Click += new System.EventHandler(this.rbtnSeparate_Click);
      // 
      // label8
      // 
      this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label8.Location = new System.Drawing.Point(7, 86);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(564, 2);
      this.label8.TabIndex = 110;
      // 
      // labRepeatPost
      // 
      this.labRepeatPost.AutoSize = true;
      this.labRepeatPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labRepeatPost.Location = new System.Drawing.Point(349, 130);
      this.labRepeatPost.Name = "labRepeatPost";
      this.labRepeatPost.Size = new System.Drawing.Size(34, 13);
      this.labRepeatPost.TabIndex = 109;
      this.labRepeatPost.Text = "мин.";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label7.Location = new System.Drawing.Point(43, 130);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(88, 13);
      this.label7.TabIndex = 108;
      this.label7.Text = "либо с шагом";
      // 
      // txtRepeat
      // 
      this.txtRepeat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtRepeat.Enabled = false;
      this.txtRepeat.Location = new System.Drawing.Point(203, 126);
      this.txtRepeat.Name = "txtRepeat";
      this.txtRepeat.Size = new System.Drawing.Size(140, 22);
      this.txtRepeat.TabIndex = 4;
      this.txtRepeat.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // txtInterval
      // 
      this.txtInterval.BackColor = System.Drawing.Color.PowderBlue;
      this.txtInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtInterval.Location = new System.Drawing.Point(130, 50);
      this.txtInterval.Name = "txtInterval";
      this.txtInterval.ReadOnly = true;
      this.txtInterval.Size = new System.Drawing.Size(360, 22);
      this.txtInterval.TabIndex = 105;
      this.txtInterval.TabStop = false;
      this.txtInterval.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // btnInterval
      // 
      this.btnInterval.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnInterval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnInterval.Location = new System.Drawing.Point(496, 50);
      this.btnInterval.Name = "btnInterval";
      this.btnInterval.Size = new System.Drawing.Size(34, 22);
      this.btnInterval.TabIndex = 3;
      this.btnInterval.Text = ". . .";
      this.btnInterval.UseVisualStyleBackColor = true;
      this.btnInterval.Click += new System.EventHandler(this.btnInterval_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label6.Location = new System.Drawing.Point(43, 54);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(73, 13);
      this.label6.TabIndex = 104;
      this.label6.Text = "Интервалы";
      // 
      // tpHour
      // 
      this.tpHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tpHour.Location = new System.Drawing.Point(4, 25);
      this.tpHour.Name = "tpHour";
      this.tpHour.Padding = new System.Windows.Forms.Padding(3);
      this.tpHour.Size = new System.Drawing.Size(584, 205);
      this.tpHour.TabIndex = 2;
      this.tpHour.Text = "Часы";
      this.tpHour.UseVisualStyleBackColor = true;
      // 
      // tpDay
      // 
      this.tpDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tpDay.Location = new System.Drawing.Point(4, 25);
      this.tpDay.Name = "tpDay";
      this.tpDay.Padding = new System.Windows.Forms.Padding(3);
      this.tpDay.Size = new System.Drawing.Size(584, 205);
      this.tpDay.TabIndex = 3;
      this.tpDay.Text = "Дни месяца";
      this.tpDay.UseVisualStyleBackColor = true;
      // 
      // tpMonth
      // 
      this.tpMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tpMonth.Location = new System.Drawing.Point(4, 25);
      this.tpMonth.Name = "tpMonth";
      this.tpMonth.Padding = new System.Windows.Forms.Padding(3);
      this.tpMonth.Size = new System.Drawing.Size(584, 205);
      this.tpMonth.TabIndex = 4;
      this.tpMonth.Text = "Месяцы";
      this.tpMonth.UseVisualStyleBackColor = true;
      // 
      // tpWeek
      // 
      this.tpWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tpWeek.Location = new System.Drawing.Point(4, 25);
      this.tpWeek.Name = "tpWeek";
      this.tpWeek.Padding = new System.Windows.Forms.Padding(3);
      this.tpWeek.Size = new System.Drawing.Size(584, 205);
      this.tpWeek.TabIndex = 5;
      this.tpWeek.Text = "Дни недели";
      this.tpWeek.UseVisualStyleBackColor = true;
      // 
      // tpOther
      // 
      this.tpOther.Controls.Add(this.label11);
      this.tpOther.Controls.Add(this.label12);
      this.tpOther.Controls.Add(this.txtDuration);
      this.tpOther.Location = new System.Drawing.Point(4, 25);
      this.tpOther.Name = "tpOther";
      this.tpOther.Padding = new System.Windows.Forms.Padding(3);
      this.tpOther.Size = new System.Drawing.Size(584, 205);
      this.tpOther.TabIndex = 6;
      this.tpOther.Text = "Дополнительно";
      this.tpOther.UseVisualStyleBackColor = true;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label11.Location = new System.Drawing.Point(352, 20);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(42, 13);
      this.label11.TabIndex = 112;
      this.label11.Text = "минут";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label12.Location = new System.Drawing.Point(10, 20);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(219, 13);
      this.label12.TabIndex = 111;
      this.label12.Text = "Максимальная продолжительность";
      // 
      // txtDuration
      // 
      this.txtDuration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtDuration.Location = new System.Drawing.Point(235, 16);
      this.txtDuration.Name = "txtDuration";
      this.txtDuration.Size = new System.Drawing.Size(111, 22);
      this.txtDuration.TabIndex = 0;
      this.txtDuration.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // panHeader
      // 
      this.panHeader.Controls.Add(this.txtIdentifier);
      this.panHeader.Controls.Add(this.txtName);
      this.panHeader.Controls.Add(this.label10);
      this.panHeader.Controls.Add(this.label9);
      this.panHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.panHeader.Location = new System.Drawing.Point(0, 0);
      this.panHeader.Name = "panHeader";
      this.panHeader.Size = new System.Drawing.Size(592, 79);
      this.panHeader.TabIndex = 0;
      // 
      // txtIdentifier
      // 
      this.txtIdentifier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtIdentifier.Location = new System.Drawing.Point(157, 46);
      this.txtIdentifier.Name = "txtIdentifier";
      this.txtIdentifier.Size = new System.Drawing.Size(340, 20);
      this.txtIdentifier.TabIndex = 1;
      this.txtIdentifier.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // txtName
      // 
      this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtName.Location = new System.Drawing.Point(157, 10);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(425, 20);
      this.txtName.TabIndex = 0;
      this.txtName.TextChanged += new System.EventHandler(this.formControl_ValueChanged);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label10.Location = new System.Drawing.Point(11, 48);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(100, 13);
      this.label10.TabIndex = 105;
      this.label10.Text = "Идентификатор";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label9.Location = new System.Drawing.Point(11, 12);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(140, 13);
      this.label9.TabIndex = 104;
      this.label9.Text = "Наименование задачи";
      // 
      // CTaskEditForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(592, 373);
      this.Controls.Add(this.tcField);
      this.Controls.Add(this.panMain);
      this.Controls.Add(this.panHeader);
      this.Name = "CTaskEditForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "TaskEditForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CTaskEditForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CTaskEditForm_FormClosed);
      this.panMain.ResumeLayout(false);
      this.panMain.PerformLayout();
      this.tcField.ResumeLayout(false);
      this.tpMinute.ResumeLayout(false);
      this.panCommon.ResumeLayout(false);
      this.panCommon.PerformLayout();
      this.tpOther.ResumeLayout(false);
      this.tpOther.PerformLayout();
      this.panHeader.ResumeLayout(false);
      this.panHeader.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panMain;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.TabControl tcField;
    private System.Windows.Forms.TabPage tpMinute;
    private System.Windows.Forms.Panel panHeader;
    private System.Windows.Forms.TabPage tpHour;
    private System.Windows.Forms.TabPage tpDay;
    private System.Windows.Forms.TabPage tpMonth;
    private System.Windows.Forms.TabPage tpWeek;
    private System.Windows.Forms.TabPage tpOther;
    private System.Windows.Forms.TextBox txtRemark;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Label labMeasure;
    private Aist.Engine.Windows.Controls.CheckedComboBox ccbAlone;
    private System.Windows.Forms.TextBox txtInterval;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button btnInterval;
    private System.Windows.Forms.Panel panCommon;
    private System.Windows.Forms.Label labRepeatPost;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox txtRepeat;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox txtDuration;
    private System.Windows.Forms.TextBox txtIdentifier;
    private System.Windows.Forms.TextBox txtName;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.RadioButton rbtnIntegral;
    private System.Windows.Forms.RadioButton rbtnSeparate;
    private System.Windows.Forms.Label labRepeatPre;

  }
}