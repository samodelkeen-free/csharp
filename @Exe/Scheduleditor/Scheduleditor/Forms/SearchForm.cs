﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System;
using System.Drawing;
using System.Windows.Forms;
using Aist.Engine.Classes;
using Aist.Engine.Windows;
using Scheduleditor.Docs;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Config;
using Sfo.Sys.Windows;

#endregion

namespace Scheduleditor.Forms
{
  /// <summary>
  /// Форма поиска записи в БД
  /// </summary>
  public partial class CSearchForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private CSearchFormDoc m_pFormDoc;
    /// <summary>
    /// Источник для отображения списка
    /// </summary>
    private BindingSource m_bsGridFind;
    /// <summary>
    /// Объект страничного меню
    /// </summary>
    private CPageMenu m_pPageMenu;
    /// <summary>
    /// Цвет по умолчанию для элементов управления
    /// </summary>
    private Color m_pControlBackColor;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    public CSearchForm(CScheduleditorConfig pAppConfig)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CSearchFormDoc(pAppConfig);
        m_pFormDoc.Open();

        m_pPageMenu = new CPageMenu();

        gridFind.AutoGenerateColumns = false;
        m_bsGridFind = new BindingSource();
        m_bsGridFind.DataSource = m_pFormDoc.FoundTaskList;
        gridFind.DataSource = m_bsGridFind;

        h_search();
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods

    /// <summary>
    /// Раскрашивание комбобокса
    /// </summary>
    /// <param name="sender">Объект комбобокса</param>
    /// <param name="bIsColored">Признак выделения цветом</param>
    private void h_comboBoxColor(object sender, bool bIsColored)
    {
      ComboBox pComboBox = (ComboBox) sender;
      if (bIsColored) {
        pComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))),
                                                            ((int)(((byte)(192)))));
      }
      else {
        pComboBox.BackColor = m_pControlBackColor;
      }
    }

    /// <summary>
    /// Заполнение грида значениями
    /// </summary>
    private void h_fillGrid()
    {
      string sMessage;
      // Выполняем SQL-запрос, получаем номер страницы выборки:
      m_pPageMenu.PageNumber = m_pFormDoc.DbSelect(m_pFormDoc.AppConfig.AppParam.MaxPageSize, ref m_pPageMenu.CurrentPage, out sMessage);
      m_pPageMenu.Update();
      m_bsGridFind.ResetBindings(false);
      h_repaintPageMenu();
      Text = m_pFormDoc.FormName;
    }

    /// <summary>
    /// Перерисовка меню выбора страницы
    /// </summary>
    private void h_repaintPageMenu()
    {
      CWindowToolkit.RepaintPageMenuItem(lnkBegin, m_pPageMenu.LnkBegin);
      CWindowToolkit.RepaintPageMenuItem(lnkBack, m_pPageMenu.LnkBack);
      CWindowToolkit.RepaintPageMenuItem(lnkOne, m_pPageMenu.NumericList[0]);
      CWindowToolkit.RepaintPageMenuItem(lnkTwo, m_pPageMenu.NumericList[1]);
      CWindowToolkit.RepaintPageMenuItem(lnkThree, m_pPageMenu.NumericList[2]);
      CWindowToolkit.RepaintPageMenuItem(lnkOther, m_pPageMenu.LnkOther);
      CWindowToolkit.RepaintPageMenuItem(lnkForward, m_pPageMenu.LnkForward);
      CWindowToolkit.RepaintPageMenuItem(lnkEnd, m_pPageMenu.LnkEnd);
    }

    /// <summary>
    /// Запуск поиска по БД
    /// </summary>
    private void h_search()
    {
      h_fillGrid();
      if (m_bsGridFind.Count == 0) {
        MessageBox.Show("Ничего не найдено.");
      }
    }

    /// <summary>
    /// Активация/деактивация чекбоксов
    /// </summary>
    private void h_setEnableState(bool bValue)
    {
      chbSearchString.Enabled =
        chbNextStart.Enabled =
        chbNearest.Enabled = bValue;
    }

    /// <summary>
    /// Вызов формы редактирования найденной записи
    /// </summary>
    private void h_edit()
    {
      if (m_bsGridFind.Current == null) return;
      CTaskEditForm pForm = new CTaskEditForm(m_pFormDoc.AppConfig, ((CFoundTaskItem)m_bsGridFind.Current).Identifier/*"044c812f-339e-4745-b0ff-cc2ced28717b"*/);
      if (!pForm.IsDisposed) {
        pForm.MdiParent = this.MdiParent;
        pForm.WindowState = FormWindowState.Maximized;
        pForm.Show();
      }
    }

    /// <summary>
    /// Вызов диалога удаления найденной записи
    /// </summary>
    private void h_delete()
    {
      if (m_bsGridFind.Current == null) return;

      switch (CMessageBox.ShowYesNo(string.Format("Удалить задачу {0} из Хранилища?", string.Format("{0} [{1}]",
                                                  (m_bsGridFind.Current as CFoundTaskItem).Name,
                                                  (m_bsGridFind.Current as CFoundTaskItem).Identifier)))) {
        case DialogResult.No:
          return;
      }
      string sErrorMessage;
      if (!m_pFormDoc.Delete(((CFoundTaskItem) m_bsGridFind.Current).Identifier, out sErrorMessage)) {
        MessageBox.Show(string.Format("Ошибка удаления задачи:\r\n{0}", sErrorMessage));
      }
      else {
        m_bsGridFind.RemoveCurrent();
      }
    }

    #endregion

    #region Public/internal methods

    /// <summary>
    /// Запуск поиска по Хранилищу (обрабатываются текущие настройки фильтра для текущей страницы выборки)
    /// </summary>
    internal void Search()
    {
      h_search();
    }

    #endregion

    #region Control handlers

    private void btnExit_Click(object sender, System.EventArgs e)
    {
      Close();
    }

    private void CSearchForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
    }

    private void btnShow_Click(object sender, System.EventArgs e)
    {
      splSearch.Panel1Collapsed = !splSearch.Panel1Collapsed;
    }

    private void btnSearch_Click(object sender, System.EventArgs e)
    {
      // Запуск поиска по БД:
      m_pPageMenu.CurrentPage = 1;
      h_search();
    }

    private void panPage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      // Обработчик перехода по страницам выборки:
      m_pPageMenu.ItemClick(((LinkLabel)sender).Name);
      h_fillGrid();
    }

    private void chbAll_CheckedChanged(object sender, System.EventArgs e)
    {
      // Закраска контролов
      foreach (Control c in ((CheckBox)sender).Parent.Controls) {
        if (c.Tag != null && c.Tag.ToString() == ((CheckBox)sender).Name) {
          if (((CheckBox)sender).Checked) {
            c.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))),
                                                        ((int)(((byte)(192)))));
          }
          else {
            c.BackColor = m_pControlBackColor;
          }
        }
      }
    }

    private void btnEdit_Click(object sender, System.EventArgs e)
    {
      // Вызов формы редактирования записи об устройстве в БД
      h_edit();
    }

    private void gridFind_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      // Вызов формы редактирования записи об устройстве в БД
      h_edit();
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      // Удаление записи из БД
      h_delete();
    }

    private void CSearchForm_Load(object sender, EventArgs e)
    {
    }

    private void btnPrint_Click(object sender, EventArgs e)
    {
    }

    #endregion
  }
}