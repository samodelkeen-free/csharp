﻿namespace Scheduleditor.Forms
{
  partial class CBlankForm2
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panMain = new System.Windows.Forms.Panel();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.panMain.SuspendLayout();
      this.SuspendLayout();
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnOk);
      this.panMain.Controls.Add(this.btnCancel);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panMain.Location = new System.Drawing.Point(0, 213);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(592, 60);
      this.panMain.TabIndex = 100;
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnOk.Image = global::Scheduleditor.Properties.Resources.button_ok_p03;
      this.btnOk.Location = new System.Drawing.Point(496, 10);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(40, 40);
      this.btnOk.TabIndex = 0;
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Image = global::Scheduleditor.Properties.Resources.button_cancel_p03;
      this.btnCancel.Location = new System.Drawing.Point(542, 10);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(40, 40);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // CBlankForm2
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(592, 273);
      this.Controls.Add(this.panMain);
      this.Name = "CBlankForm2";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "BlankForm2";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CBlankForm2_FormClosed);
      this.panMain.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panMain;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;

  }
}