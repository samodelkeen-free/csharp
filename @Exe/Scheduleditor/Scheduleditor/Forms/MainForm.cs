﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Windows.Forms;
using Scheduleditor.Docs;
using Scheduleditor.Lib.Config;
using Sfo.Sys;
using Sfo.Sys.Service;

#endregion

namespace Scheduleditor.Forms
{
  /// <summary>
  /// Главная форма приложения
  /// </summary>
  public partial class CMainForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Основной документ приложения
    /// </summary>
    private readonly CMainFormDoc m_pFormDoc;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор по заданному имени экземпляра
    /// </summary>
    /// <param name="sInstanceName"></param>
    public CMainForm(string sInstanceName)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CMainFormDoc(this, sInstanceName);
        m_pFormDoc.Open();
        this.Text = m_pFormDoc.AppConfig.DisplayName;
        this.mnuMain.MdiWindowListItem = miWindow;

      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при инициализации приложения: {0}", pEx.Message));
      }
    }

    #endregion

    #region Static methods

    /// <summary>
    /// Точка входа в приложение
    /// </summary>
    /// <param name="args"></param>
    public static void MainEntry(string[] args)
    {
      string sInstanceName = null;
      StringDictionary sd = CTool.SplitCommandLineArguments(args);
      if (sd.ContainsKey(CServiceTool.SERVICE_INSTANCE_PARAMETER_NAME)) {
        sInstanceName = sd[CServiceTool.SERVICE_INSTANCE_PARAMETER_NAME];
      }

      if (args.Length > 0) {
        if (sInstanceName == null) {
          sInstanceName = String.Empty;
        }
      }

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      CMainForm pMainForm = new CMainForm(sInstanceName);
      if (!pMainForm.IsDisposed) {
        Application.Run(pMainForm);
      }
    }

    #endregion

    #region Help methods

    /// <summary>
    /// Вызов формы добавления новой записи о задаче
    /// </summary>
    private void h_new()
    {
      bool bFlag = false;
      foreach (Form pChildForm in MdiChildren) {
        if (pChildForm.Name == "CTaskEditForm" /*&&
          (pChildForm as CTaskEditForm).SpareStoreItem.Identifier == null*/) {
          pChildForm.BringToFront();
          bFlag = true;
          break;
        }
      }
      if (bFlag) return;
      CTaskEditForm pForm = new CTaskEditForm(m_pFormDoc.AppConfig, null);
      if (!pForm.IsDisposed) {
        pForm.MdiParent = this;
        pForm.WindowState = FormWindowState.Maximized;
        pForm.Show();
      }
    }

    /// <summary>
    /// Вызов формы поиска по базе запланированных задач
    /// </summary>
    private void h_search()
    {
      // Вызов формы поиска по БД
      CSearchForm pForm = new CSearchForm(m_pFormDoc.AppConfig);
      if (!pForm.IsDisposed) {
        //pForm.Owner = this;
        pForm.MdiParent = this;
        pForm.WindowState = FormWindowState.Maximized;
        pForm.Show();
      }
    }

    /// <summary>
    /// Вызов формы настройки программы
    /// </summary>
    private void h_setting()
    {/*
      CSettingForm pForm = new CSettingForm(m_pFormDoc.AppConfig);
      if (!pForm.IsDisposed) {
        pForm.ShowDialog();
      }*/
    }

    /// <summary>
    /// Вызов формы для отображения ближайшего задания
    /// </summary>
    private void h_alarm()
    {

    }

    #endregion

    #region Control handlers

    private void miSetting_Click(object sender, EventArgs e)
    {
      h_setting();
    }

    private void btnSetting_Click(object sender, EventArgs e)
    {
      h_setting();
    }

    private void miExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void CMainForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
    }

    private void miTest_Click(object sender, EventArgs e)
    {
      // Вызов окна для тестирования
      CBlankForm pForm = new CBlankForm(m_pFormDoc.AppConfig);
      if (!pForm.IsDisposed) {
        pForm.ShowDialog();
      }
    }

    private void miTest2_Click(object sender, EventArgs e)
    {
      // Вызов окна 2 для тестирования (через Reflection)
      Type tForm = Type.GetType("Scheduleditor.Forms.CBlankForm2", false, true);
      if (tForm != null) {
        ConstructorInfo pConstructorInfo = tForm.GetConstructor(new Type[] { typeof(CScheduleditorConfig) });
        Form pForm = (Form)pConstructorInfo.Invoke(new object[] { m_pFormDoc.AppConfig });
        if (!pForm.IsDisposed) {
          pForm.ShowDialog();
        }
      }
    }

    private void miSearch_Click(object sender, EventArgs e)
    {
      h_search();
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      h_search();
    }

    private void miNew_Click(object sender, EventArgs e)
    {
      h_new();
    }

    private void btnNew_Click(object sender, EventArgs e)
    {
      h_new();
    }

    private void miCascade_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.Cascade); 
    }

    private void miVertical_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileHorizontal);
    }

    private void miHorizontal_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileVertical);
    }

    private void CMainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      /*
      if (e.CloseReason == CloseReason.UserClosing) {
        foreach (Form pForm in MdiChildren) {
          pForm.Close();
        }
      }
      e.Cancel = MdiChildren.Length > 0;
      */
    }

    private void miClose_Click(object sender, EventArgs e)
    {
      foreach (Form pForm in MdiChildren) {
        pForm.Close();
      }
    }

    private void btnInfo_Click(object sender, EventArgs e)
    {

    }

    private void miAlarm_Click(object sender, EventArgs e)
    {
      h_alarm();
    }

    private void btnAlarm_Click(object sender, EventArgs e)
    {
      h_alarm();
    }

    #endregion

  }
}
