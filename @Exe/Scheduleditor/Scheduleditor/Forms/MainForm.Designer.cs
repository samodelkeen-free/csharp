﻿namespace Scheduleditor.Forms
{
  partial class CMainForm
  {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
          if (disposing && (components != null))
          {
              components.Dispose();
          }
          base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CMainForm));
      this.mnuMain = new System.Windows.Forms.MenuStrip();
      this.miProgram = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.miSetting = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.miExit = new System.Windows.Forms.ToolStripMenuItem();
      this.miDataBase = new System.Windows.Forms.ToolStripMenuItem();
      this.miNew = new System.Windows.Forms.ToolStripMenuItem();
      this.miSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.miAlarm = new System.Windows.Forms.ToolStripMenuItem();
      this.miWindow = new System.Windows.Forms.ToolStripMenuItem();
      this.miCascade = new System.Windows.Forms.ToolStripMenuItem();
      this.miVertical = new System.Windows.Forms.ToolStripMenuItem();
      this.miHorizontal = new System.Windows.Forms.ToolStripMenuItem();
      this.miClose = new System.Windows.Forms.ToolStripMenuItem();
      this.miHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.miTest = new System.Windows.Forms.ToolStripMenuItem();
      this.miTest2 = new System.Windows.Forms.ToolStripMenuItem();
      this.panMain = new System.Windows.Forms.Panel();
      this.btnSetting = new System.Windows.Forms.Button();
      this.btnAlarm = new System.Windows.Forms.Button();
      this.btnNew = new System.Windows.Forms.Button();
      this.btnSearch = new System.Windows.Forms.Button();
      this.btnInfo = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.mnuMain.SuspendLayout();
      this.panMain.SuspendLayout();
      this.SuspendLayout();
      // 
      // mnuMain
      // 
      this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miProgram,
            this.miDataBase,
            this.miWindow,
            this.miHelp});
      this.mnuMain.Location = new System.Drawing.Point(0, 0);
      this.mnuMain.Name = "mnuMain";
      this.mnuMain.Size = new System.Drawing.Size(792, 24);
      this.mnuMain.TabIndex = 0;
      this.mnuMain.TabStop = true;
      this.mnuMain.Text = "menuStrip1";
      // 
      // miProgram
      // 
      this.miProgram.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.miSetting,
            this.toolStripMenuItem2,
            this.miExit});
      this.miProgram.Name = "miProgram";
      this.miProgram.Size = new System.Drawing.Size(73, 20);
      this.miProgram.Text = "Программа";
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
      // 
      // miSetting
      // 
      this.miSetting.Name = "miSetting";
      this.miSetting.Size = new System.Drawing.Size(152, 22);
      this.miSetting.Text = "Настройки";
      this.miSetting.Click += new System.EventHandler(this.miSetting_Click);
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(149, 6);
      // 
      // miExit
      // 
      this.miExit.Name = "miExit";
      this.miExit.Size = new System.Drawing.Size(152, 22);
      this.miExit.Text = "Выход";
      this.miExit.Click += new System.EventHandler(this.miExit_Click);
      // 
      // miDataBase
      // 
      this.miDataBase.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNew,
            this.miSearch,
            this.toolStripMenuItem3,
            this.miAlarm});
      this.miDataBase.Name = "miDataBase";
      this.miDataBase.Size = new System.Drawing.Size(89, 20);
      this.miDataBase.Text = "Планировщик";
      // 
      // miNew
      // 
      this.miNew.Name = "miNew";
      this.miNew.Size = new System.Drawing.Size(171, 22);
      this.miNew.Text = "Новая задача";
      this.miNew.Click += new System.EventHandler(this.miNew_Click);
      // 
      // miSearch
      // 
      this.miSearch.Name = "miSearch";
      this.miSearch.Size = new System.Drawing.Size(171, 22);
      this.miSearch.Text = "Поиск";
      this.miSearch.Click += new System.EventHandler(this.miSearch_Click);
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new System.Drawing.Size(168, 6);
      // 
      // miAlarm
      // 
      this.miAlarm.Name = "miAlarm";
      this.miAlarm.Size = new System.Drawing.Size(171, 22);
      this.miAlarm.Text = "Ближайшая задача";
      this.miAlarm.Click += new System.EventHandler(this.miAlarm_Click);
      // 
      // miWindow
      // 
      this.miWindow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCascade,
            this.miVertical,
            this.miHorizontal,
            this.miClose});
      this.miWindow.Name = "miWindow";
      this.miWindow.Size = new System.Drawing.Size(45, 20);
      this.miWindow.Text = "&Окна";
      // 
      // miCascade
      // 
      this.miCascade.Name = "miCascade";
      this.miCascade.Size = new System.Drawing.Size(152, 22);
      this.miCascade.Text = "Каскадом";
      this.miCascade.Click += new System.EventHandler(this.miCascade_Click);
      // 
      // miVertical
      // 
      this.miVertical.Name = "miVertical";
      this.miVertical.Size = new System.Drawing.Size(152, 22);
      this.miVertical.Text = "Вертикально";
      this.miVertical.Click += new System.EventHandler(this.miVertical_Click);
      // 
      // miHorizontal
      // 
      this.miHorizontal.Name = "miHorizontal";
      this.miHorizontal.Size = new System.Drawing.Size(152, 22);
      this.miHorizontal.Text = "Горизонтально";
      this.miHorizontal.Click += new System.EventHandler(this.miHorizontal_Click);
      // 
      // miClose
      // 
      this.miClose.Name = "miClose";
      this.miClose.Size = new System.Drawing.Size(152, 22);
      this.miClose.Text = "Закрыть все";
      this.miClose.Click += new System.EventHandler(this.miClose_Click);
      // 
      // miHelp
      // 
      this.miHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTest,
            this.miTest2});
      this.miHelp.Name = "miHelp";
      this.miHelp.Size = new System.Drawing.Size(59, 20);
      this.miHelp.Text = "Помощь";
      // 
      // miTest
      // 
      this.miTest.Name = "miTest";
      this.miTest.Size = new System.Drawing.Size(157, 22);
      this.miTest.Text = "Тестовое окно";
      this.miTest.Click += new System.EventHandler(this.miTest_Click);
      // 
      // miTest2
      // 
      this.miTest2.Name = "miTest2";
      this.miTest2.Size = new System.Drawing.Size(157, 22);
      this.miTest2.Text = "Тестовое окно 2";
      this.miTest2.Click += new System.EventHandler(this.miTest2_Click);
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnAlarm);
      this.panMain.Controls.Add(this.btnNew);
      this.panMain.Controls.Add(this.btnSearch);
      this.panMain.Controls.Add(this.btnSetting);
      this.panMain.Controls.Add(this.btnInfo);
      this.panMain.Controls.Add(this.btnExit);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Left;
      this.panMain.Location = new System.Drawing.Point(0, 24);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(60, 529);
      this.panMain.TabIndex = 2;
      // 
      // btnSetting
      // 
      this.btnSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSetting.Image = global::Scheduleditor.Properties.Resources.device_p03;
      this.btnSetting.Location = new System.Drawing.Point(10, 387);
      this.btnSetting.Name = "btnSetting";
      this.btnSetting.Size = new System.Drawing.Size(40, 40);
      this.btnSetting.TabIndex = 11;
      this.btnSetting.UseVisualStyleBackColor = true;
      this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
      // 
      // btnAlarm
      // 
      this.btnAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnAlarm.Image = global::Scheduleditor.Properties.Resources.Book_n_clock_p3;
      this.btnAlarm.Location = new System.Drawing.Point(10, 102);
      this.btnAlarm.Name = "btnAlarm";
      this.btnAlarm.Size = new System.Drawing.Size(40, 40);
      this.btnAlarm.TabIndex = 6;
      this.btnAlarm.UseVisualStyleBackColor = true;
      this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
      // 
      // btnNew
      // 
      this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnNew.Image = global::Scheduleditor.Properties.Resources.paper_new_p03;
      this.btnNew.Location = new System.Drawing.Point(10, 10);
      this.btnNew.Name = "btnNew";
      this.btnNew.Size = new System.Drawing.Size(40, 40);
      this.btnNew.TabIndex = 1;
      this.btnNew.UseVisualStyleBackColor = true;
      this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
      // 
      // btnSearch
      // 
      this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSearch.Image = global::Scheduleditor.Properties.Resources.search_p03;
      this.btnSearch.Location = new System.Drawing.Point(10, 56);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(40, 40);
      this.btnSearch.TabIndex = 2;
      this.btnSearch.UseVisualStyleBackColor = true;
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // btnInfo
      // 
      this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnInfo.Image = global::Scheduleditor.Properties.Resources.info_p03;
      this.btnInfo.Location = new System.Drawing.Point(10, 433);
      this.btnInfo.Name = "btnInfo";
      this.btnInfo.Size = new System.Drawing.Size(40, 40);
      this.btnInfo.TabIndex = 12;
      this.btnInfo.UseVisualStyleBackColor = true;
      this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
      // 
      // btnExit
      // 
      this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnExit.Image = global::Scheduleditor.Properties.Resources.exit_p03;
      this.btnExit.Location = new System.Drawing.Point(10, 479);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(40, 40);
      this.btnExit.TabIndex = 13;
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // CMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(792, 553);
      this.Controls.Add(this.panMain);
      this.Controls.Add(this.mnuMain);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.IsMdiContainer = true;
      this.MainMenuStrip = this.mnuMain;
      this.Name = "CMainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "MainForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CMainForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CMainForm_FormClosed);
      this.mnuMain.ResumeLayout(false);
      this.mnuMain.PerformLayout();
      this.panMain.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip mnuMain;
      private System.Windows.Forms.ToolStripMenuItem miProgram;
      private System.Windows.Forms.ToolStripMenuItem miSetting;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
      private System.Windows.Forms.ToolStripMenuItem miExit;
      private System.Windows.Forms.ToolStripMenuItem miDataBase;
      private System.Windows.Forms.ToolStripMenuItem miSearch;
      private System.Windows.Forms.ToolStripMenuItem miHelp;
      private System.Windows.Forms.ToolStripMenuItem miTest;
      private System.Windows.Forms.ToolStripMenuItem miTest2;
      private System.Windows.Forms.ToolStripMenuItem miNew;
      private System.Windows.Forms.Panel panMain;
      private System.Windows.Forms.Button btnExit;
      private System.Windows.Forms.ToolStripMenuItem miWindow;
      private System.Windows.Forms.ToolStripMenuItem miCascade;
      private System.Windows.Forms.ToolStripMenuItem miVertical;
      private System.Windows.Forms.ToolStripMenuItem miHorizontal;
      private System.Windows.Forms.Button btnInfo;
      private System.Windows.Forms.Button btnSearch;
      private System.Windows.Forms.Button btnSetting;
      private System.Windows.Forms.Button btnNew;
      private System.Windows.Forms.ToolStripMenuItem miClose;
      private System.Windows.Forms.Button btnAlarm;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
      private System.Windows.Forms.ToolStripMenuItem miAlarm;
  }
}

