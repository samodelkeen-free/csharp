﻿#region CopyRight
//This code was originally developed 2012-04-03 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;

#endregion

namespace Aist.Engine.Classes
{
  /// <summary>
  /// Класс для работы с меню навигации по страницам выборки из БД
  /// </summary>
  public class CPageMenu
  {
    /// <summary>
    /// Конструктор
    /// </summary>
    public CPageMenu()
    {
      NumericList = new List<CPageMenuItem>();
      NumericList.Add(new CPageMenuItem { Text = "1", Value = 1 });
      NumericList.Add(new CPageMenuItem { Text = "2", Value = 2 });
      NumericList.Add(new CPageMenuItem { Text = "3", Value = 3 });
      this.LnkOther = new CPageMenuItem { Text = "..." };
      this.LnkBegin = new CPageMenuItem { Text = "Начало", Visible = true };
      this.LnkBack = new CPageMenuItem { Text = "<<" };
      this.LnkForward = new CPageMenuItem { Text = ">>" };
      this.LnkEnd = new CPageMenuItem { Text = "Конец", Visible = true };
    }

    /// <summary>
    /// Номер текущей страницы выборки
    /// </summary>
    public int CurrentPage = 1;
    /// <summary>
    /// Всего страниц в выборке
    /// </summary>
    public int PageNumber;

    /// <summary>
    /// Описание числовых пунктов меню
    /// </summary>
    public List<CPageMenuItem> NumericList
    {
      get;
      set;
    }
    /// <summary>
    /// Пункт меню "Начало"
    /// </summary>
    public CPageMenuItem LnkBegin
    {
      get;
      set;
    }
    /// <summary>
    /// Пункт меню "Конец"
    /// </summary>
    public CPageMenuItem LnkEnd
    {
      get;
      set;
    }
    /// <summary>
    /// Пункт меню "Вперед"
    /// </summary>
    public CPageMenuItem LnkForward
    {
      get;
      set;
    }
    /// <summary>
    /// Пункт меню "Назад"
    /// </summary>
    public CPageMenuItem LnkBack
    {
      get;
      set;
    }
    /// <summary>
    /// Пункт меню "Другие"
    /// </summary>
    public CPageMenuItem LnkOther
    {
      get;
      set;
    }

    /// <summary>
    /// Обновление состояния пунктов меню
    /// </summary>
    /// <param></param>
    public void Update()
    {
      bool bStayOnFlag = false; // Флаг необходимости перезаполнения списка страниц
      foreach (CPageMenuItem pPageMenuItem in NumericList) {
        if (pPageMenuItem.Value == CurrentPage) {
          pPageMenuItem.Active = true;
          bStayOnFlag = true;
        }
        else {
          pPageMenuItem.Active = false;
        }
      }
      if (PageNumber == 0) {
        bStayOnFlag = true;
      }
      if (!bStayOnFlag) {
        // Перезаполняем список страниц:
        if (CurrentPage == 0) {
          // Выбор пункта меню "Другие"
          int mm = PageNumber - NumericList[2].Value;
          if (mm > 3) {
            mm = 3;
          }
          int ii = NumericList[2].Value + mm - 2;
          CurrentPage = ii;
          foreach (CPageMenuItem pPageMenuItem in NumericList) {
            pPageMenuItem.Value = ii;
            ii++;
          }
        }
        else {
          // Смещение на одну позицию вправо или влево
          int ii;
          if (CurrentPage < NumericList[0].Value) {
            ii = CurrentPage;
          }
          else {
            ii = CurrentPage - NumericList.Count + 1;
          }
          foreach (CPageMenuItem pPageMenuItem in NumericList) {
            pPageMenuItem.Value = ii;
            ii++;
          }
        }
      }
      foreach (CPageMenuItem pPageMenuItem in NumericList) {
        pPageMenuItem.Visible = (pPageMenuItem.Value > 0 && pPageMenuItem.Value <= PageNumber);
        pPageMenuItem.Text = pPageMenuItem.Value.ToString();
      }
      LnkOther.Visible = (NumericList[2].Value < PageNumber);
      LnkBack.Visible = (CurrentPage > 1);
      LnkForward.Visible = (CurrentPage < PageNumber);
    }

    /// <summary>
    /// Обработка клика на пункте меню
    /// </summary>
    /// <param name="sItemName">Имя пункта меню</param>
    public void ItemClick(string sItemName)
    {
      switch (sItemName)
      {
        case "lnkBegin":
          CurrentPage = 1;
          break;
        case "lnkEnd":
          CurrentPage = PageNumber;
          break;
        case "lnkForward":
          if (CurrentPage < PageNumber) {
            CurrentPage ++;
          }
          break;
        case "lnkBack":
          if (CurrentPage > 1) {
            CurrentPage--;
          }
          break;
        case "lnkOne":
          h_numericItemClick(0);
          break;
        case "lnkTwo":
          h_numericItemClick(1);
          break;
        case "lnkThree":
          h_numericItemClick(2);
          break;
        case "lnkOther":
          CurrentPage = 0;
          break;
      }
      Update();
    }

    /// <summary>
    /// Обработка клика на числовом пункте меню
    /// </summary>
    /// <param name="iItemNo">Номер пункта меню</param>
    public void h_numericItemClick(int iItemNo)
    {
      CurrentPage = NumericList[iItemNo].Value;
    }

  }
}
