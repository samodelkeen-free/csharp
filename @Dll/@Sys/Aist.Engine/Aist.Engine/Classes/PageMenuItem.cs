﻿#region CopyRight
//This code was originally developed 2012-04-03 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Classes
{
  /// <summary>
  /// Класс описания пункта меню навигации по страницам выборки из БД
  /// </summary>
  public class CPageMenuItem
  {
    /// <summary>
    /// Значение
    /// </summary>
    public int Value
    {
      get;
      set;
    }
    /// <summary>
    /// Надпись
    /// </summary>
    public string Text
    {
      get;
      set;
    }
    /// <summary>
    /// Флаг видимости
    /// </summary>
    public bool Visible
    {
      get;
      set;
    }
    /// <summary>
    /// Флаг активности
    /// </summary>
    public bool Active
    {
      get;
      set;
    }

  }
}
