﻿#region CopyRight
//This code was originally developed 2012-06-20 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings
using System;
using System.Reflection;
using System.Windows.Forms;

#endregion

namespace Aist.Engine.Windows
{
  /// <summary>
  /// Класс для работы с дочерними формами и объектами
  /// </summary>
  abstract public class CChildFormProcessor
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    protected CChildFormProcessor()
    {

    }

    /// <summary>
    /// Ссылка на родительскую форму (для создания MDI-форм)
    /// </summary>
    public Form ParentForm
    {
      get;
      set;
    }

    /// <summary>
    /// Создает форму указанного в параметре класса
    /// </summary>
    /// <param name="sClassName">Имя класса формы</param>
    /// <param name="arConstructorParamType">Массив типов параметров конструктора</param>
    /// <param name="arConstructorParamValue">Массив значений параметров конструктора</param>
    public virtual Form CreateFormByClass(string sClassName, Type[] arConstructorParamType, object[] arConstructorParamValue)
    {
      Form result = null;
      Type tForm = Type.GetType(sClassName, false, true);
      if (tForm != null) {
        ConstructorInfo pConstructorInfo = tForm.GetConstructor(arConstructorParamType);
        result = (Form)pConstructorInfo.Invoke(arConstructorParamValue);
      }
      return result;      
    }

    /// <summary>
    /// Создает экземпляр указанного в параметре класса
    /// </summary>
    /// <param name="sClassName">Имя класса</param>
    /// <param name="arConstructorParamType">Массив типов параметров конструктора</param>
    /// <param name="arConstructorParamValue">Массив значений параметров конструктора</param>
    public virtual object CreateObjectByClass(string sClassName, Type[] arConstructorParamType, object[] arConstructorParamValue)
    {
      object result = null;
      Type tObject = Type.GetType(sClassName, false, true);
      if (tObject != null) {
        ConstructorInfo pConstructorInfo = tObject.GetConstructor(arConstructorParamType);
        result = pConstructorInfo.Invoke(arConstructorParamValue);
      }
      return result;
    }

  }
}
