﻿#region CopyRight
//This code was originally developed 2012-06-20 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Windows
{
  /// <summary>
  /// Класс для работы с дочерними формами
  /// </summary>
  public class CChildFormProcessorImplementation : CChildFormProcessor
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CChildFormProcessorImplementation() : base()
    {

    }
    /* TODO: Убрать! Перенес в базовый класс
    /// <summary>
    /// Создает форму указанного в параметре класса
    /// </summary>
    /// <param name="sClassName">Имя класса формы</param>
    /// <param name="arConstructorParamType">Массив типов параметров конструктора</param>
    /// <param name="arConstructorParamValue">Массив значений параметров конструктора</param>
    public override Form CreateFormByClass(string sClassName, Type[] arConstructorParamType, object[] arConstructorParamValue)
    {
      Form result = null;
      Type tForm = Type.GetType(sClassName, false, true);
      if (tForm != null) {
        ConstructorInfo pConstructorInfo = tForm.GetConstructor(arConstructorParamType);
        result = (Form)pConstructorInfo.Invoke(arConstructorParamValue);
      }
      return result;
    }
    */
  }
}
