﻿using System;
using System.Windows.Forms;

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// Класс для отображения ячейки типа CCheckedComboBox в DataGridView
  /// </summary>
	public class CDataGridViewCheckedComboBoxCell : DataGridViewComboBoxCell
	{
    /// <summary>
    /// Конструктор
    /// </summary>
    public CDataGridViewCheckedComboBoxCell()
			: base()
		{
      //this.Style.Format = "dd.MM.yyyy  HH:mm";
		}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rowIndex"></param>
    /// <param name="initialFormattedValue"></param>
    /// <param name="dataGridViewCellStyle"></param>
		public override void InitializeEditingControl(int rowIndex, object 
			initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			// Set the value of the editing control to the current cell value.
			base.InitializeEditingControl(rowIndex, initialFormattedValue, 
				dataGridViewCellStyle);
      CheckedComboBox ctl =
        DataGridView.EditingControl as CheckedComboBox;
			// Use the default row value when Value property is null.
			if (this.Value == null)
			{
				//ctl.Value = (String)this.DefaultNewRowValue;
			}
			else
			{
				//ctl.Value = (String)this.Value;
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override Type EditType
		{
			get
			{
        // Return the type of the editing control that CDataGridViewCheckedComboBoxCell uses.
        return typeof(CheckedComboBox);
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override Type ValueType
		{
			get
			{
        // Return the type of the value that CDataGridViewCheckedComboBoxCell contains.

				return typeof(String);
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override object DefaultNewRowValue
		{
			get
			{
				// Use the empty string as the default value.
				return String.Empty;
			}
		}
	}
}