﻿#region CopyRight
// *****************************************
// ** Author: Vincenzo Rossi              **
// ** Year  : 2008                        **
// ** Mail  : redmaster@tiscali.it        **
// **                                     **
// ** Released under                      **
// **   The Code Project Open License     **
// *****************************************
#endregion

#region Using

using System.ComponentModel;
using System.Drawing;

#endregion

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// A derivation of DateTimePicker allowing to change background color
  /// </summary>
  public partial class CAistDateTimePicker : System.Windows.Forms.DateTimePicker
  {
    private Color _backDisabledColor;
    const int WM_ERASEBKGND = 0x14;

    /// <summary>
    ///     Gets or sets the background color of the control
    /// </summary>
    [Browsable(true)]
    public override Color BackColor
    {
        get { return base.BackColor; }
        set { base.BackColor = value; }
    }

    /// <summary>
    ///     Gets or sets the background color of the control when disabled
    /// </summary>
    [Category("Appearance"), Description("The background color of the component when disabled")]
    [Browsable(true)]
    public Color BackDisabledColor
    {
        get { return _backDisabledColor; }
        set { _backDisabledColor = value; }
    }

    /*
    protected override void OnPaint(PaintEventArgs e)
    {
        base.OnPaint(e);
        e.Graphics.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);
        e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), 0, 0);
        e.Graphics.DrawImage(Properties.Resources.DropDownTriangle, this.ClientRectangle.X + this.ClientRectangle.Width - 16, this.ClientRectangle.Y, Properties.Resources.DropDownTriangle.Width, Properties.Resources.DropDownTriangle.Height);
    }
      */
    /// <summary>
    /// 
    /// </summary>
    /// <param name="m"></param>
    protected override void WndProc(ref System.Windows.Forms.Message m)
    {
        if (m.Msg == WM_ERASEBKGND)
        {
            Graphics g = Graphics.FromHdc(m.WParam);
            g.FillRectangle(new SolidBrush(BackColor), ClientRectangle);
            g.Dispose();
            return;
        }

        base.WndProc(ref m);
    }

    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CAistDateTimePicker() : base()
    {
        //InitializeComponent();
        //this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
        _backDisabledColor = Color.FromKnownColor(KnownColor.Control);
    }
  }
}
