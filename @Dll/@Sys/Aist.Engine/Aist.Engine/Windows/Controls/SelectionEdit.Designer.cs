#region CopyRight
//This code was originally developed 2012-08-14 by Egorov V.V.
//(c)2012 ������������� �������������� ������� � ����������
#endregion

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// �������� �������� ��� ������ � �������� ������ �� ������/������
  /// </summary>
  partial class SelectionEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.btnEllipsis = new System.Windows.Forms.Button();
      this.txtValue = new System.Windows.Forms.TextBox();
      this.btnClear = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnEllipsis
      // 
      this.btnEllipsis.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnEllipsis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnEllipsis.Location = new System.Drawing.Point(89, 0);
      this.btnEllipsis.Name = "btnEllipsis";
      this.btnEllipsis.Size = new System.Drawing.Size(25, 22);
      this.btnEllipsis.TabIndex = 0;
      this.btnEllipsis.Text = "...";
      this.btnEllipsis.UseVisualStyleBackColor = true;
      this.btnEllipsis.Click += new System.EventHandler(this.btnEllipsis_Click);
      // 
      // txtValue
      // 
      this.txtValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
      this.txtValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtValue.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtValue.Location = new System.Drawing.Point(0, 0);
      this.txtValue.Multiline = true;
      this.txtValue.Name = "txtValue";
      this.txtValue.Size = new System.Drawing.Size(114, 22);
      this.txtValue.TabIndex = 1;
      this.txtValue.TabStop = false;
      this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
      this.txtValue.Leave += new System.EventHandler(this.txtValue_Leave);
      // 
      // btnClear
      // 
      this.btnClear.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnClear.Location = new System.Drawing.Point(64, 0);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size(25, 22);
      this.btnClear.TabIndex = 2;
      this.btnClear.Text = "X";
      this.btnClear.UseVisualStyleBackColor = true;
      this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
      // 
      // SelectionEdit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.btnClear);
      this.Controls.Add(this.btnEllipsis);
      this.Controls.Add(this.txtValue);
      this.Name = "SelectionEdit";
      this.Size = new System.Drawing.Size(114, 22);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEllipsis;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Button btnClear;
    }
}
