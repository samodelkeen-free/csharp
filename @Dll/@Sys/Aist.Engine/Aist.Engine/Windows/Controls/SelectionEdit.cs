#region CopyRight
//This code was originally developed 2012-08-13 by Egorov V.V.
//(c)2012 ������������� �������������� ������� � ����������
#endregion

#region Using
using System;
using System.ComponentModel;
using System.Windows.Forms;
using Aist.Engine.Data.Structures;

#endregion

namespace Aist.Engine.Windows.Controls
{
  #region SelectionEdit
  /// <summary>
  /// ����� �������� ��� ������ � �������� ������ �������� �� ������/������
  /// </summary>
  public partial class SelectionEdit : UserControl, IDataGridViewEditingControl
  {
    private int _row;
    private DataGridView _dgv;
    private bool _valueChanged = false;
    private CSelectionProperty _pForEditSession;

    /// <summary>
    /// ����������� ������
    /// </summary>
    public SelectionEdit()
    {
      InitializeComponent();
    }

    /// <summary>
    /// ���� �� ������ ������ ����� ������ �� ������/������
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnEllipsis_Click(object sender, EventArgs e)
    {
      DataGridViewSelectionColumn pColumn = (DataGridViewSelectionColumn) this.EditingControlDataGridView.CurrentCell.OwningColumn;
      DataGridViewRow pRow = this.EditingControlDataGridView.CurrentCell.OwningRow;
      pColumn.PreSelectionFormMethod(pColumn.DataPropertyName, pRow.DataBoundItem, pColumn.SelectionFormDescription);
      if (pColumn.SelectionFormMethod(pColumn.SelectionFormDescription, _pForEditSession)) {
        this.txtValue.Text = _pForEditSession.Value.ToString();
        this.OnValueChanged();
        pColumn.PostSelectionFormMethod(pColumn.DataPropertyName, pRow.DataBoundItem, pColumn.SelectionFormDescription);
        this.txtValue.Focus();
      }
    }

    /// <summary>
    /// ���� �� ������ ������� �������� ������
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnClear_Click(object sender, EventArgs e)
    {
      _pForEditSession.Value = string.Empty;
      _pForEditSession.Identifier = null;
      this.txtValue.Text = _pForEditSession.Value.ToString();
      this.OnValueChanged();
    }

    /// <summary>
    /// �������� ���������� ���� ����������
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtValue_TextChanged(object sender, EventArgs e)
    {
      //this.OnValueChanged();
    }

    private void txtValue_Leave(object sender, EventArgs e)
    {
      DataGridViewSelectionColumn pColumn = (DataGridViewSelectionColumn)this.EditingControlDataGridView.CurrentCell.OwningColumn;
      object pIdentifier = pColumn.LeaveTextBoxMethod(_pForEditSession.Identifier, this.txtValue.Text, pColumn.SelectionFormDescription);
      if (pIdentifier != null) {
        _pForEditSession.Identifier = pIdentifier;
        _pForEditSession.Value = this.txtValue.Text;
        this.OnValueChanged();
      }
    }

    private void OnValueChanged()
    {
      this._valueChanged = true;
      //this._pForEditSession.Value = this.txtValue.Text;
      DataGridView dgv = this.EditingControlDataGridView;
      if (dgv != null)
        dgv.NotifyCurrentCellDirty(true);
    }

    /// <summary>
    /// �������������
    /// </summary>
    public void SetupControls(CSelectionProperty pSelectionProperty)
    {
      this._pForEditSession = new CSelectionProperty();
      _pForEditSession.Identifier = pSelectionProperty.Identifier;
      _pForEditSession.Value = pSelectionProperty.Value;
      this.txtValue.Text = pSelectionProperty.Value != null ? pSelectionProperty.Value.ToString() : string.Empty;
    }

    /// <summary>
    /// �������������� ����� ��� ���������� ������� Enter �� ���������
    /// </summary>
    /// <param name="keyData"></param>
    /// <returns></returns>
    protected override bool ProcessDialogKey(Keys keyData)
    {
      if (keyData == Keys.Enter) {
        if (!txtValue.Focused) {
          SendKeys.Send(" ");
          return true;
        }
        else {
          this.EditingControlDataGridView.Select();
          //this.SelectNextControl(this, true, true, false, true);
          //this.EditingControlDataGridView.EndEdit();
          SendKeys.Send("{RIGHT}");
          return true;
          //return base.ProcessDialogKey(Keys.Tab);
        }
      }
      else
        return base.ProcessDialogKey(keyData);
    }

    #region IDataGridViewEditingControl Members
    /// <summary>
    /// �������� ���������������� ��������� �������� ���������� � ������������ � �������� ������ ������.
    /// </summary>
    public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
    {
      this.txtValue.Font = dataGridViewCellStyle.Font;
    }
    /// <summary>
    /// ���������� ������������ ������ DataGridView.
    /// </summary>
    public DataGridView EditingControlDataGridView
    {
      get
      {
        return this._dgv;
      }
      set
      {
        this._dgv = value;
      }
    }
    /// <summary>
    /// Gets or sets the formatted representation of the current value of the text box control.
    /// </summary>
    public object EditingControlFormattedValue
    {
      get
      {
        return this._pForEditSession;
      }
      set
      {
        //nothing to do...
      }
    }
    /// <summary>
    /// ���������� ����� ������������� ������.
    /// </summary>
    public int EditingControlRowIndex
    {
      get
      {
        return this._row;
      }
      set
      {
        this._row = value;
      }
    }
    /// <summary>
    /// �������� ��� ������ ��������, �����������, ���������� �� �������� �������� ���������� �������������� 
    /// �� �������� ����������� ������.
    /// </summary>
    public bool EditingControlValueChanged
    {
      get
      {
        return this._valueChanged;
      }
      set
      {
        this._valueChanged = value;
      }
    }
    /// <summary>
    /// ��������� �� ����� ������ true, �� �������, ��� ����� ������������ ������ ��������� ������.
    /// </summary>
    public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
    {
      switch (keyData & Keys.KeyCode) {
        case Keys.Down:
        case Keys.Right:
        case Keys.Left:
        case Keys.Up:
        case Keys.Home:
        case Keys.End:
          return true;
        default:
          return false;
      }
    }
    /// <summary>
    /// �������� ������
    /// </summary>
    public Cursor EditingPanelCursor
    {
      get { return base.Cursor; }
    }
    /// <summary>
    /// �������� ��������������� �������� ������.
    /// </summary>
    public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
    {
      return this.EditingControlFormattedValue;
    }
    /// <summary>
    /// �������������� ��������� ������ � ��������������.
    /// </summary>
    /// <param name="selectAll">�������� true, ����� ������� ��� ���������� ������, � �������� false � ��������� ������.</param>
    public void PrepareEditingControlForEdit(bool selectAll) { }
    /// <summary>
    /// �������� ��������, �����������, ����� �� ���������� ���������� ������ � ������ ��������� �� ��������.
    /// </summary>
    public bool RepositionEditingControlOnValueChange { get { return false; } }

    #endregion

  }
  #endregion

  #region DataGridViewSelectionCell
  /// <summary>
  /// ����� ��������� ������ ������ �������� �� ������/������
  /// </summary>
  public class DataGridViewSelectionCell : DataGridViewTextBoxCell
  {
    private const string DEFAULT_STRING = "";
    private int _heightOfRowBeforeEditMode;

    /// <summary>
    /// ����������� ������
    /// </summary>
    public DataGridViewSelectionCell() : base() { }

    /// <summary>
    /// �������������
    /// </summary>
    /// <param name="rowIndex"></param>
    /// <param name="initialFormattedValue"></param>
    /// <param name="dataGridViewCellStyle"></param>
    public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
    {
      base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
      SelectionEdit pSelectionEdit = this.DataGridView.EditingControl as SelectionEdit;
      this._heightOfRowBeforeEditMode = this.OwningRow.Height;
      this.OwningRow.Height = pSelectionEdit.Height;
      CSelectionProperty pSelectionProperty = this.Value as CSelectionProperty;
      if (pSelectionProperty == null)
        pSelectionProperty = new CSelectionProperty();
      pSelectionEdit.SetupControls(pSelectionProperty);
    }
    /// <summary>
    /// ������� ������� ���������� "���� �����" ������ �� ������� DataGridView.
    /// </summary>
    public override void DetachEditingControl()
    {
      if (this._heightOfRowBeforeEditMode > 0)
        this.OwningRow.Height = this._heightOfRowBeforeEditMode;
      base.DetachEditingControl();
    }
    /// <summary>
    /// ���������� ����� �������� � ������
    /// </summary>
    public override Type EditType
    {
      get
      {
        return typeof(SelectionEdit);
      }
    }
    /// <summary>
    /// ���������� ��� �������� � ������
    /// </summary>
    public override Type ValueType
    {
      get
      {
        return typeof(CSelectionProperty);
      }
    }
    /// <summary>
    /// ���������� ��� ������������������ �������� � ������
    /// </summary>
    public override Type FormattedValueType
    {
      get
      {
        return typeof(CSelectionProperty);
      }
    }
    /// <summary>
    /// ���������� �������� � ������ �� ���������
    /// </summary>
    public override object DefaultNewRowValue
    {
      get
      {
        return DEFAULT_STRING;
      }
    }
    /// <summary>
    /// ���������� ����������������� �������� � ������
    /// </summary>
    /// <param name="value"></param>
    /// <param name="rowIndex"></param>
    /// <param name="cellStyle"></param>
    /// <param name="valueTypeConverter"></param>
    /// <param name="formattedValueTypeConverter"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
    {
      if (value == null)
        return DEFAULT_STRING;
      else
        return TypeDescriptor.GetConverter(value).ConvertToString(value);
    }

  }
  #endregion

  #region DataGridViewSelectionColumn
  /// <summary>
  /// ����� ������� ������ �������� �� ������/������
  /// </summary>
  public class DataGridViewSelectionColumn : DataGridViewColumn
  {
    #region Delegates
    /// <summary>
    /// ������� ������ ��������������� ��� �������� ����� ������ �������� �� ������/������
    /// </summary>
    /// <param name="sFieldName">������������ ����</param>
    /// <param name="pGridItem">������ ������� ������ �����</param>
    /// <param name="pSelectionFormList">������ ������ ��������</param>
    /// <returns></returns>
    public delegate void FPreSelectionFormCall(string sFieldName, object pGridItem, object pSelectionFormList);
    /// <summary>
    /// ������� ������ ����� ������ �������� �� ������
    /// </summary>
    /// <param name="pSelectionFormDescription">�������� ������ ��� ������ ��������� ��� ����� ������</param>
    /// <param name="pSelectionProperty">���������� ������ ��������������</param>
    /// <returns>���� ������� ��������, �� ���������� "true"</returns>
    public delegate bool FSelectionFormCall(object pSelectionFormDescription, CSelectionProperty pSelectionProperty);
    /// <summary>
    /// ������� ������ ��������������� ����� ������ �������� �� ������/������
    /// </summary>
    /// <param name="sFieldName">������������ ����</param>
    /// <param name="pGridItem">������ ������� ������ �����</param>
    /// <param name="pSelectionFormList">������ ������ ��������</param>
    /// <returns></returns>
    public delegate void FPostSelectionFormCall(string sFieldName, object pGridItem, object pSelectionFormList);
    /// <summary>
    /// ������� ������ ��������������� ������� ������ �� TextBox'a �������� ������
    /// </summary>
    /// <param name="pIdentifier"></param>
    /// <param name="pValue"></param>
    /// <param name="pSelectionFormList">������ ������ ��������</param>
    /// <returns>������������� �������� ������</returns>
    public delegate object FLeaveTextBoxCall(object pIdentifier, object pValue, object pSelectionFormList);

    #endregion

    /// <summary>
    /// ����������� ������
    /// </summary>
    public DataGridViewSelectionColumn() : base(new DataGridViewSelectionCell()) { }

    /// <summary>
    /// �������� � ������ ������ ����� ������
    /// </summary>
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof(DataGridViewSelectionCell)))
          throw new InvalidCastException("Cell must be a DataGridViewSelectionCell");
        base.CellTemplate = value;
      }
    }

    /// <summary>
    /// ������ ������� ������ ��������������� ��� �������� ����� ������ �������� �� ������/������
    /// </summary>
    public FPreSelectionFormCall PreSelectionFormMethod { get; set; }
    /// <summary>
    /// ������ ������� ������ ����� ������ �������� �� ������/������
    /// </summary>
    public FSelectionFormCall SelectionFormMethod { get; set; }
    /// <summary>
    /// ������ ������� ������ ��������������� ����� ������ �������� �� ������/������
    /// </summary>
    public FPostSelectionFormCall PostSelectionFormMethod { get; set; }
    /// <summary>
    /// ������ ������� ������ ��������������� ������� ������ �� TextBox'a �������� ������
    /// </summary>
    public FLeaveTextBoxCall LeaveTextBoxMethod { get; set; }
    /// <summary>
    /// ������ �������� ������ ��� ������ ��������� ��� ����� ������
    /// </summary>
    public object SelectionFormDescription { get; set; }

  }

  #endregion

}