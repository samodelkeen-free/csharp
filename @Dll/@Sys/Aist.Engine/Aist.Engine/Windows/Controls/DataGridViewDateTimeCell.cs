﻿using System;
using System.Windows.Forms;

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// Класс для отображения ячейки типа DateTime в DataGridView
  /// </summary>
	public class CDataGridViewDateTimeCell : DataGridViewTextBoxCell
	{
    /// <summary>
    /// Конструктор
    /// </summary>
    public CDataGridViewDateTimeCell()
			: base()
		{
			// Use the long date format.
      this.Style.Format = "dd.MM.yyyy  HH:mm";
		}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rowIndex"></param>
    /// <param name="initialFormattedValue"></param>
    /// <param name="dataGridViewCellStyle"></param>
		public override void InitializeEditingControl(int rowIndex, object 
			initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			// Set the value of the editing control to the current cell value.
			base.InitializeEditingControl(rowIndex, initialFormattedValue, 
				dataGridViewCellStyle);
      CDateTimeEditingControl ctl =
        DataGridView.EditingControl as CDateTimeEditingControl;
			// Use the default row value when Value property is null.
			if (this.Value == null)
			{
				ctl.Value = (DateTime)this.DefaultNewRowValue;
			}
			else
			{
				ctl.Value = (DateTime)this.Value;
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override Type EditType
		{
			get
			{
        // Return the type of the editing control that CDataGridViewDateTimeCell uses.
        return typeof(CDateTimeEditingControl);
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override Type ValueType
		{
			get
			{
        // Return the type of the value that CDataGridViewDateTimeCell contains.

				return typeof(DateTime);
			}
		}
    /// <summary>
    /// 
    /// </summary>
		public override object DefaultNewRowValue
		{
			get
			{
				// Use the current date and time as the default value.
				return DateTime.Now;
			}
		}
	}
}