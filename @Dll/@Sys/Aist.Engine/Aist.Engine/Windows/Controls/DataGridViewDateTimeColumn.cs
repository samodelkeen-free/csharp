﻿using System;
using System.Windows.Forms;

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// Класс для отображения колонки типа DateTime в DataGridView
  /// </summary>
	public class CDataGridViewDateTimeColumn : DataGridViewColumn
	{
    /// <summary>
    /// Конструктор
    /// </summary>
    public CDataGridViewDateTimeColumn()
      : base(new CDataGridViewDateTimeCell())
		{
		}
    /// <summary>
    /// 
    /// </summary>
		public override DataGridViewCell CellTemplate
		{
			get
			{
				return base.CellTemplate;
			}
			set
			{
        // Ensure that the cell used for the template is a CDataGridViewDateTimeCell.
				if (value != null &&
          !value.GetType().IsAssignableFrom(typeof(CDataGridViewDateTimeCell)))
				{
          throw new InvalidCastException("Must be a CDataGridViewDateTimeCell");
				}
				base.CellTemplate = value;
			}
		}
	}
}