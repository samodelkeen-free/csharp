﻿using System;
using System.Windows.Forms;

namespace Aist.Engine.Windows.Controls
{
  /// <summary>
  /// Класс для отображения колонки типа CCheckedComboBox в DataGridView
  /// </summary>
  public class CDataGridViewCheckedComboBoxColumn : DataGridViewComboBoxColumn
	{
    /// <summary>
    /// Конструктор
    /// </summary>
    public CDataGridViewCheckedComboBoxColumn() : base()
		{
		}
    /// <summary>
    /// 
    /// </summary>
		public override DataGridViewCell CellTemplate
		{
			get
			{
				return base.CellTemplate;
			}
			set
			{
        // Ensure that the cell used for the template is a CDataGridViewCheckedComboBoxCell.
				if (value != null &&
          !value.GetType().IsAssignableFrom(typeof(CDataGridViewCheckedComboBoxCell)))
				{
          throw new InvalidCastException("Must be a CDataGridViewCheckedComboBoxCell");
				}
				base.CellTemplate = value;
			}
		}
	}
}