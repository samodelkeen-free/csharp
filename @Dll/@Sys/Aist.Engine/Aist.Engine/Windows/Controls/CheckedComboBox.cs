#region CopyRight
// This code was originally developed Stelios Alexandrakis, 22 Nov 2008
// https://www.codeproject.com/articles/31105/a-combobox-with-a-checkedlistbox-as-a-dropdown
// Modified by Vladimir Egorov 2016-12-20
#endregion

using System;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace Aist.Engine.Windows.Controls
{
  public class CheckedComboBox : ComboBox {
    /// <summary>
    /// Internal class to represent the dropdown list of the CheckedComboBox
    /// </summary>
    internal class Dropdown : Form {

      // ---------------------------------- internal class CCBoxEventArgs --------------------------------------------
      /// <summary>
      /// Custom EventArgs encapsulating value as to whether the combo box value(s) should be assignd to or not.
      /// </summary>
      internal class CCBoxEventArgs : EventArgs {

        private bool m_bAssignValues;

        public bool AssignValues {
          get { return m_bAssignValues; }
          set { m_bAssignValues = value; }
        }

        private EventArgs m_pEventArgs;

        public EventArgs EventArgs {
          get { return m_pEventArgs; }
          set { m_pEventArgs = value; }
        }

        public CCBoxEventArgs(EventArgs e, bool assignValues) : base() {
          this.m_pEventArgs = e;
          this.m_bAssignValues = assignValues;
        }
      } // end internal class CCBoxEventArgs

      // ---------------------------------- internal class CustomCheckedListBox --------------------------------------------

      /// <summary>
      /// A custom CheckedListBox being shown within the dropdown form representing the dropdown list of the CheckedComboBox.
      /// </summary>
      internal class CustomCheckedListBox : CheckedListBox {

        private int m_iCurSelIndex = -1;

        public CustomCheckedListBox() : base() {
          this.SelectionMode = SelectionMode.One;
          //this.HorizontalScrollbar = false;
          this.MultiColumn = true;
          //this.ScrollAlwaysVisible = true;
        }

        /// <summary>
        /// Intercepts the keyboard input, [Enter] confirms a selection and [Esc] cancels it.
        /// </summary>
        /// <param name="e">The Key event arguments</param>
        protected override void OnKeyDown(KeyEventArgs e) {
          if (e.KeyCode == Keys.Enter) {
            // Enact selection.
            ((CheckedComboBox.Dropdown) Parent).OnDeactivate(new CCBoxEventArgs(null, true));
            e.Handled = true;

          } else if (e.KeyCode == Keys.Escape) {
            // Cancel selection.
            ((CheckedComboBox.Dropdown) Parent).OnDeactivate(new CCBoxEventArgs(null, false));
            e.Handled = true;

          } else if (e.KeyCode == Keys.Delete) {
            // Delete unckecks all, [Shift + Delete] checks all.
            for (int i = 0; i < Items.Count; i++) {
              SetItemChecked(i, e.Shift);
            }
            e.Handled = true;
          }
          // If no Enter or Esc keys presses, let the base class handle it.
          base.OnKeyDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e) {
          base.OnMouseMove(e);
          int index = IndexFromPoint(e.Location);

          Debug.WriteLine("Mouse over item: " + (index >= 0 ? GetItemText(Items[index]) : "None"));
          if ((index >= 0) && (index != m_iCurSelIndex)) {
              m_iCurSelIndex = index;
              SetSelected(index, true);
          }

        }

      } // end internal class CustomCheckedListBox

      // --------------------------------------------------------------------------------------------------------

      // ********************************************* Data *********************************************

      private readonly CheckedComboBox m_ccbParent;

      // Keeps track of whether checked item(s) changed, hence the value of the CheckedComboBox as a whole changed.
      // This is simply done via maintaining the old string-representation of the value(s) and the new one and comparing them!
      private string m_sOldStrValue = "";

      public bool ValueChanged {
        get {
          string sNewStrValue = m_ccbParent.Text;
          if ((m_sOldStrValue.Length > 0) && (sNewStrValue.Length > 0)) {
            return (m_sOldStrValue.CompareTo(sNewStrValue) != 0);
          } else {
            return (m_sOldStrValue.Length != sNewStrValue.Length);
          }
        }
      }

      // Array holding the checked states of the items. This will be used to reverse any changes if user cancels selection.
      bool[] m_arCheckedState;

      // Whether the dropdown is closed.
      private bool m_bDropdownClosed = true;

      private CustomCheckedListBox m_cclb;

      public CustomCheckedListBox List {
        get { return m_cclb; }
        set { m_cclb = value; }
      }

      // ********************************************* Construction *********************************************

      public Dropdown(CheckedComboBox ccbParent) {
        this.m_ccbParent = ccbParent;
        InitializeComponent();
        this.ShowInTaskbar = false;
        // Add a handler to notify our parent of ItemCheck events.
        this.m_cclb.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cclb_ItemCheck);
      }

      // ********************************************* Methods *********************************************

      // Create a CustomCheckedListBox which fills up the entire form area.
      private void InitializeComponent() {
        this.m_cclb = new CustomCheckedListBox();
        this.SuspendLayout();
        // 
        // cclb
        // 
        this.m_cclb.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.m_cclb.Dock = System.Windows.Forms.DockStyle.Fill;
        this.m_cclb.FormattingEnabled = true;
        this.m_cclb.Location = new System.Drawing.Point(0, 0);
        this.m_cclb.Name = "cclb";
        this.m_cclb.Size = new System.Drawing.Size(47, 15);
        this.m_cclb.TabIndex = 0;
        //
        // Dropdown
        //
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.SystemColors.Menu;
        this.ClientSize = new System.Drawing.Size(47, 16);
        this.ControlBox = false;
        this.Controls.Add(this.m_cclb);
        this.ForeColor = System.Drawing.SystemColors.ControlText;
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        this.MinimizeBox = false;
        this.Name = "ccbParent";
        this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
        this.ResumeLayout(false);
      }

      public string GetCheckedItemsStringValue() {
        StringBuilder sb = new StringBuilder("");
        for (int ii = 0; ii < m_cclb.CheckedItems.Count; ii++) {                  
          sb.Append(m_cclb.GetItemText(m_cclb.CheckedItems[ii])).Append(m_ccbParent.ValueSeparator);
        }
        if (sb.Length > 0) {
          sb.Remove(sb.Length - m_ccbParent.ValueSeparator.Length, m_ccbParent.ValueSeparator.Length);
        }
        return sb.ToString();
      }

      /// <summary>
      /// Closes the dropdown portion and enacts any changes according to the specified boolean parameter.
      /// NOTE: even though the caller might ask for changes to be enacted, this doesn't necessarily mean
      ///       that any changes have occurred as such. Caller should check the ValueChanged property of the
      ///       CheckedComboBox (after the dropdown has closed) to determine any actual value changes.
      /// </summary>
      /// <param name="enactChanges"></param>
      public void CloseDropdown(bool enactChanges) {
        if (m_bDropdownClosed) {
          return;
        }                
        Debug.WriteLine("CloseDropdown");
        // Perform the actual selection and display of checked items.
        if (enactChanges) {
          m_ccbParent.SelectedIndex = -1;                    
          // Set the text portion equal to the string comprising all checked items (if any, otherwise empty!).
          m_ccbParent.Text = GetCheckedItemsStringValue();

        } else {
          // Caller cancelled selection - need to restore the checked items to their original state.
          for (int ii = 0; ii < m_cclb.Items.Count; ii++) {
              m_cclb.SetItemChecked(ii, m_arCheckedState[ii]);
          }
        }
        // From now on the dropdown is considered closed. We set the flag here to prevent OnDeactivate() calling
        // this method once again after hiding this window.
        m_bDropdownClosed = true;
        // Set the focus to our parent CheckedComboBox and hide the dropdown check list.
        m_ccbParent.Focus();
        this.Hide();
        // Notify CheckedComboBox that its dropdown is closed. (NOTE: it does not matter which parameters we pass to
        // OnDropDownClosed() as long as the argument is CCBoxEventArgs so that the method knows the notification has
        // come from our code and not from the framework).
        m_ccbParent.OnDropDownClosed(new CCBoxEventArgs(null, false));
      }

      protected override void OnActivated(EventArgs e) {
        Debug.WriteLine("OnActivated");
        base.OnActivated(e);
        m_bDropdownClosed = false;
        // Assign the old string value to compare with the new value for any changes.
        m_sOldStrValue = m_ccbParent.Text;
        // Make a copy of the checked state of each item, in cace caller cancels selection.
        m_arCheckedState = new bool[m_cclb.Items.Count];
        for (int ii = 0; ii < m_cclb.Items.Count; ii++) {
          m_arCheckedState[ii] = m_cclb.GetItemChecked(ii);
        }
      }

      protected override void OnDeactivate(EventArgs e) {
        Debug.WriteLine("OnDeactivate");
        base.OnDeactivate(e);
        CCBoxEventArgs ce = e as CCBoxEventArgs;
        if (ce != null) {
          CloseDropdown(ce.AssignValues);

        } else {
          // If not custom event arguments passed, means that this method was called from the
          // framework. We assume that the checked values should be registered regardless.
          CloseDropdown(true);
        }
      }

      private void cclb_ItemCheck(object sender, ItemCheckEventArgs e) {
        if (m_ccbParent.ItemCheck != null) {
          m_ccbParent.ItemCheck(sender, e);
        }
      }

    } // end internal class Dropdown

    // ******************************** Data ********************************
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private readonly System.ComponentModel.IContainer m_components = null;
    // A form-derived object representing the drop-down list of the checked combo box.
    private readonly Dropdown m_dropdown;

    // The valueSeparator character(s) between the ticked elements as they appear in the 
    // text portion of the CheckedComboBox.
    private string m_sValueSeparator;

    public string ValueSeparator {
      get { return m_sValueSeparator; }
      set { m_sValueSeparator = value; }
    }

    public bool CheckOnClick {
      get { return m_dropdown.List.CheckOnClick; }
      set { m_dropdown.List.CheckOnClick = value; }
    }

    public new string DisplayMember {
      get { return m_dropdown.List.DisplayMember; }
      set { m_dropdown.List.DisplayMember = value; }
    }

    public new CheckedListBox.ObjectCollection Items {
      get { return m_dropdown.List.Items; }
    }

    public CheckedListBox.CheckedItemCollection CheckedItems {
      get { return m_dropdown.List.CheckedItems; }
    }
        
    public CheckedListBox.CheckedIndexCollection CheckedIndices {
      get { return m_dropdown.List.CheckedIndices; }
    }

    public bool ValueChanged {
      get { return m_dropdown.ValueChanged; }
    }

    public int ColumnWidth {
      get { return m_dropdown.List.ColumnWidth; }
      set { m_dropdown.List.ColumnWidth = value; }
    }

    // Event handler for when an item check state changes.
    public event ItemCheckEventHandler ItemCheck;
        
    // ******************************** Construction ********************************

    public CheckedComboBox() : base() {
      // We want to do the drawing of the dropdown.
      this.DrawMode = DrawMode.OwnerDrawVariable;
      // Default value separator.
      this.m_sValueSeparator = ", ";
      // This prevents the actual ComboBox dropdown to show, although it's not strickly-speaking necessary.
      // But including this remove a slight flickering just before our dropdown appears (which is caused by
      // the empty-dropdown list of the ComboBox which is displayed for fractions of a second).
      this.DropDownHeight = 1;            
      // This is the default setting - text portion is editable and user must click the arrow button
      // to see the list portion. Although we don't want to allow the user to edit the text portion
      // the DropDownList style is not being used because for some reason it wouldn't allow the text
      // portion to be programmatically set. Hence we set it as editable but disable keyboard input (see below).
      this.DropDownStyle = ComboBoxStyle.DropDown;
      this.m_dropdown = new Dropdown(this);
      // CheckOnClick style for the dropdown (NOTE: must be set after dropdown is created).
      this.CheckOnClick = true;
    }

    // ******************************** Operations ********************************

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (m_components != null)) {
        m_components.Dispose();
      }
      base.Dispose(disposing);
    }        

    protected override void OnDropDown(EventArgs e) {
      base.OnDropDown(e);
      DoDropDown();    
    }

    private void DoDropDown() {
      if (!m_dropdown.Visible) {
        Rectangle rect = RectangleToScreen(this.ClientRectangle);
        m_dropdown.Location = new Point(rect.X, rect.Y + this.Size.Height);
        int count = m_dropdown.List.Items.Count;
        if (count > this.MaxDropDownItems) {
          count = this.MaxDropDownItems;
        } else if (count == 0) {
          count = 1;
        }
        m_dropdown.Size = new Size(this.Size.Width, (m_dropdown.List.ItemHeight + 0) * (count + 0) +
           (m_dropdown.List.Items.Count / count * m_dropdown.List.ColumnWidth > this.Width ? m_dropdown.List.ItemHeight + SystemInformation.HorizontalScrollBarHeight + 1 : 2));
        m_dropdown.Show(this);
        m_dropdown.Activate();
        m_dropdown.List.Focus();
      }
    }

    protected override void OnDropDownClosed(EventArgs e) {
      // Call the handlers for this event only if the call comes from our code - NOT the framework's!
      // NOTE: that is because the events were being fired in a wrong order, due to the actual dropdown list
      //       of the ComboBox which lies underneath our dropdown and gets involved every time.
      if (e is Dropdown.CCBoxEventArgs) {
        base.OnDropDownClosed(e);
      }
    }

    protected override void OnKeyDown(KeyEventArgs e) {
      if (e.KeyCode == Keys.Down) {
        // Signal that the dropdown is "down". This is required so that the behaviour of the dropdown is the same
        // when it is a result of user pressing the Down_Arrow (which we handle and the framework wouldn't know that
        // the list portion is down unless we tell it so).
        // NOTE: all that so the DropDownClosed event fires correctly!                
        OnDropDown(null);
      }
      // Make sure that certain keys or combinations are not blocked.
      e.Handled = !e.Alt && !(e.KeyCode == Keys.Tab) &&
        !((e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right) || (e.KeyCode == Keys.Home) || (e.KeyCode == Keys.End));

      base.OnKeyDown(e);
    }

    protected override void OnKeyPress(KeyPressEventArgs e) {
      e.Handled = true;
      base.OnKeyPress(e);
    }

    public bool GetItemChecked(int index) {
      if (index < 0 || index > Items.Count) {
        throw new ArgumentOutOfRangeException("index", "value out of range");
      } else {
        return m_dropdown.List.GetItemChecked(index);
      }
    }

    public void SetItemChecked(int index, bool isChecked) {
      if (index < 0 || index > Items.Count) {
        throw new ArgumentOutOfRangeException("index", "value out of range");
      } else {
        m_dropdown.List.SetItemChecked(index, isChecked);
        // Need to update the Text.
        this.Text = m_dropdown.GetCheckedItemsStringValue();
      }
    }

    public CheckState GetItemCheckState(int index) {
      if (index < 0 || index > Items.Count) {
        throw new ArgumentOutOfRangeException("index", "value out of range");
      } else {
        return m_dropdown.List.GetItemCheckState(index);
      }
    }

    public void SetItemCheckState(int index, CheckState state) {
      if (index < 0 || index > Items.Count) {
        throw new ArgumentOutOfRangeException("index", "value out of range");
      } else {
        m_dropdown.List.SetItemCheckState(index, state);
        // Need to update the Text.
        this.Text = m_dropdown.GetCheckedItemsStringValue();
      }
    }

  } // end public class CheckedComboBox

}