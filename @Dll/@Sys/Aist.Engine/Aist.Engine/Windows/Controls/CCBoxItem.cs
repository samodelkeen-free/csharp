#region CopyRight
// This code was originally developed Stelios Alexandrakis, 22 Nov 2008
// https://www.codeproject.com/articles/31105/a-combobox-with-a-checkedlistbox-as-a-dropdown
#endregion

using Aist.Engine.Data.Structures;

namespace Aist.Engine.Windows.Controls
{
  public class CCBoxItem : CSelectionItem
  {

    private int m_iValue;
    public int Value {
      get { return m_iValue; }
      set { m_iValue = value; }
    }
        
    private string m_sText;
    public string Text {
      get { return m_sText; }
      set { m_sText = value; }
    }

    public CCBoxItem() {
    }

    public CCBoxItem(string name, int value) {
      this.m_sText = name;
      this.m_iValue = value;
    }

    public override string ToString() {
      /* EVV: ��� �������� CheckedListBox ������� ������ m_sText
      return string.Format("name: '{0}', value: {1}", m_sText, m_iValue);
      */
      return m_sText;
    }
  }
}