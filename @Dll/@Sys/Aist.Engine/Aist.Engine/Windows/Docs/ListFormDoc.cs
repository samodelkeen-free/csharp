﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using Aist.Engine.Data.Structures;
using Sfo.Sys.Config;
using Sfo.Sys.Doc;

#endregion

namespace Aist.Engine.Windows.Docs
{
  #region Class CListFormDoc

  /// <summary>
  /// Документ формы
  /// </summary>
  public class CListFormDoc: CDoc
  {
    #region Variable declarations

    /// <summary>
    /// Конфигуратор
    /// </summary>
    private CAppConfig m_pAppConfig;
    /* EVV: TODO Убрать!
    /// <summary>
    /// Описание элемента списка объекта типа DataGridView
    /// </summary>
    private CGridItem m_pGridItem;
    */
    /// <summary>
    /// Описание элемента управления типа DataGridView
    /// </summary>
    private CGridDescription m_pGridDescription;
    /// <summary>
    /// Текущий элемент в массиве найденных значений
    /// </summary>
    internal int FoundIdentifierListCurrentIndex = -1;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pGridDescription">Описание элемента управления типа DataGridView</param>
    public CListFormDoc(CAppConfig pAppConfig, object pGridDescription)
    {
      m_pAppConfig = pAppConfig;
      //TODO: Разобраться с m_pLogWriter
      //m_pLogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
      m_pGridDescription = (CGridDescription) pGridDescription;
      m_pGridDescription.Changed = false;
    }

    #endregion
    
    #region Public methods
    #endregion

    #region Public properties

    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CAppConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }
    /* EVV: TODO Убрать!
    /// <summary>
    /// Получает объект строки Grid`a
    /// </summary>
    public CGridItem GridItem
    {
      get
      {
        return m_pGridItem;
      }
    }
    */
    /// <summary>
    /// Получает описание Grid`a
    /// </summary>
    public CGridDescription GridDescription
    {
      get
      {
        return m_pGridDescription;
      }
    }

    #endregion

    #region CDoc methods

    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion
  }

  #endregion
}
