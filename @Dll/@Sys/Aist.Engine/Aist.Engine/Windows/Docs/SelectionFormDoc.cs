﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using Aist.Engine.Data.Structures;
using Sfo.Sys.Config;
using Sfo.Sys.Doc;

#endregion

namespace Aist.Engine.Windows.Docs
{
  #region CSelectionFormDoc class
  /// <summary>
  /// Документ формы
  /// </summary>
  public class CSelectionFormDoc: CDoc
  {
    #region Variable declarations
    /// <summary>
    /// Конфигуратор
    /// </summary>
    private CAppConfig m_pAppConfig;
    /// <summary>
    /// Список элементов
    /// </summary>
    private CSelectionFormList m_pElementList;

    #endregion

    #region Class constructors
    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConf"></param>
    /// <param name="pElementList">Список элементов</param>
    public CSelectionFormDoc(CAppConfig pAppConf, object pElementList)
    {
      m_pAppConfig = pAppConf;
      m_pElementList = (CSelectionFormList)pElementList;
    }

    #endregion

    #region CSelectionFormDoc public/internal methods
    /// <summary>
    /// Отмечает элемент, либо снимает с него отметку
    /// </summary>
    /// <param name="pSelectionItem">Объект элемента списка</param>
    internal void CheckElement(CSelectionItem pSelectionItem)
    {
      if (m_pElementList.OnlyElementMode) {
        if (!pSelectionItem.Checked) {
          m_pElementList.UnCheckAll();
        }
      }
      pSelectionItem.Checked = !pSelectionItem.Checked;
    }

    #endregion

    #region CSelectionFormDoc public/internal properties
    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CAppConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }
    /// <summary>
    /// Получает список элементов
    /// </summary>
    internal CSelectionFormList ElementList
    {
      get
      {
        return m_pElementList;
      }
    }

    #endregion

    #region CDoc methods
    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion
  }

  #endregion
}
