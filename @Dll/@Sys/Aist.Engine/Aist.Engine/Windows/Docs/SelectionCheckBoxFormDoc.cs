﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using Aist.Engine.Data.Structures;
using Sfo.Sys.Config;
using Sfo.Sys.Doc;

#endregion

namespace Aist.Engine.Windows.Docs
{
  #region CSelectionCheckBoxFormDoc class
  /// <summary>
  /// Документ формы
  /// </summary>
  public class CSelectionCheckBoxFormDoc: CDoc
  {
    #region Variable declarations

    /// <summary>
    /// Конфигуратор
    /// </summary>
    private readonly CAppConfig m_pAppConfig;
    /// <summary>
    /// Список элементов
    /// </summary>
    private CSelectionFormCheckBox m_pElementList;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pElementList">Список элементов</param>
    public CSelectionCheckBoxFormDoc(CAppConfig pAppConfig, object pElementList)
    {
      m_pAppConfig = pAppConfig;
      m_pElementList = (CSelectionFormCheckBox)pElementList;
    }

    #endregion
    
    #region Public methods

    /// <summary>
    /// Получает список элементов
    /// </summary>
    internal CSelectionFormCheckBox ElementList
    {
      get
      {
        return m_pElementList;
      }
    }

    #endregion

    #region Public properties

    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CAppConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }

    #endregion

    #region CDoc methods
    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion
  }

  #endregion
}
