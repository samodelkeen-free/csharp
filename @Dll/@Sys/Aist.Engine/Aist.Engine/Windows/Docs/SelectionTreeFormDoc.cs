﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using Aist.Engine.Data.Structures;
using Sfo.Sys.Config;
using Sfo.Sys.Doc;

#endregion

namespace Aist.Engine.Windows.Docs
{
  #region CSelectionTreeFormDoc class
  /// <summary>
  /// Документ формы
  /// </summary>
  public class CSelectionTreeFormDoc: CDoc
  {
    #region Variable declarations
    /// <summary>
    /// Конфигуратор
    /// </summary>
    private CAppConfig m_pAppConfig;
    /// <summary>
    /// Дерево элементов
    /// </summary>
    private CSelectionFormTree m_pElementTree;

    #endregion

    #region Class constructors
    /// <summary>
    /// Конструктор документа формы
    /// </summary>
    /// <param name="pAppConf"></param>
    /// <param name="pElementTree">Дерево элементов</param>
    public CSelectionTreeFormDoc(CAppConfig pAppConf, object pElementTree)
    {
      m_pAppConfig = pAppConf;
      m_pElementTree = (CSelectionFormTree)pElementTree;
    }

    #endregion

    #region CSelectionTreeFormDoc public/internal methods
    /// <summary>
    /// Отмечает элемент, либо снимает с него отметку
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    /// <param name="bCheckState">Статус элемента дерева</param>
    internal void CheckElement(object pIdentifier, bool bCheckState)
    {
      CSelectionItem pSelectionItem = m_pElementTree.FindIdentifier(pIdentifier);
      if (m_pElementTree.OnlyElementMode) {
        if (!pSelectionItem.Checked) {
          m_pElementTree.UnCheckAll();
        }
      }
      pSelectionItem.Checked = bCheckState;
    }

    #endregion

    #region CSelectionTreeFormDoc public/internal properties
    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CAppConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }
    /// <summary>
    /// Получает дерево элементов
    /// </summary>
    internal CSelectionFormTree ElementTree
    {
      get
      {
        return m_pElementTree;
      }
    }

    #endregion

    #region CDoc methods
    /// <summary>
    /// Открывает документ
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
    }

    /// <summary>
    /// Закрывает документ
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {
    }

    #endregion
  }

  #endregion
}
