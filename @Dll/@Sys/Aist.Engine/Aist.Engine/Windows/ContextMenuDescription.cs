﻿#region CopyRight
//This code was originally developed 2013-04-15 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;
using System.Windows.Forms;
using Aist.Engine.Data.Db;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Windows
{
  /// <summary>
  /// Описание объекта типа ContextMenuStrip
  /// </summary>
  public abstract class CContextMenuDescription : List<ToolStripMenuItem>
  {
    /// <summary>
    /// Документ базы данных
    /// </summary>
    protected CDbCommonDoc m_pDbDoc;
    /// <summary>
    /// Конфигуратор
    /// </summary>
    protected CAppConfig m_pAppConfig;
    /// <summary>
    /// Идентификатор элемента родительского контрола
    /// </summary>
    protected object m_pIdentifier;

    /// <summary>
    /// Конструктор
    /// </summary>
    protected CContextMenuDescription(CDbCommonDoc pDbDoc)
    {
      m_pDbDoc = pDbDoc;
      m_pAppConfig = pDbDoc.AppConfig;
      Param = null;
    }

    /// <summary>
    /// Идентификатор элемента родительского контрола
    /// </summary>
    abstract public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Родительская форма
    /// </summary>
    public Form ParentForm
    {
      get;
      set;
    }
    /// <summary>
    /// Родительский контрол
    /// </summary>
    public Control ParentControl
    {
      get;
      set;
    }
    /// <summary>
    /// Объект для работы с дочерними формами
    /// </summary>
    public CChildFormProcessor ChildFormProcessor
    {
      get;
      set;
    }
    /// <summary>
    /// Список произвольных параметров
    /// </summary>
    public object[] Param
    {
      get;
      set;
    }

    #region Public service properties
    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbCommonDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }

    #endregion

  }
}
