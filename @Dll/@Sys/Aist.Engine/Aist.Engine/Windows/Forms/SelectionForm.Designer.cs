﻿namespace Aist.Engine.Windows.Forms
{
  partial class CSelectionForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.panMain = new System.Windows.Forms.Panel();
      this.btnFilterClear = new System.Windows.Forms.Button();
      this.btnFilter = new System.Windows.Forms.Button();
      this.labCount = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.txtFilter = new System.Windows.Forms.TextBox();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.gridSelect = new System.Windows.Forms.DataGridView();
      this.colCheckState = new System.Windows.Forms.DataGridViewImageColumn();
      this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panInfo = new System.Windows.Forms.Panel();
      this.btnDictionary = new System.Windows.Forms.Button();
      this.labParent = new System.Windows.Forms.Label();
      this.labTitle = new System.Windows.Forms.Label();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panMain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridSelect)).BeginInit();
      this.panInfo.SuspendLayout();
      this.SuspendLayout();
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnFilterClear);
      this.panMain.Controls.Add(this.btnFilter);
      this.panMain.Controls.Add(this.labCount);
      this.panMain.Controls.Add(this.label12);
      this.panMain.Controls.Add(this.label1);
      this.panMain.Controls.Add(this.txtFilter);
      this.panMain.Controls.Add(this.btnOk);
      this.panMain.Controls.Add(this.btnCancel);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panMain.Location = new System.Drawing.Point(0, 213);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(592, 60);
      this.panMain.TabIndex = 1;
      // 
      // btnFilterClear
      // 
      this.btnFilterClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilterClear.Image = global::Aist.Engine.Properties.Resources.drop_p04;
      this.btnFilterClear.Location = new System.Drawing.Point(213, 25);
      this.btnFilterClear.Name = "btnFilterClear";
      this.btnFilterClear.Size = new System.Drawing.Size(25, 25);
      this.btnFilterClear.TabIndex = 2;
      this.btnFilterClear.UseVisualStyleBackColor = true;
      this.btnFilterClear.Click += new System.EventHandler(this.btnFilterClear_Click);
      // 
      // btnFilter
      // 
      this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilter.Image = global::Aist.Engine.Properties.Resources.search_p04;
      this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnFilter.Location = new System.Drawing.Point(244, 25);
      this.btnFilter.Name = "btnFilter";
      this.btnFilter.Size = new System.Drawing.Size(25, 25);
      this.btnFilter.TabIndex = 3;
      this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnFilter.UseVisualStyleBackColor = true;
      this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
      // 
      // labCount
      // 
      this.labCount.AutoSize = true;
      this.labCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labCount.ForeColor = System.Drawing.Color.Navy;
      this.labCount.Location = new System.Drawing.Point(426, 32);
      this.labCount.Name = "labCount";
      this.labCount.Size = new System.Drawing.Size(14, 13);
      this.labCount.TabIndex = 116;
      this.labCount.Text = "0";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label12.Location = new System.Drawing.Point(289, 32);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(131, 13);
      this.label12.TabIndex = 115;
      this.label12.Text = "Выбрано элементов:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.Location = new System.Drawing.Point(9, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Фильтр:";
      // 
      // txtFilter
      // 
      this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtFilter.Location = new System.Drawing.Point(12, 28);
      this.txtFilter.Name = "txtFilter";
      this.txtFilter.Size = new System.Drawing.Size(195, 22);
      this.txtFilter.TabIndex = 1;
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnOk.Image = global::Aist.Engine.Properties.Resources.button_ok_p03;
      this.btnOk.Location = new System.Drawing.Point(496, 10);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(40, 40);
      this.btnOk.TabIndex = 4;
      this.btnOk.UseVisualStyleBackColor = true;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Image = global::Aist.Engine.Properties.Resources.button_cancel_p03;
      this.btnCancel.Location = new System.Drawing.Point(542, 10);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(40, 40);
      this.btnCancel.TabIndex = 5;
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // gridSelect
      // 
      this.gridSelect.AllowUserToAddRows = false;
      this.gridSelect.AllowUserToDeleteRows = false;
      this.gridSelect.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.gridSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.gridSelect.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheckState,
            this.colName,
            this.colInfo});
      this.gridSelect.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gridSelect.Location = new System.Drawing.Point(0, 60);
      this.gridSelect.MultiSelect = false;
      this.gridSelect.Name = "gridSelect";
      this.gridSelect.ReadOnly = true;
      this.gridSelect.RowHeadersWidth = 25;
      this.gridSelect.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.gridSelect.RowTemplate.ReadOnly = true;
      this.gridSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.gridSelect.Size = new System.Drawing.Size(592, 153);
      this.gridSelect.TabIndex = 0;
      this.gridSelect.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSelect_CellClick);
      this.gridSelect.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.gridSelect_CellFormatting);
      // 
      // colCheckState
      // 
      this.colCheckState.DataPropertyName = "Checked";
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle1.NullValue = null;
      this.colCheckState.DefaultCellStyle = dataGridViewCellStyle1;
      this.colCheckState.HeaderText = "";
      this.colCheckState.MinimumWidth = 25;
      this.colCheckState.Name = "colCheckState";
      this.colCheckState.ReadOnly = true;
      this.colCheckState.Width = 25;
      // 
      // colName
      // 
      this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.colName.DataPropertyName = "Name";
      this.colName.HeaderText = "Наименование";
      this.colName.Name = "colName";
      this.colName.ReadOnly = true;
      // 
      // colInfo
      // 
      this.colInfo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.colInfo.DataPropertyName = "Info";
      this.colInfo.HeaderText = "Информация";
      this.colInfo.Name = "colInfo";
      this.colInfo.ReadOnly = true;
      // 
      // panInfo
      // 
      this.panInfo.Controls.Add(this.btnDictionary);
      this.panInfo.Controls.Add(this.labParent);
      this.panInfo.Controls.Add(this.labTitle);
      this.panInfo.Dock = System.Windows.Forms.DockStyle.Top;
      this.panInfo.Location = new System.Drawing.Point(0, 0);
      this.panInfo.Name = "panInfo";
      this.panInfo.Size = new System.Drawing.Size(592, 60);
      this.panInfo.TabIndex = 2;
      // 
      // btnDictionary
      // 
      this.btnDictionary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnDictionary.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnDictionary.Image = global::Aist.Engine.Properties.Resources.Book_p03;
      this.btnDictionary.Location = new System.Drawing.Point(542, 9);
      this.btnDictionary.Name = "btnDictionary";
      this.btnDictionary.Size = new System.Drawing.Size(40, 40);
      this.btnDictionary.TabIndex = 6;
      this.btnDictionary.UseVisualStyleBackColor = true;
      this.btnDictionary.Click += new System.EventHandler(this.btnDictionary_Click);
      // 
      // labParent
      // 
      this.labParent.AutoSize = true;
      this.labParent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labParent.ForeColor = System.Drawing.Color.Navy;
      this.labParent.Location = new System.Drawing.Point(9, 31);
      this.labParent.Name = "labParent";
      this.labParent.Size = new System.Drawing.Size(174, 13);
      this.labParent.TabIndex = 117;
      this.labParent.Text = "РОДИТЕЛЬСКИЙ ЭЛЕМЕНТ";
      // 
      // labTitle
      // 
      this.labTitle.AutoSize = true;
      this.labTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labTitle.Location = new System.Drawing.Point(9, 9);
      this.labTitle.Name = "labTitle";
      this.labTitle.Size = new System.Drawing.Size(452, 13);
      this.labTitle.TabIndex = 116;
      this.labTitle.Text = "Выберите элементы из представленного списка для включения в состав:";
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
      this.dataGridViewTextBoxColumn1.HeaderText = "Наименование";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.dataGridViewTextBoxColumn2.DataPropertyName = "Info";
      this.dataGridViewTextBoxColumn2.HeaderText = "Информация";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      // 
      // CSelectionForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(592, 273);
      this.Controls.Add(this.gridSelect);
      this.Controls.Add(this.panInfo);
      this.Controls.Add(this.panMain);
      this.MinimumSize = new System.Drawing.Size(500, 200);
      this.Name = "CSelectionForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "SelectionForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CSelectionForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CSelectionForm_FormClosed);
      this.Load += new System.EventHandler(this.CSelectionForm_Load);
      this.panMain.ResumeLayout(false);
      this.panMain.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridSelect)).EndInit();
      this.panInfo.ResumeLayout(false);
      this.panInfo.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panMain;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.DataGridView gridSelect;
    private System.Windows.Forms.Panel panInfo;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtFilter;
    private System.Windows.Forms.Label labCount;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label labParent;
    private System.Windows.Forms.Label labTitle;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.Button btnFilter;
    private System.Windows.Forms.DataGridViewImageColumn colCheckState;
    private System.Windows.Forms.DataGridViewTextBoxColumn colName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colInfo;
    private System.Windows.Forms.Button btnDictionary;
    private System.Windows.Forms.Button btnFilterClear;
  }
}