﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Aist.Engine.Classes;
using Aist.Engine.Data.Structures;
using Aist.Engine.Windows.Controls;
using Aist.Engine.Windows.Docs;
using Sfo.Sys.Config;
using Sfo.Sys.Windows;

#endregion

namespace Aist.Engine.Windows.Forms
{
  /// <summary>
  /// Форма для работы со списком элементов
  /// </summary>
  public partial class CListForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    protected CListFormDoc m_pListFormDoc;
    /// <summary>
    /// Источник для отображения списка
    /// </summary>
    protected BindingSource m_bsGridList;
    /// <summary>
    /// Объект страничного меню
    /// </summary>
    private CPageMenu m_pPageMenu;
    /// <summary>
    /// Максимальное количество записей на странице
    /// </summary>
    private int m_iMaxPageSize;
    /// <summary>
    /// Объект для работы с дочерними формами
    /// </summary>
    private CChildFormProcessor m_pChildFormProcessor;
    /// <summary>
    /// Цвет по умолчанию для ячеек грида
    /// </summary>
    private readonly Color m_pGridBackColor;
    /// <summary>
    /// Цвет по умолчанию для шрифта ячеек грида
    /// </summary>
    private readonly Color m_pGridForeColor;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    public CListForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pGridDescription">Описание элемента управления типа DataGridView</param>
    /// <param name="pChildFormProcessor">Объект для работы с дочерними формами</param>
    public CListForm(CAppConfig pAppConfig, object pGridDescription, CChildFormProcessor pChildFormProcessor)
    {
      InitializeComponent();
      try {
        m_pListFormDoc = new CListFormDoc(pAppConfig, pGridDescription);
        m_pListFormDoc.Open();
        m_iMaxPageSize = m_pListFormDoc.GridDescription.MaxPageSize;
        m_pChildFormProcessor = pChildFormProcessor;

        m_pPageMenu = new CPageMenu();

        m_pGridBackColor = gridList.DefaultCellStyle.BackColor;
        m_pGridForeColor = gridList.DefaultCellStyle.ForeColor;
        gridList.AutoGenerateColumns = false;
        gridList.Columns.Clear();
        DataGridViewColumn pNumColumn = new DataGridViewColumn();
        DataGridViewCell pNumCell = new DataGridViewTextBoxCell();
        pNumColumn.CellTemplate = pNumCell;
        pNumColumn.DataPropertyName = "RowRank";
        pNumColumn.HeaderText = "№, п/п";
        pNumColumn.Name = "RowRank";
        pNumColumn.Visible = true;
        pNumColumn.Frozen = true;
        pNumColumn.ReadOnly = true;
        pNumColumn.Width = 30;
        pNumColumn.MinimumWidth = 30;
        pNumColumn.DefaultCellStyle.BackColor = SystemColors.Control;
        pNumColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        pNumColumn.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
        pNumColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        pNumColumn.HeaderCell.Style.WrapMode = DataGridViewTriState.True;
        pNumColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        gridList.Columns.Add(pNumColumn);
        foreach (CGridColumn pGridColumn in m_pListFormDoc.GridDescription.GridColumn) {
          DataGridViewColumn pColumn;
          DataGridViewCell pCell;
          if (pGridColumn.EditFormClass == null) {
            switch ((EGridColumnType) pGridColumn.ColumnType) {
              case EGridColumnType.ColumnTypeBool:
                pColumn = new DataGridViewCheckBoxColumn(); //DataGridViewImageColumn();
                pCell = new DataGridViewCheckBoxCell(); //DataGridViewImageCell();
                break;
              case EGridColumnType.ColumnTypeDate:
                pColumn = new Aist.Engine.Windows.Controls.CDataGridViewDateTimeColumn();
                pCell = new Aist.Engine.Windows.Controls.CDataGridViewDateTimeCell();
                break;
              /* EVV: Такой вариант пока исключаем.
              case EGridColumnType.ColumnTypeCheck:
                pColumn = new Aist.Engine.Windows.Controls.CDataGridViewCheckedComboBoxColumn();
                pCell = new Aist.Engine.Windows.Controls.CDataGridViewCheckedComboBoxCell();
                break;
              */
              default:
                pColumn = new DataGridViewColumn();
                pCell = new DataGridViewTextBoxCell();
                break;
            }
          }
          else {
            pColumn = new DataGridViewSelectionColumn();
            switch ((EGridColumnType) pGridColumn.ColumnType) {
              case EGridColumnType.ColumnTypeCheck:
                // EVV: Возможно, пригодится. Пока непонятно.
              default:
                (pColumn as DataGridViewSelectionColumn).PreSelectionFormMethod = m_pListFormDoc.GridDescription.GetSelectionFormList;
                (pColumn as DataGridViewSelectionColumn).SelectionFormMethod = h_selectionFormCall;
                (pColumn as DataGridViewSelectionColumn).PostSelectionFormMethod = m_pListFormDoc.GridDescription.GetSelectionFormItem;
                (pColumn as DataGridViewSelectionColumn).LeaveTextBoxMethod = m_pListFormDoc.GridDescription.DbSearch;
                (pColumn as DataGridViewSelectionColumn).SelectionFormDescription = pGridColumn.EditFormClass;
                pCell = new DataGridViewSelectionCell();
                break;
            }
          }
          pColumn.CellTemplate = pCell;
          pColumn.DataPropertyName = (string)pGridColumn.DataPropertyName;
          pColumn.HeaderText = (string)pGridColumn.HeaderText;
          pColumn.Name = (string)pGridColumn.ColumnName;
          pColumn.Visible = true;
          pColumn.MinimumWidth = (int)pGridColumn.MinimumWidth;
          pColumn.DefaultCellStyle.ForeColor = (Color)pGridColumn.CellForeColor;
          pColumn.HeaderCell.Style.Alignment = (DataGridViewContentAlignment)pGridColumn.HeaderCellAlignment;
          pColumn.DefaultCellStyle.WrapMode = (DataGridViewTriState)pGridColumn.CellWrapMode;
          pColumn.Visible = (bool)pGridColumn.Visible;
          pColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          gridList.Columns.Add(pColumn);
        }

        m_bsGridList = new BindingSource();
        m_bsGridList.DataSource = m_pListFormDoc.GridDescription;
        gridList.DataSource = m_bsGridList;
        gridList.DataError += new DataGridViewDataErrorEventHandler(gridList_DataError);        
        m_bsGridList.AddingNew += new AddingNewEventHandler(m_bsGridList_AddingNew);
        m_bsGridList.ListChanged += new ListChangedEventHandler(m_bsGridList_ListChanged);
        m_bsGridList.DataError += new BindingManagerDataErrorEventHandler(m_bsGridList_DataError);

        h_fillGrid(txtFilter.Text.Trim(), false);
        labTitle.Text = m_pListFormDoc.GridDescription.Title;
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods
    /// <summary>
    /// Вызов формы на добавление или редактирование записи
    /// </summary>
    private void h_editForm()
    {
      if (m_pChildFormProcessor == null) return;
      Form pForm = m_pChildFormProcessor.CreateFormByClass(m_pListFormDoc.GridDescription.EditFormClass,
                                                           new Type[] {typeof (CAppConfig), typeof (CGridItem)},
                                                           new object[] {m_pListFormDoc.AppConfig, (CGridItem) m_bsGridList.Current}
        );
      //Type tForm = Type.GetType(m_pListFormDoc.GridDescription.EditFormClass, false, true);
      //if (tForm != null) {
        //ConstructorInfo pConstructorInfo = tForm.GetConstructor(new Type[] { typeof(CAppConfig), typeof(CGridItem) });
        //Form pForm = (Form)pConstructorInfo.Invoke(new object[] { m_pListFormDoc.AppConfig, (CGridItem)m_bsGridList.Current });
        if (pForm != null && !pForm.IsDisposed) {
          DialogResult pDialogResult = pForm.ShowDialog();
          if (pDialogResult == DialogResult.OK) {
            // Изменения в форме редактирования приняты
            if (!m_pListFormDoc.GridDescription.DbEdit((CGridItem)m_bsGridList.Current)) {
              MessageBox.Show("Внимание: имеются ошибки сохранения записи в БД. Смотрите описание.");
            }
          }
          else {
            // Если это была вновь вставленная строка и изменения не приняты, то удаляем ее:
            if (((CGridItem)m_bsGridList.Current).Identifier == null) {              
              m_bsGridList.RemoveCurrent();
            }
          }
        }
      //}
    }

    /// <summary>
    /// Диалог сохранения изменений в БД
    /// </summary>
    protected bool h_saveDialog()
    {
      if (!m_pListFormDoc.GridDescription.IsSaved()) {
        switch (CMessageBox.ShowYesNoCancel("Сохранить изменения текущей страницы?")) {
          case DialogResult.Yes:
            int ii = m_pListFormDoc.GridDescription.DbSave();
            m_bsGridList.ResetBindings(true);
            if (ii > 0) {              
              MessageBox.Show(string.Format("Внимание! Не удалось сохранить записей в базе: {0}. Смотрите описание.", ii));
              return false;
            }
            break;
          case DialogResult.No:
            break;
          case DialogResult.Cancel:
            return false;
        }
      }
      return true;
    }

    /// <summary>
    /// Заполнение грида значениями
    /// </summary>
    /// <param name="bShowDialog">true - использовать диалог сохранения, false - не использовать диалог сохранения</param>
    protected void h_fillGrid(bool bShowDialog)
    {
      h_fillGrid(m_pListFormDoc.GridDescription.Filter, bShowDialog);
    }

    /// <summary>
    /// Заполнение грида значениями
    /// </summary>
    /// <param name="sFilter">Значение фильтра выборки</param>
    /// <param name="bShowDialog">true - использовать диалог сохранения, false - не использовать диалог сохранения</param>
    private void h_fillGrid(string sFilter, bool bShowDialog)
    {
      if (bShowDialog && !h_saveDialog()) return;

      // Сохраняет текущее значение идентификатора записи:
      object pIdentifier = null;
      if (m_bsGridList.Current != null) {
        pIdentifier = ((CGridItem) m_bsGridList.Current).Identifier;
      }

      // Обновляем сетку и сопутствующие элементы формы:
      m_pPageMenu.PageNumber = m_pListFormDoc.GridDescription.DbSelect(sFilter, m_iMaxPageSize, ref m_pPageMenu.CurrentPage);
      m_pPageMenu.Update();
      m_bsGridList.ResetBindings(true);
      h_repaintPageMenu();
      Text = m_pListFormDoc.GridDescription.FormName;

      // Пытается найти значение на текущей странице сетки:
      object pGridItem = m_pListFormDoc.GridDescription.FindItemByIdentifier(pIdentifier);
      if (pGridItem != null) {
        // Значение найдено. Помечаем:
        m_bsGridList.Position = m_bsGridList.IndexOf(pGridItem);
      }
    }

    /// <summary>
    /// Фильтрация списка
    /// </summary>
    private void h_filter()
    {
      h_fillGrid(txtFilter.Text.Trim(), true);
      if (m_pListFormDoc.GridDescription.FoundIdentifierList.Count > 0) {
        m_pListFormDoc.FoundIdentifierListCurrentIndex = -1;
        h_search();
      }
    }

    /// <summary>
    /// Поиск по БД
    /// </summary>
    private void h_search()
    {
      // Если изменилось значение для поиска:
      if (m_pListFormDoc.FoundIdentifierListCurrentIndex < 0) {
        m_pListFormDoc.GridDescription.DbSearch(txtSearch.Text.Trim());
        if (m_pListFormDoc.GridDescription.FoundIdentifierList.Count > 0) {
          m_pListFormDoc.FoundIdentifierListCurrentIndex = 0;
        }
      }
      //TODO: Здесь добавить поиск по записям, еще не сохраненным в БД...

      // Если нет записей, удовлетворяющих условию, то возвращаемся:
      if (m_pListFormDoc.FoundIdentifierListCurrentIndex < 0) {
        return;
      }
      else {
        // Пытается найти значение на текущей странице:
        object pGridItem = m_pListFormDoc.GridDescription.FindItemByIdentifier(
          ((CIdentifierRankItem)m_pListFormDoc.GridDescription.FoundIdentifierList[m_pListFormDoc.FoundIdentifierListCurrentIndex]).Identifier);
        if (pGridItem != null) {
          // Значение найдено. Помечаем:
          m_bsGridList.Position = m_bsGridList.IndexOf(pGridItem);
        }
        else {
          // Значение не найдено.
          if (!h_saveDialog()) return;
          CIdentifierRankItem pIdentifierRankItem = (CIdentifierRankItem)m_pListFormDoc.GridDescription.FoundIdentifierList[m_pListFormDoc.FoundIdentifierListCurrentIndex];
          m_pListFormDoc.GridDescription.DbSearch(txtSearch.Text.Trim());
          int ii = m_pListFormDoc.GridDescription.FoundIdentifierList.FindIdentifierRankItem(pIdentifierRankItem.Identifier);
          if  (ii < 0) {
            m_pListFormDoc.FoundIdentifierListCurrentIndex = -1;
            return;
          }
          else {
            // Вычисляем номер страницы:
            int pp = m_pListFormDoc.GridDescription.GetElementPage(((CIdentifierRankItem)m_pListFormDoc.GridDescription.FoundIdentifierList[ii]).Rank);
            m_pPageMenu.CurrentPage = pp;
            h_fillGrid(m_pListFormDoc.GridDescription.Filter, false);
            pGridItem = m_pListFormDoc.GridDescription.FindItemByIdentifier(
              ((CIdentifierRankItem)m_pListFormDoc.GridDescription.FoundIdentifierList[ii]).Identifier);
            if (pGridItem != null) {
              // Значение найдено. Помечаем:
              m_bsGridList.Position = m_bsGridList.IndexOf(pGridItem);
            }
          }

        }
      }
    }

    /// <summary>
    /// Перерисовка меню выбора страницы
    /// </summary>
    private void h_repaintPageMenu()
    {
      CWindowToolkit.RepaintPageMenuItem(lnkBegin, m_pPageMenu.LnkBegin);
      CWindowToolkit.RepaintPageMenuItem(lnkBack, m_pPageMenu.LnkBack);
      CWindowToolkit.RepaintPageMenuItem(lnkOne, m_pPageMenu.NumericList[0]);
      CWindowToolkit.RepaintPageMenuItem(lnkTwo, m_pPageMenu.NumericList[1]);
      CWindowToolkit.RepaintPageMenuItem(lnkThree, m_pPageMenu.NumericList[2]);
      CWindowToolkit.RepaintPageMenuItem(lnkOther, m_pPageMenu.LnkOther);
      CWindowToolkit.RepaintPageMenuItem(lnkForward, m_pPageMenu.LnkForward);
      CWindowToolkit.RepaintPageMenuItem(lnkEnd, m_pPageMenu.LnkEnd);
    }

    /// <summary>
    /// Вызов формы выбора значения из списка
    /// </summary>
    /// <param name="pSelectionFormDescription">Описание списка или дерева элементов для формы выбора</param>
    /// <param name="pSelectionProperty">Содержимое ячейки редактирования</param>
    /// <returns>Если выбрано значение, то возвращает "true"</returns>
    private bool h_selectionFormCall(object pSelectionFormDescription, CSelectionProperty pSelectionProperty)
    {
      bool result = false; 
      CSelectionItem pSelectionItem = null;
      if (pSelectionFormDescription.GetType() == typeof(CSelectionFormList)) {
        (pSelectionFormDescription as CSelectionFormList).Clear();
        CSelectionForm pForm = new CSelectionForm(m_pListFormDoc.AppConfig, pSelectionFormDescription, m_pChildFormProcessor);
        if (!pForm.IsDisposed) {
          (pSelectionFormDescription as CSelectionFormList).CheckIdentifier(pSelectionProperty.Identifier);
          if (pForm.ShowDialog() == DialogResult.OK) {
            pSelectionItem = (pSelectionFormDescription as CSelectionFormList).FindChecked();
          }
        }
      }
      else {
        if (pSelectionFormDescription.GetType() == typeof(CSelectionFormTree)) {
          (pSelectionFormDescription as CSelectionFormTree).SelectionTreeList.Clear();
          CSelectionTreeForm pForm = new CSelectionTreeForm(m_pListFormDoc.AppConfig, pSelectionFormDescription, m_pChildFormProcessor);
          if (!pForm.IsDisposed) {
            (pSelectionFormDescription as CSelectionFormTree).CheckIdentifier(pSelectionProperty.Identifier);
            if (pForm.ShowDialog() == DialogResult.OK) {
              pSelectionItem = (pSelectionFormDescription as CSelectionFormTree).FindChecked();
            }
          }
        }
        else {
          if (pSelectionFormDescription.GetType() == typeof(CSelectionFormCheckBox)) {
            (pSelectionFormDescription as CSelectionFormCheckBox).Clear();
            CSelectionCheckBoxForm pForm = new CSelectionCheckBoxForm(m_pListFormDoc.AppConfig, pSelectionFormDescription, m_pChildFormProcessor);
            if (!pForm.IsDisposed) {
              (pSelectionFormDescription as CSelectionFormCheckBox).CheckIdentifier(pSelectionProperty.Identifier);
              if (pForm.ShowDialog() == DialogResult.OK) {
                pSelectionItem = (pSelectionFormDescription as CSelectionFormCheckBox).FindChecked();
              }
            }
          }
          else {
            if (pSelectionFormDescription.GetType() == typeof (OpenFileDialog)) {
              OpenFileDialog pOpenFileDialog = pSelectionFormDescription as OpenFileDialog;
              string sCurrentDirectory = pSelectionProperty.Value != null && pSelectionProperty.Value.ToString() != "" ? Path.GetDirectoryName(pSelectionProperty.Value.ToString()) : "";
              if (Directory.Exists(sCurrentDirectory)) {
                pOpenFileDialog.InitialDirectory = sCurrentDirectory;
              }
              else {
                pOpenFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
              }
              if (pOpenFileDialog.ShowDialog() == DialogResult.OK) {
                if (!string.IsNullOrEmpty(pOpenFileDialog.FileName.Trim())) {
                  pSelectionItem = new CSelectionItem();
                  pSelectionItem.Name = pOpenFileDialog.FileName.Trim();
                }
              }
            }
          }
        }
      }
      if (pSelectionItem != null) {
        pSelectionProperty.Identifier = pSelectionItem.Identifier;
        if (pSelectionItem.GetType() == typeof(CSelectionItem))
          pSelectionProperty.Value = pSelectionItem.Name + (!String.IsNullOrEmpty(pSelectionItem.Info) ? " [" + pSelectionItem.Info + "]" : "");
        else if (pSelectionItem.GetType() == typeof(CCBoxItem))
          pSelectionProperty.Value = (pSelectionItem as CCBoxItem).Text;

        result = true;
      }
      return result;
    }

    /// <summary>
    /// Сохранение изменений в БД
    /// </summary>
    /// <returns>Результат сохранения</returns>
    protected bool h_save()
    {
      int ii = m_pListFormDoc.GridDescription.DbSave();
      m_bsGridList.ResetBindings(true);
      if (ii > 0) {
        MessageBox.Show(string.Format("Внимание! Не удалось сохранить записей в базе: {0}. Смотрите описание.", ii));
        return false;
      }
      return true;
    }

    #endregion

    #region CListForm public/internal properties
    /// <summary>
    /// Идентификатор формы
    /// </summary>
    public object FormIdentifier { get; set; }

    #endregion

    #region Control handlers
    private void btnExit_Click(object sender, System.EventArgs e)
    {
      Close();
    }

    private void CListForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (m_pListFormDoc.GridDescription.Changed) {
        DialogResult = DialogResult.OK;
      }
      m_pListFormDoc.Close();
    }

    private void CListForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (m_pListFormDoc.GridDescription.IsSaved()) return;

      switch (CMessageBox.ShowYesNoCancel(string.Format("{0}.\r\nСохранить изменения?", m_pListFormDoc.GridDescription.Title))) {
        case DialogResult.Yes:
          int ii = m_pListFormDoc.GridDescription.DbSave();
          e.Cancel = ii > 0;
          if (e.Cancel) {
            m_bsGridList.ResetBindings(true);
            MessageBox.Show(string.Format("Внимание! Не удалось сохранить записей в базе: {0}. Смотрите описание.", ii));
          }
          break;
        case DialogResult.No:
          break;
        case DialogResult.Cancel:
          e.Cancel = true;
          break;
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      // Вызов формы добавления записи
      //m_bsGridList.AddNew();
      int ii = m_pListFormDoc.GridDescription.AddItem();
      m_bsGridList.ResetBindings(true);
      m_bsGridList.Position = m_bsGridList.IndexOf(m_pListFormDoc.GridDescription[ii]);
      h_editForm();
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      // Вызов формы удаления записи
      if (m_bsGridList.Current == null) {
        return;
      }
      CGridItem pGridItem = (CGridItem) m_bsGridList.Current;
      if (pGridItem.Identifier == null) {
        m_bsGridList.RemoveCurrent();
      }
      else {
        switch (
          CMessageBox.ShowYesNo(string.Format("Удалить элемент {0} из базы данных?", string.Format("№{0} ({1})", pGridItem.RowRank, m_pListFormDoc.GridDescription.GetElementText(pGridItem))))) {
            case DialogResult.No:
              return;
        }
        if (m_pListFormDoc.GridDescription.DbDelete(pGridItem)) {
          m_bsGridList.RemoveCurrent();
        }
      }
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      // Вызов формы редактирования записи
      if (m_bsGridList.Current == null) {
        return;
      }
      h_editForm();
    }

    private void gridList_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
    {
      if (((CGridItem)e.Row.DataBoundItem).Identifier == null) {
        return;
      }
      if (m_pListFormDoc.GridDescription.DbDelete((CGridItem)e.Row.DataBoundItem)) {
        e.Cancel = false;
      }
      else {
        e.Cancel = true;
      }
    }

    private void gridList_DataError(object sender, DataGridViewDataErrorEventArgs anError)
    {
      /*
      if (anError.Exception == null) return;
      if (anError.Context == DataGridViewDataErrorContexts.Display &&
        anError.Exception.InnerException != null &&
        anError.Exception.InnerException.Message.ToUpper() == "OBJECT DOES NOT MATCH TARGET TYPE.") {
        //MessageBox.Show(anError.Exception.InnerException.Message);
        anError.Cancel = true;
        m_bsGridList.CancelEdit();
      }
      else {
        // Повторное выбрасывание исключения, которое не связано с ошибкой вставки объекта
        anError.ThrowException = true;
      }
       */
    }

    private void m_bsGridList_AddingNew(object sender, AddingNewEventArgs e)
    {
      int ii = m_pListFormDoc.GridDescription.AddItem();
      e.NewObject = m_pListFormDoc.GridDescription[ii];
      m_bsGridList.ResetBindings(true);
      m_bsGridList.Position = m_bsGridList.IndexOf(m_pListFormDoc.GridDescription[ii]);
      h_editForm();
      //((CGridItem) e.NewObject).Saved = true;
      //e.NewObject = "Хрень";
    }

    private void m_bsGridList_ListChanged(object sender, ListChangedEventArgs e)
    {
      /*
      if (e.ListChangedType == ListChangedType.ItemAdded &&
        m_bsGridList[e.NewIndex] != null &&
        ((CGridItem)m_bsGridList[e.NewIndex]).ModifyFlag) {
        //m_pListFormDoc.GridDescription.Remove((CGridItem)m_bsGridList[e.NewIndex]);
          m_bsGridList.Position = e.NewIndex;
        m_bsGridList.RemoveCurrent();
        //m_bsGridList.RemoveAt(e.NewIndex-1);
        m_bsGridList.ResetBindings(true);
        //txtSearch.Text = m_pListFormDoc.GridDescription.Count.ToString() + " " +
        //  m_bsGridList.Count.ToString();
      }
       */
    }

    private void m_bsGridList_DataError(Object sender, BindingManagerDataErrorEventArgs e)
    {

    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      h_save();
    }

    private void gridList_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {  
      /*
      for (int ii = 0; ii < e.RowCount; ii++) {
        if (gridList.Rows[e.RowIndex + ii].DataBoundItem != null &&
            ((CGridItem)gridList.Rows[e.RowIndex + ii].DataBoundItem).Identifier == null &&
            ((CGridItem)gridList.Rows[e.RowIndex + ii].DataBoundItem).ModifyFlag
          ) {
          gridList.Rows.RemoveAt(e.RowIndex + ii);          
        }
      }
       */
    }

    private void gridList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      // Вызов формы редактирования записи
      if (m_bsGridList.Current == null) {
        return;
      }
      h_editForm();
    }

    private void gridList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex > -1) {
        ((CGridItem) gridList.Rows[e.RowIndex].DataBoundItem).Saved = false;
        ((CGridItem) gridList.Rows[e.RowIndex].DataBoundItem).ErrorDescription = "Исправлена";
        m_bsGridList.ResetBindings(false);
      }
    }

    private void btnFilter_Click(object sender, EventArgs e)
    {
      m_pPageMenu.CurrentPage = 1;
      h_filter();
    }

    private void panPage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      m_pPageMenu.ItemClick(((LinkLabel)sender).Name);
      h_fillGrid(m_pListFormDoc.GridDescription.Filter, true);
    }

    private void gridList_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      DataGridViewRow pDgvr = (sender as DataGridView).Rows[e.RowIndex];
      if (pDgvr.DataBoundItem != null) {
        if ((pDgvr.DataBoundItem as CGridItem).Saved == false) {
          pDgvr.DefaultCellStyle.BackColor = CWindowToolkit.EditControlBackColor;
        }
        else {
          pDgvr.DefaultCellStyle.BackColor = m_pGridBackColor;
        }
        if ((pDgvr.DataBoundItem as CGridItem).Active == false) {
          pDgvr.DefaultCellStyle.ForeColor = CWindowToolkit.InactiveControlForeColor;
        }
        else {
          pDgvr.DefaultCellStyle.ForeColor = m_pGridForeColor;
        }
        if ((pDgvr.DataBoundItem as CGridItem).Selected == true) {
          pDgvr.DefaultCellStyle.Font = new Font(DefaultFont, FontStyle.Bold);
        }
        else {
          pDgvr.DefaultCellStyle.Font = new Font(DefaultFont, FontStyle.Regular);
        }
      }
      else {
        pDgvr.DefaultCellStyle.BackColor = m_pGridBackColor;
        pDgvr.DefaultCellStyle.ForeColor = m_pGridForeColor;
        pDgvr.DefaultCellStyle.Font = new Font(DefaultFont, FontStyle.Regular);
      }
      pDgvr.Cells[0].Style.BackColor = SystemColors.Control;
    }

    private void btnFilterClear_Click(object sender, EventArgs e)
    {
      txtFilter.Text = string.Empty;
      m_pPageMenu.CurrentPage = 1;
      h_filter();
    }

    private void btnNext_Click(object sender, EventArgs e)
    {
      // Перемещение по массиву найденных значений вперед
      if (m_pListFormDoc.FoundIdentifierListCurrentIndex >= 0 &&
        m_pListFormDoc.FoundIdentifierListCurrentIndex < m_pListFormDoc.GridDescription.FoundIdentifierList.Count - 1) {
        m_pListFormDoc.FoundIdentifierListCurrentIndex++;
      }
      h_search();
    }

    private void btnPrevious_Click(object sender, EventArgs e)
    {
      // Перемещение по массиву найденных значений назад
      if (m_pListFormDoc.FoundIdentifierListCurrentIndex > 0) {
        m_pListFormDoc.FoundIdentifierListCurrentIndex--;
      }
      h_search();
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
      m_pListFormDoc.FoundIdentifierListCurrentIndex = -1;
    }

    private void gridList_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter) {
        (sender as DataGridView).CurrentCell.Selected = true;
        (sender as DataGridView).BeginEdit(true);
        e.Handled = true;
      }
    }

    #endregion

  }
}
