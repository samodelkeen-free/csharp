﻿namespace Aist.Engine.Windows.Forms
{
  partial class CListForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.panTool = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.btnNext = new System.Windows.Forms.Button();
      this.btnPrevious = new System.Windows.Forms.Button();
      this.btnFilterClear = new System.Windows.Forms.Button();
      this.btnFilter = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.txtFilter = new System.Windows.Forms.TextBox();
      this.btnPrint = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnEdit = new System.Windows.Forms.Button();
      this.btnDelete = new System.Windows.Forms.Button();
      this.btnAdd = new System.Windows.Forms.Button();
      this.panPage = new System.Windows.Forms.Panel();
      this.labTitle = new System.Windows.Forms.Label();
      this.lnkEnd = new System.Windows.Forms.LinkLabel();
      this.lnkBack = new System.Windows.Forms.LinkLabel();
      this.lnkBegin = new System.Windows.Forms.LinkLabel();
      this.lnkForward = new System.Windows.Forms.LinkLabel();
      this.lnkOne = new System.Windows.Forms.LinkLabel();
      this.lnkOther = new System.Windows.Forms.LinkLabel();
      this.lnkTwo = new System.Windows.Forms.LinkLabel();
      this.lnkThree = new System.Windows.Forms.LinkLabel();
      this.gridList = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panTool.SuspendLayout();
      this.panPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
      this.SuspendLayout();
      // 
      // panTool
      // 
      this.panTool.Controls.Add(this.label2);
      this.panTool.Controls.Add(this.txtSearch);
      this.panTool.Controls.Add(this.btnNext);
      this.panTool.Controls.Add(this.btnPrevious);
      this.panTool.Controls.Add(this.btnFilterClear);
      this.panTool.Controls.Add(this.btnFilter);
      this.panTool.Controls.Add(this.btnExit);
      this.panTool.Controls.Add(this.label1);
      this.panTool.Controls.Add(this.txtFilter);
      this.panTool.Controls.Add(this.btnPrint);
      this.panTool.Controls.Add(this.btnSave);
      this.panTool.Controls.Add(this.btnEdit);
      this.panTool.Controls.Add(this.btnDelete);
      this.panTool.Controls.Add(this.btnAdd);
      this.panTool.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panTool.Location = new System.Drawing.Point(0, 313);
      this.panTool.Name = "panTool";
      this.panTool.Size = new System.Drawing.Size(792, 60);
      this.panTool.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.Location = new System.Drawing.Point(500, 10);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(48, 13);
      this.label2.TabIndex = 102;
      this.label2.Text = "Поиск:";
      // 
      // txtSearch
      // 
      this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
      this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtSearch.Location = new System.Drawing.Point(503, 28);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(160, 22);
      this.txtSearch.TabIndex = 10;
      this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
      // 
      // btnNext
      // 
      this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnNext.Image = global::Aist.Engine.Properties.Resources.arrow_right;
      this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnNext.Location = new System.Drawing.Point(700, 25);
      this.btnNext.Name = "btnNext";
      this.btnNext.Size = new System.Drawing.Size(25, 25);
      this.btnNext.TabIndex = 12;
      this.btnNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnNext.UseVisualStyleBackColor = true;
      this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
      // 
      // btnPrevious
      // 
      this.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnPrevious.Image = global::Aist.Engine.Properties.Resources.arrow_left;
      this.btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnPrevious.Location = new System.Drawing.Point(669, 25);
      this.btnPrevious.Name = "btnPrevious";
      this.btnPrevious.Size = new System.Drawing.Size(25, 25);
      this.btnPrevious.TabIndex = 11;
      this.btnPrevious.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnPrevious.UseVisualStyleBackColor = true;
      this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
      // 
      // btnFilterClear
      // 
      this.btnFilterClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilterClear.Image = global::Aist.Engine.Properties.Resources.drop_p04;
      this.btnFilterClear.Location = new System.Drawing.Point(430, 25);
      this.btnFilterClear.Name = "btnFilterClear";
      this.btnFilterClear.Size = new System.Drawing.Size(25, 25);
      this.btnFilterClear.TabIndex = 8;
      this.btnFilterClear.UseVisualStyleBackColor = true;
      this.btnFilterClear.Click += new System.EventHandler(this.btnFilterClear_Click);
      // 
      // btnFilter
      // 
      this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilter.Image = global::Aist.Engine.Properties.Resources.search_p04;
      this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnFilter.Location = new System.Drawing.Point(461, 25);
      this.btnFilter.Name = "btnFilter";
      this.btnFilter.Size = new System.Drawing.Size(25, 25);
      this.btnFilter.TabIndex = 9;
      this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnFilter.UseVisualStyleBackColor = true;
      this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
      // 
      // btnExit
      // 
      this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnExit.Image = global::Aist.Engine.Properties.Resources.exit_p03;
      this.btnExit.Location = new System.Drawing.Point(742, 10);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(40, 40);
      this.btnExit.TabIndex = 30;
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.Location = new System.Drawing.Point(261, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 13);
      this.label1.TabIndex = 100;
      this.label1.Text = "Фильтр:";
      // 
      // txtFilter
      // 
      this.txtFilter.BackColor = System.Drawing.SystemColors.Window;
      this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtFilter.Location = new System.Drawing.Point(264, 28);
      this.txtFilter.Name = "txtFilter";
      this.txtFilter.Size = new System.Drawing.Size(160, 22);
      this.txtFilter.TabIndex = 7;
      // 
      // btnPrint
      // 
      this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnPrint.Image = global::Aist.Engine.Properties.Resources.fileprint_p03;
      this.btnPrint.Location = new System.Drawing.Point(206, 10);
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.Size = new System.Drawing.Size(40, 40);
      this.btnPrint.TabIndex = 6;
      this.btnPrint.UseVisualStyleBackColor = true;
      // 
      // btnSave
      // 
      this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnSave.Image = global::Aist.Engine.Properties.Resources.filesave_p03;
      this.btnSave.Location = new System.Drawing.Point(160, 10);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(40, 40);
      this.btnSave.TabIndex = 5;
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnEdit
      // 
      this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnEdit.Image = global::Aist.Engine.Properties.Resources.paper_edit_p03;
      this.btnEdit.Location = new System.Drawing.Point(102, 10);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new System.Drawing.Size(40, 40);
      this.btnEdit.TabIndex = 4;
      this.btnEdit.UseVisualStyleBackColor = true;
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // btnDelete
      // 
      this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnDelete.Image = global::Aist.Engine.Properties.Resources.paper_drop_p03;
      this.btnDelete.Location = new System.Drawing.Point(56, 10);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.Size = new System.Drawing.Size(40, 40);
      this.btnDelete.TabIndex = 3;
      this.btnDelete.UseVisualStyleBackColor = true;
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      // 
      // btnAdd
      // 
      this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnAdd.Image = global::Aist.Engine.Properties.Resources.paper_new_p03;
      this.btnAdd.Location = new System.Drawing.Point(10, 10);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(40, 40);
      this.btnAdd.TabIndex = 2;
      this.btnAdd.UseVisualStyleBackColor = true;
      this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
      // 
      // panPage
      // 
      this.panPage.Controls.Add(this.labTitle);
      this.panPage.Controls.Add(this.lnkEnd);
      this.panPage.Controls.Add(this.lnkBack);
      this.panPage.Controls.Add(this.lnkBegin);
      this.panPage.Controls.Add(this.lnkForward);
      this.panPage.Controls.Add(this.lnkOne);
      this.panPage.Controls.Add(this.lnkOther);
      this.panPage.Controls.Add(this.lnkTwo);
      this.panPage.Controls.Add(this.lnkThree);
      this.panPage.Dock = System.Windows.Forms.DockStyle.Top;
      this.panPage.Location = new System.Drawing.Point(0, 0);
      this.panPage.Name = "panPage";
      this.panPage.Size = new System.Drawing.Size(792, 38);
      this.panPage.TabIndex = 2;
      // 
      // labTitle
      // 
      this.labTitle.AutoEllipsis = true;
      this.labTitle.AutoSize = true;
      this.labTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labTitle.ForeColor = System.Drawing.Color.Black;
      this.labTitle.Location = new System.Drawing.Point(10, 10);
      this.labTitle.MaximumSize = new System.Drawing.Size(530, 0);
      this.labTitle.Name = "labTitle";
      this.labTitle.Size = new System.Drawing.Size(116, 13);
      this.labTitle.TabIndex = 118;
      this.labTitle.Text = "Заголовок списка";
      // 
      // lnkEnd
      // 
      this.lnkEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkEnd.AutoSize = true;
      this.lnkEnd.LinkVisited = true;
      this.lnkEnd.Location = new System.Drawing.Point(744, 10);
      this.lnkEnd.Name = "lnkEnd";
      this.lnkEnd.Size = new System.Drawing.Size(38, 13);
      this.lnkEnd.TabIndex = 27;
      this.lnkEnd.TabStop = true;
      this.lnkEnd.Text = "Конец";
      this.lnkEnd.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkBack
      // 
      this.lnkBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkBack.AutoSize = true;
      this.lnkBack.LinkVisited = true;
      this.lnkBack.Location = new System.Drawing.Point(615, 10);
      this.lnkBack.Name = "lnkBack";
      this.lnkBack.Size = new System.Drawing.Size(19, 13);
      this.lnkBack.TabIndex = 21;
      this.lnkBack.TabStop = true;
      this.lnkBack.Text = "<<";
      this.lnkBack.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkBegin
      // 
      this.lnkBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkBegin.AutoSize = true;
      this.lnkBegin.LinkVisited = true;
      this.lnkBegin.Location = new System.Drawing.Point(565, 10);
      this.lnkBegin.Name = "lnkBegin";
      this.lnkBegin.Size = new System.Drawing.Size(44, 13);
      this.lnkBegin.TabIndex = 20;
      this.lnkBegin.TabStop = true;
      this.lnkBegin.Text = "Начало";
      this.lnkBegin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkForward
      // 
      this.lnkForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkForward.AutoSize = true;
      this.lnkForward.LinkVisited = true;
      this.lnkForward.Location = new System.Drawing.Point(719, 10);
      this.lnkForward.Name = "lnkForward";
      this.lnkForward.Size = new System.Drawing.Size(19, 13);
      this.lnkForward.TabIndex = 26;
      this.lnkForward.TabStop = true;
      this.lnkForward.Text = ">>";
      this.lnkForward.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkOne
      // 
      this.lnkOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkOne.AutoSize = true;
      this.lnkOne.LinkVisited = true;
      this.lnkOne.Location = new System.Drawing.Point(640, 10);
      this.lnkOne.Name = "lnkOne";
      this.lnkOne.Size = new System.Drawing.Size(13, 13);
      this.lnkOne.TabIndex = 22;
      this.lnkOne.TabStop = true;
      this.lnkOne.Text = "1";
      this.lnkOne.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkOther
      // 
      this.lnkOther.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkOther.AutoSize = true;
      this.lnkOther.LinkVisited = true;
      this.lnkOther.Location = new System.Drawing.Point(697, 10);
      this.lnkOther.Name = "lnkOther";
      this.lnkOther.Size = new System.Drawing.Size(16, 13);
      this.lnkOther.TabIndex = 25;
      this.lnkOther.TabStop = true;
      this.lnkOther.Text = "...";
      this.lnkOther.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkTwo
      // 
      this.lnkTwo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkTwo.AutoSize = true;
      this.lnkTwo.LinkVisited = true;
      this.lnkTwo.Location = new System.Drawing.Point(659, 10);
      this.lnkTwo.Name = "lnkTwo";
      this.lnkTwo.Size = new System.Drawing.Size(13, 13);
      this.lnkTwo.TabIndex = 23;
      this.lnkTwo.TabStop = true;
      this.lnkTwo.Text = "2";
      this.lnkTwo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // lnkThree
      // 
      this.lnkThree.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lnkThree.AutoSize = true;
      this.lnkThree.LinkVisited = true;
      this.lnkThree.Location = new System.Drawing.Point(678, 10);
      this.lnkThree.Name = "lnkThree";
      this.lnkThree.Size = new System.Drawing.Size(13, 13);
      this.lnkThree.TabIndex = 24;
      this.lnkThree.TabStop = true;
      this.lnkThree.Text = "3";
      this.lnkThree.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.panPage_LinkClicked);
      // 
      // gridList
      // 
      this.gridList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
      this.gridList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
      this.gridList.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.gridList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.gridList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gridList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
      this.gridList.Location = new System.Drawing.Point(0, 38);
      this.gridList.Name = "gridList";
      this.gridList.RowHeadersWidth = 25;
      this.gridList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.gridList.Size = new System.Drawing.Size(792, 275);
      this.gridList.StandardTab = true;
      this.gridList.TabIndex = 0;
      this.gridList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridList_CellDoubleClick);
      this.gridList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridList_CellValueChanged);
      this.gridList.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.gridList_RowPrePaint);
      this.gridList.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridList_RowsAdded);
      this.gridList.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.gridList_UserDeletingRow);
      this.gridList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridList_KeyDown);
      // 
      // dataGridViewTextBoxColumn1
      // 
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
      this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
      this.dataGridViewTextBoxColumn1.Frozen = true;
      this.dataGridViewTextBoxColumn1.HeaderText = "Column1";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 73;
      // 
      // CListForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(792, 373);
      this.Controls.Add(this.gridList);
      this.Controls.Add(this.panPage);
      this.Controls.Add(this.panTool);
      this.KeyPreview = true;
      this.Name = "CListForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "ListForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CListForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CListForm_FormClosed);
      this.panTool.ResumeLayout(false);
      this.panTool.PerformLayout();
      this.panPage.ResumeLayout(false);
      this.panPage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panTool;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtFilter;
    private System.Windows.Forms.Button btnPrint;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnEdit;
    private System.Windows.Forms.Button btnDelete;
    private System.Windows.Forms.Button btnAdd;
    private System.Windows.Forms.DataGridView gridList;
    private System.Windows.Forms.LinkLabel lnkBack;
    private System.Windows.Forms.LinkLabel lnkEnd;
    private System.Windows.Forms.LinkLabel lnkForward;
    private System.Windows.Forms.LinkLabel lnkOther;
    private System.Windows.Forms.LinkLabel lnkThree;
    private System.Windows.Forms.LinkLabel lnkTwo;
    private System.Windows.Forms.LinkLabel lnkOne;
    private System.Windows.Forms.LinkLabel lnkBegin;
    private System.Windows.Forms.Button btnFilter;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.Button btnFilterClear;
    private System.Windows.Forms.Button btnNext;
    private System.Windows.Forms.Button btnPrevious;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txtSearch;
    /// <summary>
    /// Панель навигации по списку
    /// </summary>
    protected internal System.Windows.Forms.Panel panPage;
    /// <summary>
    /// Название набора данных
    /// </summary>
    protected internal System.Windows.Forms.Label labTitle;

  }
}