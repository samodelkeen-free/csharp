﻿namespace Aist.Engine.Windows.Forms
{
  partial class CSelectionTreeForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panMain = new System.Windows.Forms.Panel();
      this.labCount = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.btnFilter = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.txtFilter = new System.Windows.Forms.TextBox();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.panInfo = new System.Windows.Forms.Panel();
      this.btnCollapse = new System.Windows.Forms.Button();
      this.btnHierarchy = new System.Windows.Forms.Button();
      this.labParent = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.tvSelect = new System.Windows.Forms.TreeView();
      this.btnFilterClear = new System.Windows.Forms.Button();
      this.panMain.SuspendLayout();
      this.panInfo.SuspendLayout();
      this.SuspendLayout();
      // 
      // panMain
      // 
      this.panMain.Controls.Add(this.btnFilterClear);
      this.panMain.Controls.Add(this.labCount);
      this.panMain.Controls.Add(this.label12);
      this.panMain.Controls.Add(this.btnFilter);
      this.panMain.Controls.Add(this.label1);
      this.panMain.Controls.Add(this.txtFilter);
      this.panMain.Controls.Add(this.btnOk);
      this.panMain.Controls.Add(this.btnCancel);
      this.panMain.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panMain.Location = new System.Drawing.Point(0, 313);
      this.panMain.Name = "panMain";
      this.panMain.Size = new System.Drawing.Size(592, 60);
      this.panMain.TabIndex = 1;
      // 
      // labCount
      // 
      this.labCount.AutoSize = true;
      this.labCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labCount.ForeColor = System.Drawing.Color.Navy;
      this.labCount.Location = new System.Drawing.Point(426, 32);
      this.labCount.Name = "labCount";
      this.labCount.Size = new System.Drawing.Size(14, 13);
      this.labCount.TabIndex = 120;
      this.labCount.Text = "0";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label12.Location = new System.Drawing.Point(289, 32);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(131, 13);
      this.label12.TabIndex = 119;
      this.label12.Text = "Выбрано элементов:";
      // 
      // btnFilter
      // 
      this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilter.Image = global::Aist.Engine.Properties.Resources.search_p04;
      this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnFilter.Location = new System.Drawing.Point(244, 25);
      this.btnFilter.Name = "btnFilter";
      this.btnFilter.Size = new System.Drawing.Size(25, 25);
      this.btnFilter.TabIndex = 3;
      this.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btnFilter.UseVisualStyleBackColor = true;
      this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.Location = new System.Drawing.Point(9, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Фильтр:";
      // 
      // txtFilter
      // 
      this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtFilter.Location = new System.Drawing.Point(12, 28);
      this.txtFilter.Name = "txtFilter";
      this.txtFilter.Size = new System.Drawing.Size(195, 22);
      this.txtFilter.TabIndex = 1;
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnOk.Image = global::Aist.Engine.Properties.Resources.button_ok_p03;
      this.btnOk.Location = new System.Drawing.Point(496, 10);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(40, 40);
      this.btnOk.TabIndex = 4;
      this.btnOk.UseVisualStyleBackColor = true;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Image = global::Aist.Engine.Properties.Resources.button_cancel_p03;
      this.btnCancel.Location = new System.Drawing.Point(542, 10);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(40, 40);
      this.btnCancel.TabIndex = 5;
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // panInfo
      // 
      this.panInfo.Controls.Add(this.btnCollapse);
      this.panInfo.Controls.Add(this.btnHierarchy);
      this.panInfo.Controls.Add(this.labParent);
      this.panInfo.Controls.Add(this.label3);
      this.panInfo.Dock = System.Windows.Forms.DockStyle.Top;
      this.panInfo.Location = new System.Drawing.Point(0, 0);
      this.panInfo.Name = "panInfo";
      this.panInfo.Size = new System.Drawing.Size(592, 60);
      this.panInfo.TabIndex = 2;
      // 
      // btnCollapse
      // 
      this.btnCollapse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCollapse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCollapse.Image = global::Aist.Engine.Properties.Resources.window_nofullscreen_p03;
      this.btnCollapse.Location = new System.Drawing.Point(496, 9);
      this.btnCollapse.Name = "btnCollapse";
      this.btnCollapse.Size = new System.Drawing.Size(40, 40);
      this.btnCollapse.TabIndex = 6;
      this.btnCollapse.UseVisualStyleBackColor = true;
      this.btnCollapse.Click += new System.EventHandler(this.btnCollapse_Click);
      // 
      // btnHierarchy
      // 
      this.btnHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnHierarchy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnHierarchy.Image = global::Aist.Engine.Properties.Resources.view_tree_p03;
      this.btnHierarchy.Location = new System.Drawing.Point(542, 9);
      this.btnHierarchy.Name = "btnHierarchy";
      this.btnHierarchy.Size = new System.Drawing.Size(40, 40);
      this.btnHierarchy.TabIndex = 7;
      this.btnHierarchy.UseVisualStyleBackColor = true;
      this.btnHierarchy.Click += new System.EventHandler(this.btnHierarchy_Click);
      // 
      // labParent
      // 
      this.labParent.AutoSize = true;
      this.labParent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.labParent.ForeColor = System.Drawing.Color.Navy;
      this.labParent.Location = new System.Drawing.Point(9, 31);
      this.labParent.Name = "labParent";
      this.labParent.Size = new System.Drawing.Size(174, 13);
      this.labParent.TabIndex = 117;
      this.labParent.Text = "РОДИТЕЛЬСКИЙ ЭЛЕМЕНТ";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.Location = new System.Drawing.Point(9, 9);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(357, 13);
      this.label3.TabIndex = 116;
      this.label3.Text = "Выберите элементы из каталога для включения в состав:";
      // 
      // tvSelect
      // 
      this.tvSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.tvSelect.CheckBoxes = true;
      this.tvSelect.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tvSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tvSelect.Location = new System.Drawing.Point(0, 60);
      this.tvSelect.Name = "tvSelect";
      this.tvSelect.ShowRootLines = false;
      this.tvSelect.Size = new System.Drawing.Size(592, 253);
      this.tvSelect.TabIndex = 0;
      this.tvSelect.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvSelect_BeforeCheck);
      this.tvSelect.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvSelect_AfterCheck);
      // 
      // btnFilterClear
      // 
      this.btnFilterClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnFilterClear.Image = global::Aist.Engine.Properties.Resources.drop_p04;
      this.btnFilterClear.Location = new System.Drawing.Point(213, 25);
      this.btnFilterClear.Name = "btnFilterClear";
      this.btnFilterClear.Size = new System.Drawing.Size(25, 25);
      this.btnFilterClear.TabIndex = 2;
      this.btnFilterClear.UseVisualStyleBackColor = true;
      this.btnFilterClear.Click += new System.EventHandler(this.btnFilterClear_Click);
      // 
      // CSelectionTreeForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(592, 373);
      this.Controls.Add(this.tvSelect);
      this.Controls.Add(this.panInfo);
      this.Controls.Add(this.panMain);
      this.Name = "CSelectionTreeForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "SelectionTreeForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CSelectionTreeForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CSelectionTreeForm_FormClosed);
      this.Shown += new System.EventHandler(this.CSelectionTreeForm_Shown);
      this.panMain.ResumeLayout(false);
      this.panMain.PerformLayout();
      this.panInfo.ResumeLayout(false);
      this.panInfo.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panMain;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtFilter;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Panel panInfo;
    private System.Windows.Forms.Label labParent;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TreeView tvSelect;
    private System.Windows.Forms.Button btnFilter;
    private System.Windows.Forms.Label labCount;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Button btnHierarchy;
    /// <summary>
    /// Свернуть дерево
    /// </summary>
    protected System.Windows.Forms.Button btnCollapse;
    private System.Windows.Forms.Button btnFilterClear;

  }
}