﻿namespace Aist.Engine.Windows.Forms
{
  partial class CSelectionCheckBoxForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbItems = new System.Windows.Forms.CheckedListBox();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lbItems
      // 
      this.lbItems.CheckOnClick = true;
      this.lbItems.Dock = System.Windows.Forms.DockStyle.Top;
      this.lbItems.FormattingEnabled = true;
      this.lbItems.Location = new System.Drawing.Point(0, 0);
      this.lbItems.MultiColumn = true;
      this.lbItems.Name = "lbItems";
      this.lbItems.Size = new System.Drawing.Size(294, 214);
      this.lbItems.TabIndex = 0;
      this.lbItems.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lbItems_ItemCheck);
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnOk.Image = global::Aist.Engine.Properties.Resources.button_ok_p03;
      this.btnOk.Location = new System.Drawing.Point(200, 227);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(40, 40);
      this.btnOk.TabIndex = 1;
      this.btnOk.UseVisualStyleBackColor = true;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Image = global::Aist.Engine.Properties.Resources.button_cancel_p03;
      this.btnCancel.Location = new System.Drawing.Point(246, 227);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(40, 40);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // CSelectionCheckBoxForm
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(294, 275);
      this.ControlBox = false;
      this.Controls.Add(this.btnOk);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.lbItems);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CSelectionCheckBoxForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "CheckBoxForm";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CCheckBoxForm_FormClosed);
      this.Shown += new System.EventHandler(this.CSelectionCheckBoxForm_Shown);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckedListBox lbItems;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
  }
}