﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System;
using System.Windows.Forms;
using Aist.Engine.Windows.Controls;
using Aist.Engine.Windows.Docs;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Windows.Forms
{
  /// <summary>
  /// Форма
  /// </summary>
  public partial class CSelectionCheckBoxForm : Form
  {
    #region Variable declarations

    /// <summary>
    /// Документ формы
    /// </summary>
    private readonly CSelectionCheckBoxFormDoc m_pFormDoc;
    /// <summary>
    /// Объект для работы с дочерними формами
    /// </summary>
    private CChildFormProcessor m_pChildFormProcessor;

    #endregion

    #region Form constructors

    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pSelectionFormCheckBox">Объект списка элементов-флажков</param>
    /// <param name="pChildFormProcessor">Объект для работы с дочерними формами</param>
    public CSelectionCheckBoxForm(CAppConfig pAppConfig, object pSelectionFormCheckBox, CChildFormProcessor pChildFormProcessor)
    {
      InitializeComponent();
      try {
        m_pFormDoc = new CSelectionCheckBoxFormDoc(pAppConfig, pSelectionFormCheckBox);
        m_pFormDoc.Open();
        m_pChildFormProcessor = pChildFormProcessor;

        h_fillListBox();
        if (m_pFormDoc.ElementList.OnlyElementMode) {
          //labTitle.Text = m_pFormDoc.ElementList.Title;
          //labParent.Text = string.Empty;
        }
        else {
          //labParent.Text = m_pFormDoc.ElementList.Title;
        }
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods

    /// <summary>
    /// Заполнение листбокса значениями
    /// </summary>
    private void h_fillListBox()
    {
      m_pFormDoc.ElementList.DbSelect("");
      lbItems.Items.Clear();
      for (int ii = 0; ii < m_pFormDoc.ElementList.Count; ++ii) {
        int iIndex = lbItems.Items.Add(m_pFormDoc.ElementList[ii]);
        lbItems.SetItemChecked(iIndex, m_pFormDoc.ElementList[ii].Checked);
      }
      Text = m_pFormDoc.ElementList.FormName;
    }

    #endregion

    #region Control handlers

    private void CCheckBoxForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pFormDoc.Close();
    }

    private void lbItems_ItemCheck(object sender, ItemCheckEventArgs e)
    {
      if ((sender as CheckedListBox).SelectedItem != null) {
        if (m_pFormDoc.ElementList.OnlyElementMode) {
          m_pFormDoc.ElementList.UnCheckAll();
          if (e.NewValue == CheckState.Checked)
            foreach (int ii in lbItems.CheckedIndices) if (e.Index != ii) lbItems.SetItemChecked(ii, false);
        }
        ((sender as CheckedListBox).SelectedItem as CCBoxItem).Checked = (e.NewValue == CheckState.Checked);
      }
    }

    private void CSelectionCheckBoxForm_Shown(object sender, EventArgs e)
    {
      //h_fillListBox();
      for (int ii = 0; ii < lbItems.Items.Count; ++ii) lbItems.SetItemChecked(ii, (lbItems.Items[ii] as CCBoxItem).Checked);
    }

    #endregion

  }
}
