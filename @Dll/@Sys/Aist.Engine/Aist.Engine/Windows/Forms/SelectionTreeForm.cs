﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Aist.Engine.Data.Structures;
using Aist.Engine.Windows.Docs;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Windows.Forms
{
  /// <summary>
  /// Форма
  /// </summary>
  public partial class CSelectionTreeForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private readonly CSelectionTreeFormDoc m_pSelectionTreeFormDoc;
    /// <summary>
    /// Объект для работы с дочерними формами
    /// </summary>
    private CChildFormProcessor m_pChildFormProcessor;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pSelectionFormTree">Объект дерева элементов</param>
    /// <param name="pChildFormProcessor">Объект для работы с дочерними формами</param>
    public CSelectionTreeForm(CAppConfig pAppConfig, object pSelectionFormTree, CChildFormProcessor pChildFormProcessor)
    {
      InitializeComponent();
      try {
        m_pSelectionTreeFormDoc = new CSelectionTreeFormDoc(pAppConfig, pSelectionFormTree);
        m_pSelectionTreeFormDoc.Open();
        m_pChildFormProcessor = pChildFormProcessor;

        ImageList imgTvSelect = new ImageList();
        imgTvSelect.ColorDepth = ColorDepth.Depth32Bit;
        imgTvSelect.Images.Add(Properties.Resources.box_not_checked);
        if (!m_pSelectionTreeFormDoc.ElementTree.OnlyElementMode) {
          imgTvSelect.Images.Add(Properties.Resources.box_checked);
        }
        else {
          imgTvSelect.Images.Add(Properties.Resources.ball_green);
        }
        tvSelect.StateImageList = imgTvSelect;
        tvSelect.Tag = true;

        h_fillTree();
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods
    /// <summary>
    /// Вызов формы работы с иерархией элементов
    /// </summary>
    private void h_treeForm()
    {
      if (m_pChildFormProcessor == null) return;
      CChildFormProcessorImplementation pChildFormProcessor = new CChildFormProcessorImplementation();
      Form pForm = pChildFormProcessor.CreateFormByClass(m_pSelectionTreeFormDoc.ElementTree.TreeFormClass,
                                                         new Type[] { typeof(CAppConfig), typeof(CTreeDescription), typeof(CChildFormProcessor) },
                                                         new object[] { m_pSelectionTreeFormDoc.AppConfig, m_pSelectionTreeFormDoc.ElementTree.TreeDescription, m_pChildFormProcessor }
        );
      if (pForm != null && !pForm.IsDisposed) {
        DialogResult pDialogResult = pForm.ShowDialog();
        if (pDialogResult == DialogResult.OK) {
          h_fillTree();
          h_refreshTree();
        }
        else {

        }
      }
    }

    /// <summary>
    /// Добавление ветви в дерево
    /// </summary>
    private void h_tvSelectAddNode(TreeNode pParentElementNode, CSelectionTree pParentElement, bool bUseInfo)
    {
      if (pParentElement.SelectionList != null) {
        foreach (CSelectionTree pChildElement in pParentElement.SelectionList) {
          if (pChildElement.SelectionItem.Visible) {
            TreeNode pChildElementNode = new TreeNode(
              (bUseInfo && pChildElement.SelectionItem.Info != "")
                ? string.Format("{0} ({1})", pChildElement.SelectionItem.Name, pChildElement.SelectionItem.Info)
                : pChildElement.SelectionItem.Name
              );
            pChildElementNode.Tag = pChildElement.SelectionItem.Identifier;
            pChildElementNode.NodeFont = new Font(tvSelect.Font.Name, tvSelect.Font.Size, FontStyle.Regular);
            pChildElementNode.Checked = pChildElement.SelectionItem.Checked;
            pParentElementNode.Nodes.Add(pChildElementNode);
            h_tvSelectAddNode(pChildElementNode, pChildElement, bUseInfo);
          }
        }
      }
    }

    /// <summary>
    /// Заполнение дерева
    /// </summary>
    private void h_tvSelectFill(List<CSelectionTree> pRootElementList, bool bUseInfo)
    {
      try {
        tvSelect.Nodes.Clear();
        foreach (CSelectionTree pSelectionTree in pRootElementList) {
          if (pSelectionTree.SelectionItem.Visible) {
            TreeNode pRootNode = new TreeNode(
              (bUseInfo && pSelectionTree.SelectionItem.Info != "")
                ? string.Format("{0} ({1})", pSelectionTree.SelectionItem.Name, pSelectionTree.SelectionItem.Info)
                : pSelectionTree.SelectionItem.Name
              );
            pRootNode.Tag = pSelectionTree.SelectionItem.Identifier;
            pRootNode.Checked = pSelectionTree.SelectionItem.Checked;
            tvSelect.Nodes.Add(pRootNode);
            h_tvSelectAddNode(pRootNode, pSelectionTree, bUseInfo);
          }
        }
        tvSelect.ExpandAll();
      }
      catch (Exception ex) {
        MessageBox.Show(string.Format("Ошибка поиска зависимостей:\r\n{0}", ex.Message));
      }
    }

    /// <summary>
    /// Заполнение дерева
    /// </summary>
    private void h_fillTree()
    {
      m_pSelectionTreeFormDoc.ElementTree.DbSelect(txtFilter.Text);
      h_tvSelectFill(m_pSelectionTreeFormDoc.ElementTree.SelectionTreeList, m_pSelectionTreeFormDoc.ElementTree.UseInfo);
      Text = m_pSelectionTreeFormDoc.ElementTree.FormName;
    }

    /// <summary>
    /// Обновление дерева
    /// </summary>
    private void h_refreshTree()
    {
      m_pSelectionTreeFormDoc.ElementTree.Filter(txtFilter.Text);
      h_tvSelectFill(m_pSelectionTreeFormDoc.ElementTree.SelectionTreeList, m_pSelectionTreeFormDoc.ElementTree.UseInfo);
      Text = m_pSelectionTreeFormDoc.ElementTree.FormName;
    }

    #endregion

    #region Control handlers
    private void CSelectionTreeForm_Shown(object sender, EventArgs e)
    {
      h_refreshTree();
      labCount.Text = m_pSelectionTreeFormDoc.ElementTree.CheckedCount().ToString();
    }

    private void CSelectionTreeForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pSelectionTreeFormDoc.Close();
    }

    private void btnFilter_Click(object sender, EventArgs e)
    {
      h_refreshTree();
    }

    private void CSelectionTreeForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (this.DialogResult == DialogResult.OK && m_pSelectionTreeFormDoc.ElementTree.FindChecked() == null) {
        e.Cancel = true;
      }
    }

    private void tvSelect_AfterCheck(object sender, TreeViewEventArgs e)
    {
      if (!(bool)e.Node.TreeView.Tag) {
        return;
      }
      m_pSelectionTreeFormDoc.CheckElement(e.Node.Tag, e.Node.Checked);
      labCount.Text = m_pSelectionTreeFormDoc.ElementTree.CheckedCount().ToString();
    }

    private void tvSelect_BeforeCheck(object sender, TreeViewCancelEventArgs e)
    {
      if (!(bool)e.Node.TreeView.Tag) {
        return;
      }
      if (m_pSelectionTreeFormDoc.ElementTree.OnlyElementMode && !e.Node.Checked) {
        CWindowToolkit.CheckUncheckTreeView(e.Node.TreeView, false);
      }
    }

    private void btnHierarchy_Click(object sender, EventArgs e)
    {
      // Обработка нажатия на кнопку "Иерархия"
      h_treeForm();
    }

    private void btnCollapse_Click(object sender, EventArgs e)
    {
      if (tvSelect.Nodes.Count > 1) {
        foreach (TreeNode pTreeNode in tvSelect.Nodes) {
          pTreeNode.Collapse();
        }
      }
      else {
        if (tvSelect.Nodes.Count == 1) {
          CWindowToolkit.TreeNodeChildCollapse(tvSelect.Nodes[0]);
        }
      }
    }

    private void btnFilterClear_Click(object sender, EventArgs e)
    {
      txtFilter.Text = string.Empty;
      h_refreshTree();
    }

    #endregion
  }
}
