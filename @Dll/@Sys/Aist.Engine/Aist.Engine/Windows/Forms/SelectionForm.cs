﻿#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Using

using System;
using System.Drawing;
using System.Windows.Forms;
using Aist.Engine.Data.Structures;
using Aist.Engine.Windows.Docs;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Windows.Forms
{
  /// <summary>
  /// Форма
  /// </summary>
  public partial class CSelectionForm : Form
  {
    #region Variable declarations
    /// <summary>
    /// Документ формы
    /// </summary>
    private CSelectionFormDoc m_pSelectionFormDoc;
    /// <summary>
    /// Источник элементов для отображения
    /// </summary>
    private BindingSource m_bsSelectionList;
    /// <summary>
    /// Объект для работы с дочерними формами
    /// </summary>
    private CChildFormProcessor m_pChildFormProcessor;

    #endregion

    #region Form constructors
    /// <summary>
    /// Конструктор формы
    /// </summary>
    /// <param name="pAppConfig">Конфигуратор</param>
    /// <param name="pSelectionFormList">Объект списка элементов</param>
    /// <param name="pChildFormProcessor">Объект для работы с дочерними формами</param>
    public CSelectionForm(CAppConfig pAppConfig, object pSelectionFormList, CChildFormProcessor pChildFormProcessor)
    {
      InitializeComponent();
      try {
        m_pSelectionFormDoc = new CSelectionFormDoc(pAppConfig, pSelectionFormList);
        m_pSelectionFormDoc.Open();
        m_pChildFormProcessor = pChildFormProcessor;
        btnDictionary.Enabled = m_pSelectionFormDoc.ElementList.GridDescription != null;

        m_bsSelectionList = new BindingSource();
        gridSelect.DataSource = m_bsSelectionList;
        gridSelect.AutoGenerateColumns = false;
        m_bsSelectionList.DataSource = m_pSelectionFormDoc.ElementList;
        gridSelect.Columns[1].HeaderText = m_pSelectionFormDoc.ElementList.FirstFieldTitle;
        gridSelect.Columns[2].HeaderText = m_pSelectionFormDoc.ElementList.SecondFieldTitle;

        h_fillGrid();
        if (m_pSelectionFormDoc.ElementList.OnlyElementMode) {
          labTitle.Text = m_pSelectionFormDoc.ElementList.Title;
          labParent.Text = string.Empty;
        }
        else {
          labParent.Text = m_pSelectionFormDoc.ElementList.Title;
        }
      }
      catch (Exception pEx) {
        this.Dispose();
        MessageBox.Show(String.Format("Ошибка при создании формы: {0}", pEx.Message));
      }
    }

    #endregion

    #region Help methods
    /// <summary>
    /// Вызов формы работы со справочником элементов
    /// </summary>
    private void h_listForm()
    {
      if (m_pChildFormProcessor == null) return;
      CChildFormProcessorImplementation pChildFormProcessor = new CChildFormProcessorImplementation();
      Form pForm = pChildFormProcessor.CreateFormByClass(m_pSelectionFormDoc.ElementList.ListFormClass,
                                                         new Type[] { typeof(CAppConfig), typeof(CGridDescription), typeof(CChildFormProcessor) },
                                                         new object[] { m_pSelectionFormDoc.AppConfig, m_pSelectionFormDoc.ElementList.GridDescription, m_pChildFormProcessor }
        );
      if (pForm != null && !pForm.IsDisposed) {
        DialogResult pDialogResult = pForm.ShowDialog();
        if (pDialogResult == DialogResult.OK) {
          h_fillGrid();
        }
        else {

        }
      }
    }

    /// <summary>
    /// Служебный метод для работы с сеткой
    /// </summary>
    private void h_setDataGridViewImageStatus(object sender, DataGridViewCellFormattingEventArgs e)
    {
      string CheckColumnName = "colCheckState";
      string colName = ((DataGridView)sender).Columns[CheckColumnName].Name.ToString();
      e.Value = h_getStateImage((bool)((DataGridView)sender).Rows[e.RowIndex].Cells[colName].Value, m_pSelectionFormDoc.ElementList.OnlyElementMode);
    }

    /// <summary>
    /// Метод устанавливает статус-картинку для текущей записи
    /// </summary>
    /// <param name="bCheckState">Признак выделения записи</param>
    /// <param name="bSingleRecordMode">Режим выделения записей сетки</param>
    /// <returns></returns>
    static private Image h_getStateImage(bool bCheckState, bool bSingleRecordMode)
    {
      Image imgRes = null;
      switch (bCheckState) {
        case true: // Запись помечена
          if (!bSingleRecordMode) {
            imgRes = Properties.Resources.box_checked;
          }
          else {
            imgRes = Properties.Resources.ball_green;
          }
          break;
        case false: // Запись не помечена
          if (!bSingleRecordMode) {
            imgRes = Properties.Resources.box_not_checked;
          }
          break;
      }
      return imgRes;
    }

    /// <summary>
    /// Заполнение грида значениями
    /// </summary>
    private void h_fillGrid()
    {
      m_pSelectionFormDoc.ElementList.DbSelect(txtFilter.Text.Trim());
      m_bsSelectionList.ResetBindings(true);
      Text = m_pSelectionFormDoc.ElementList.FormName;
    }

    #endregion

    #region Control handlers
    private void gridSelect_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (e.ColumnIndex.Equals(0)) {
        h_setDataGridViewImageStatus(sender, e);
      }
    }

    private void CSelectionForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      m_pSelectionFormDoc.Close();
    }

    private void gridSelect_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex == 0 && e.RowIndex > -1) {
        m_pSelectionFormDoc.CheckElement(gridSelect.CurrentRow.DataBoundItem as CSelectionItem);
        gridSelect.Refresh();
        labCount.Text = m_pSelectionFormDoc.ElementList.CheckedCount().ToString();
      }
    }

    private void btnFilter_Click(object sender, EventArgs e)
    {
      h_fillGrid();
    }

    private void CSelectionForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (this.DialogResult == DialogResult.OK && m_pSelectionFormDoc.ElementList.FindChecked() == null && !m_pSelectionFormDoc.ElementList.EmptyElementMode) {
        e.Cancel = true;
      }
    }

    private void btnDictionary_Click(object sender, EventArgs e)
    {
      // Обработка нажатия на кнопку "Справочник"
      h_listForm();
    }

    private void btnFilterClear_Click(object sender, EventArgs e)
    {
      txtFilter.Text = string.Empty;
      h_fillGrid();
    }

    private void CSelectionForm_Load(object sender, EventArgs e)
    {
      labCount.Text = m_pSelectionFormDoc.ElementList.CheckedCount().ToString();
    }

    #endregion

  }
}
