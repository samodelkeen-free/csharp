﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using


using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Aist.Engine.Classes;

#endregion

namespace Aist.Engine.Windows
{
  #region Class CWindowToolkit

  /// <summary>
  /// Вспомогательные функции для работы с пользовательским интерфейсом
  /// </summary>
  public class CWindowToolkit
  {
    #region Constant declarations

    /// <summary>
    /// Цвет контролов, измененных при редактировании
    /// </summary>
    public static readonly Color EditControlBackColor = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));

    /// <summary>
    /// Цвет контролов, предназначенных к редактированию
    /// </summary>
    public static readonly Color EditableControlBackColor = Color.FromArgb((0xff),(0xff),(0xe1));
    
    /// <summary>
    /// Цвет шрифта неактивных контролов
    /// </summary>
    public static readonly Color InactiveControlForeColor = Color.Gray;

    #endregion

    #region Private methods

    /// <summary>
    /// Отметить/разотметить все ветви коллекции дерева элементов
    /// </summary>
    /// <param name="pTreeNodeCollection">Коллекция ветвей дерева элементов</param>
    /// <param name="bCheckState">Устанавливаемый статус элементов дерева</param>
    private static void h_checkUncheckTreeNode(TreeNodeCollection pTreeNodeCollection, bool bCheckState)
    {
      foreach (TreeNode pTreeNode in pTreeNodeCollection) {
        pTreeNode.Checked = bCheckState;
        if (pTreeNode.Nodes.Count > 0)
          h_checkUncheckTreeNode(pTreeNode.Nodes, bCheckState);
      }
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Отметить/разотметить все ветви дерева элементов
    /// </summary>
    /// <param name="pTreeView">Дерево элементов</param>
    /// <param name="bCheckState">Устанавливаемый статус элементов дерева</param>
    public static void CheckUncheckTreeView(TreeView pTreeView, bool bCheckState)
    {
      // Устанавливаем флаг запрещения исполнения методов AfterCheck и BeforeCheck
      pTreeView.Tag = false;
      try {
        h_checkUncheckTreeNode(pTreeView.Nodes, bCheckState);
      }
      finally {
        pTreeView.Tag = true;
      }
    }

    /// <summary>
    /// Перерисовка пункта меню выбора страницы
    /// </summary>
    /// <param name="pLink">Элемент "Гиперссылка"</param>
    /// <param name="pPageMenuItem">Элемент меню навигации</param>
    public static void RepaintPageMenuItem(LinkLabel pLink, CPageMenuItem pPageMenuItem)
    {
      pLink.Visible = pPageMenuItem.Visible;
      pLink.Text = pPageMenuItem.Text;
      if (pPageMenuItem.Active) {
        pLink.Font = new Font(pLink.Font.Name, pLink.Font.Size, FontStyle.Bold);
      }
      else {
        pLink.Font = new Font(pLink.Font.Name, pLink.Font.Size, FontStyle.Regular);
      }
    }

    /// <summary>
    /// Проверяет существование ветви в объекте типа TreeNode с переданным в параметрах Tag'ом
    /// </summary>
    /// <param name="pTreeNode">Объект дерева элементов типа TreeNode</param>
    /// <param name="pTag">Значение Tag'а</param>
    public static bool IsTreeNodeExists(TreeNode pTreeNode, object pTag)
    {
      foreach (TreeNode pItem in pTreeNode.Nodes) {
        if (pItem.Tag == pTag) {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Сворачивает дерево элементов, начиная с указанной ветви (но не включая ее)
    /// </summary>
    /// <param name="pTreeNode">Объект дерева элементов типа TreeNode</param>
    public static void TreeNodeChildCollapse(TreeNode pTreeNode)
    {
      foreach (TreeNode pItem in pTreeNode.Nodes) {
        pItem.Collapse();
      }
    }

    /// <summary>
    /// Формирует список объектов, содержащихся в тегах развернутых ветвей
    /// </summary>
    /// <param name="pTreeView">Объект дерева элементов типа TreeView</param>
    public static List<object> GetExpandedNodeTagList(TreeView pTreeView)
    {
      List<object> result = new List<object>();
      foreach (TreeNode pNode in pTreeView.Nodes) {
        if (pNode.IsExpanded) {
          result.Add(pNode.Tag);
        }
        result.AddRange(GetExpandedNodeTagList(pNode));
      }
      return result;
    }

    /// <summary>
    /// Формирует список объектов, содержащихся в тегах развернутых ветвей
    /// </summary>
    /// <param name="pTreeNode">Объект ветви элементов типа TreeNode</param>
    public static List<object> GetExpandedNodeTagList(TreeNode pTreeNode)
    {
      List<object> result = new List<object>();
      foreach (TreeNode pNode in pTreeNode.Nodes) {
        if (pNode.IsExpanded) {
          result.Add(pNode.Tag);
        }
        result.AddRange(GetExpandedNodeTagList(pNode));       
      }
      return result;
    }

    #endregion
  }

  #endregion
}
