﻿#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System;
using Aist.Engine.Data.Structures;
using Sfo.Sys.Config;
using Sfo.Sys.Doc;
using Sfo.Sys.Log;

#endregion

namespace Aist.Engine.Data.Db
{
  /// <summary>
  /// Класс для работы с БД
  /// </summary>
  public class CDbCommonDoc : CDoc
  {
    #region Private variables

    /// <summary>
    /// Конфигуратор
    /// </summary>
    protected CAppConfig m_pAppConfig;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    public CDbCommonDoc(CAppConfig pAppConfig)
    {
      m_pAppConfig = pAppConfig;
      LogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="pLogWriter">Объект протоколирования</param>
    public CDbCommonDoc(CAppConfig pAppConfig, ILogWriter pLogWriter)
    {
      m_pAppConfig = pAppConfig;
      LogWriter = pLogWriter;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="pLogWriter">Объект протоколирования</param>
    /// <param name="pUser">Идентификатор пользователя</param>
    public CDbCommonDoc(CAppConfig pAppConfig, ILogWriter pLogWriter, Guid pUser) : this(pAppConfig, pLogWriter)
    {
      //m_pUser = pUser;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="sConnectionString">Строка соединения</param>
    public CDbCommonDoc(CAppConfig pAppConfig, string sConnectionString)
    {
      m_pAppConfig = pAppConfig;
      LogWriter = new CTextFileLogWriter(m_pAppConfig.GetLogRotateDir(), true);
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Заполняет список идентификаторов по запросу к БД
    /// </summary>
    /// <param name="pIdentifierList">Список идентификаторов</param>
    /// <param name="sSql">SQL-запрос на извлечение данных из БД</param>
    /// <returns></returns>
    public virtual void IdentifierListFill(CIdentifierList pIdentifierList, string sSql)
    {
      pIdentifierList.Clear();

    }

    /// <summary>
    /// Выполняет запрос с параметрами к БД. Возвращает одно поле.
    /// </summary>
    /// <param name="sSql">SQL-запрос на выполнение</param>
    /// <param name="arParameter">Список параметров со значениями</param>
    /// <param name="sErrorMessage">Текст ошибки выполнения запроса</param>
    public object ExecuteParametrizedSql(string sSql, CDbCommandParameter[] arParameter, out string sErrorMessage)
    {
      object result = null;
      sErrorMessage = string.Empty;
      return result;
    }

    /// <summary>
    /// Выполняет серию запросов к БД
    /// </summary>   
    public int ExecuteQueryList(string[] arQueryList, out string sErrorMessage)
    {
      int result = 0;
      sErrorMessage = string.Empty;
      return result;
    }

    /// <summary>
    /// Заполняет список типа CSelectionFormList для формы выбора элементов содержимым Хранилища
    /// </summary>
    /// <param name="pSelectionList">Список элементов</param>
    /// <returns></returns>
    virtual public void SelectionListFill(CSelectionFormList pSelectionList)
    {
      pSelectionList.RemoveNoChecked();
      CIdentifierList pIdentifierList = new CIdentifierList();
      foreach (CSelectionItem pSelectionItem in pSelectionList) {
        pIdentifierList.Add(pSelectionItem.Identifier);
      }

    }

    /// <summary>
    /// Заполняет объект типа CSelectionFormTree описания дерева элементов содержимым Хранилища
    /// </summary>
    /// <param name="pSelectionTree">Описание дерева элементов</param>
    /// <returns></returns>
    virtual public void SelectionTreeFill(CSelectionFormTree pSelectionTree)
    {

    }

    /// <summary>
    /// Заполняет объект класса CSelectionFormCheckBox (описания списка элементов-флажков для формы выбора) содержимым Хранилища
    /// </summary>
    /// <param name="pSelectionCheckBox">Описание списка элементов-флажков</param>
    /// <returns></returns>
    virtual public void SelectionCheckBoxFill(CSelectionFormCheckBox pSelectionCheckBox)
    {

    }

    /// <summary>
    /// Выполняет запрос и возвращает значение первого поля в первой записи
    /// (например, для подсчетов количества записей в выборке)
    /// </summary>
    /// <param name="sSql">SQL-запрос на выборку</param>
    /// <returns></returns>
    public object ExecuteQueryWithOneField(string sSql)
    {
      object result = null;
      return result;
    }

    #endregion

    #region Private methods

    /// <summary>
    /// Выполняет запись отладочной информации в лог-файл
    /// </summary>
    /// <param name="sMessage">Текст сообщения</param>
    /// <param name="arParams">Параметры для записи в лог</param>
    protected void h_writeLog(string sMessage, params object[] arParams)
    {
      m_pLogWriter.Write(ELogType.Debug, m_pAppConfig.InstanceName, sMessage, arParams);
    }

    #endregion

    #region Public properties
    /// <summary>
    /// Получает конфигуратор
    /// </summary>
    public CAppConfig AppConfig
    {
      get
      {
        return m_pAppConfig;
      }
    }

    #endregion

    #region CDoc methods

    /// <summary>
    /// Открывает документ.
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
      
    }

    /// <summary>
    /// Закрывает документ.
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {

    }

    #endregion
  }

}
