﻿#region CopyRight
//This code was originally developed 2012-07-20 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Data;

#endregion

namespace Aist.Engine.Data.Db
{
  /// <summary>
  /// Описание параметра команды к БД
  /// </summary>
  public class CDbCommandParameter
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CDbCommandParameter()
    {

    }

    /// <summary>
    /// Входной/выходной параметр
    /// </summary>
    public ParameterDirection Direction
    {
      get;
      set;
    }

    /// <summary>
    /// Имя параметра
    /// </summary>
    public string ParameterName
    {
      get;
      set;
    }

    /// <summary>
    /// Тип параметра
    /// </summary>
    public DbType DbType
    {
      get;
      set;
    }

    /// <summary>
    /// Размер параметра
    /// </summary>
    public int Size
    {
      get;
      set;
    }

    /// <summary>
    /// Значение параметра
    /// </summary>
    public object Value
    {
      get;
      set;
    }

  }
}
