﻿#region CopyRight
//This code was originally developed 2012-05-07 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings
using System.Collections.Generic;
using Aist.Engine.Data.Db;
using Aist.Engine.Windows;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание объекта типа TreeView
  /// </summary>
  public abstract class CTreeDescription : List<CHierarchyTree>
  {
    /// <summary>
    /// Документ базы данных
    /// </summary>
    protected CDbCommonDoc m_pDbDoc;
    /// <summary>
    /// Конфигуратор
    /// </summary>
    protected CAppConfig m_pAppConfig;
    /// <summary>
    /// Название класса формы редактирования элемента
    /// </summary>
    protected string m_sEditFormClass;
    /// <summary>
    /// Название поля-идентификатора в БД
    /// </summary>
    protected string m_sIdentifyField;
    /// <summary>
    /// Название формы дерева элементов
    /// </summary>
    protected string m_sFormName;
    /// <summary>
    /// Запрос к БД на заполнение списка деревьев
    /// </summary>
    protected string m_sSqlSelectRoot;
    /// <summary>
    /// Запрос к БД на добавление ветви к дереву
    /// </summary>
    protected string m_sSqlInsertNode;
    /// <summary>
    /// Список параметров запроса на добавление ветви к дереву
    /// </summary>
    protected CDbCommandParameter[] m_arInsertNodeParameter;
    /// <summary>
    /// Запрос к БД на удаление ветви из дерева
    /// </summary>
    protected string m_sSqlDeleteNode;
    /// <summary>
    /// Признак внесения изменений в дерево иерархии
    /// </summary>
    protected bool m_bChanged = false;

    /// <summary>
    /// Конструктор
    /// </summary>
    protected CTreeDescription(CDbCommonDoc pDbDoc)
    {
      m_pDbDoc = pDbDoc;
      m_pAppConfig = pDbDoc.AppConfig;
      ListFormClass = "Aist.Windows.Forms.CListForm";
      ContextMenuDescription = null;
    }
	
    /// <summary>
    /// Наименование идентификатора в БД
    /// </summary>
    abstract public string IdentifyField
    {
      get;
      set;
    }
    /// <summary>
    /// Название класса формы редактирования
    /// </summary>
    abstract public string EditFormClass
    {
      get;
      set;
    }
    /// <summary>
    /// Название формы деревьев элементов
    /// </summary>
    public string FormName
    {
      get { return string.Format("{0}. Найдено записей: {1}", m_sFormName, CHierarchyTree.SelectedCount(this)); }
      set { m_sFormName = value; }
    }
    /// <summary>
    /// Название класса формы справочника элементов
    /// </summary>
    public string ListFormClass
    {
      get;
      set;
    }
    /// <summary>
    /// Описание объекта типа DataGridView для формы работы со справочником элементов
    /// </summary>
    public CGridDescription GridDescription
    {
      get;
      set; 
    }
    /// <summary>
    /// Признак внесения изменений в дерево иерархии
    /// </summary>
    public bool Changed
    {
      get { return m_bChanged; }
      set { m_bChanged = value; }
    }
    /// <summary>
    /// Запрос к БД на заполнение списка деревьев
    /// </summary>
    public string SqlSelectRoot
    {
      get { return m_sSqlSelectRoot; }
      set { m_sSqlSelectRoot = value; }
    }
    /// <summary>
    /// Объект контекстного меню
    /// </summary>
    public CContextMenuDescription ContextMenuDescription
    {
      get;
      set;
    }

    /// <summary>
    /// Добавление списка элементов к дереву
    /// </summary>
    /// <param name="pHierarchyTree">Родительская ветвь</param>
    /// <param name="pSelectionFormList">Список идентификаторов типа CSelectionFormList</param>
    abstract public void AddElementList(ref CHierarchyTree pHierarchyTree, CSelectionFormList pSelectionFormList);

    /// <summary>
    /// Метод для выборки данных из БД
    /// </summary>
    /// <param name="sFilter">Фильтр для выборки</param>
    /// <param name="sErrorMessage">Текст ошибки выполнения</param>
    /// <returns>Количество полученных элементов</returns>
    abstract public int DbSelect(string sFilter, out string sErrorMessage);

    /// <summary>
    /// Метод фильтрации данных в деревьях
    /// </summary>
    /// <param name="sFilter">Строка-фильтр</param>
    /// <returns>Количество полученных элементов</returns>
    abstract public int Filter(string sFilter);

    /// <summary>
    /// Получает список элементов для формы выбора элементов
    /// </summary>
    /// <param></param>
    /// <returns></returns>
    abstract public CSelectionFormList GetElementList();

    /// <summary>
    /// Метод для сохранения элемента в БД
    /// </summary>
    /// <param name="pTreeItem">Описание элемента дерева</param>
    /// <returns>Признак успешности сохранения</returns>
    public virtual bool SaveElement(CTreeItem pTreeItem)
    {
      if (!pTreeItem.Deleted) {
        if (m_arInsertNodeParameter != null) {
          // Добавляем новую запись в таблицу иерархии элементов:
          m_arInsertNodeParameter[0].Value = pTreeItem.GridItem.Identifier;
          m_arInsertNodeParameter[1].Value = pTreeItem.ParentIdentifier;
          string sErrorMessage;
          pTreeItem.Identifier = (int) m_pDbDoc.ExecuteParametrizedSql(m_sSqlInsertNode, m_arInsertNodeParameter, out sErrorMessage);
          if (string.IsNullOrEmpty(sErrorMessage)) {
            pTreeItem.Saved = true;
          }
          else {
            pTreeItem.ErrorDescription = sErrorMessage;
          }
        }
      }
      else {
        if (pTreeItem.Identifier != null) {
          // Удаляем запись из таблицы иерархии элементов:
          string sErrorMessage;
          if (m_pDbDoc.ExecuteQueryList(new string[] { string.Format(m_sSqlDeleteNode, pTreeItem.Identifier) }, out sErrorMessage) > 0) {
            pTreeItem.Saved = true;
          }
          else {
            pTreeItem.ErrorDescription = sErrorMessage;
          }
        }
        else {
          pTreeItem.Saved = true;
        } 
      }
      if (pTreeItem.Saved) m_bChanged = true;
      return pTreeItem.Saved;
    }

    /// <summary>
    /// Метод для сохранения элемента в таблице-справочнике БД
    /// </summary>
    /// <param name="pGridItem">Описание элемента в таблице-справочнике</param>
    /// <returns>Признак успешности сохранения</returns>
    public virtual bool SaveGridElement(CGridItem pGridItem)
    {
      return pGridItem.Saved;
    }

    /// <summary>
    /// Получает дерево из списка по идентификатору элемента
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    /// <returns>Дерево из списка, соответствующее идентификатору элемента</returns>
    public virtual CHierarchyTree GetTreeByIdentifier(object pIdentifier)
    {
      CHierarchyTree result = null;
      foreach (CHierarchyTree pHierarchyTree in this) {
        if (pHierarchyTree.TreeItem.Identifier.Equals(pIdentifier)) {
          result = pHierarchyTree;
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// Поиск элемента в деревьях по его идентификатору
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    public virtual CHierarchyTree FindIdentifier(object pIdentifier)
    {
      return FindNodeByIdentifier(this, pIdentifier, true);
    }

    /// <summary>
    /// Метод для сохранения изменений в БД
    /// </summary>
    /// <param></param>
    public virtual bool DbSave()
    {
      bool result = true;
      foreach (CHierarchyTree pHierarchyTree in this) {
        bool bResult = true;
        h_dbSave(pHierarchyTree, ref bResult);
        if (!bResult) {
          pHierarchyTree.Visible = true;
          result = false;
        }
        //CHierarchyTree.Delete(pHierarchyTree);
      }
      Delete(this);
      return result;
    }

    /// <summary>
    /// Метод для удаления ветви из дерева
    /// </summary>
    /// <param name="pHierarchyTree">Родительская ветвь</param>
    /// <param name="pChildTree">Дочерняя ветвь для удаления</param>
    /// <returns>Флаг успешности операции удаления</returns>
    public virtual bool DbDelete(CHierarchyTree pHierarchyTree, CHierarchyTree pChildTree)
    {
      pChildTree.TreeItem.Deleted = true;
      pChildTree.TreeItem.Saved = false;
      pChildTree.Visible = false;
      return true;
    }

    /// <summary>
    /// Удаление всех деревьев из списка и их ветвей, помеченных на удаление
    /// </summary>
    /// <param name="pTreeList">Список деревьев</param>
    public static void Delete(List<CHierarchyTree> pTreeList)
    {
      int ii = 0;
      while (ii < pTreeList.Count) {
        if (pTreeList[ii].TreeItem.Deleted && pTreeList[ii].TreeItem.Saved) {
          pTreeList.RemoveAt(ii);
        }
        else {
          CHierarchyTree.Delete(pTreeList[ii]);
          ii++;
        }
      }
    }

    /// <summary>
    /// Проверка списка деревьев на признак сохранения
    /// </summary>
    /// <param name="pTreeList">Список деревьев</param>
    public static bool IsSaved(List<CHierarchyTree> pTreeList)
    {
      bool result = true;
      foreach (CHierarchyTree pHierarchyTree in pTreeList) {
        if (!CHierarchyTree.IsSaved(pHierarchyTree)) {
          result = false;
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// Пометка всех деревьев в списке и их ветвей как уже удаленных в БД
    /// </summary>
    /// <param name="pTreeList">Список деревьев</param>
    public static void MarkDeleted(List<CHierarchyTree> pTreeList)
    {
      foreach (CHierarchyTree pHierarchyTree in pTreeList) {
        CHierarchyTree.MarkDeleted(pHierarchyTree);
      }
    }

    // TODO: EVV WTF???!!!
    /// <summary>
    /// Поиск первой ветви по идентификатору в списке деревьев
    /// </summary>
    /// <param name="pTreeList">Список деревьев элементов</param>
    /// <param name="pIdentifier">Идентификатор элемента</param>
    /// <param name="bRecursive">Флаг поиска по дочерним ветвям</param>
    /// <returns>Ветвь, содержащая элемент с искомым идентификатором</returns>
    public static CHierarchyTree FindNodeByIdentifier(List<CHierarchyTree> pTreeList, object pIdentifier, bool bRecursive)
    {
      CHierarchyTree result = null;
      foreach (CHierarchyTree pHierarchyTree in pTreeList) {
        result = CHierarchyTree.FindNodeByIdentifier(pHierarchyTree, pIdentifier, bRecursive);
        if (result != null) {
          break;
        }
      }
      return result;
    }

    #region Private methods
    /// <summary>
    /// Рекурсивное сохранение структуры дерева элементов в БД
    /// </summary>
    /// <param name="pHierarchyTree">Дерево элементов</param>
    /// <param name="bResult">Результат сохранения дерева</param>
    private void h_dbSave(CHierarchyTree pHierarchyTree, ref bool bResult)
    {
      if (!pHierarchyTree.TreeItem.GridItem.Saved) {
        if (!SaveGridElement(pHierarchyTree.TreeItem.GridItem)) {
          pHierarchyTree.Visible = true;
          bResult = false;
        }
      }
      if (!pHierarchyTree.TreeItem.Saved) {
        if (!SaveElement(pHierarchyTree.TreeItem)) {
          pHierarchyTree.Visible = true;
          bResult = false;
        }
      }
      if (pHierarchyTree.TreeItem.Identifier != null && pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          pItem.TreeItem.ParentIdentifier = pHierarchyTree.TreeItem.Identifier;
          h_dbSave(pItem, ref bResult);
        }
      }
    }

    #endregion

    #region CTreeDescription public service properties
    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbCommonDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }

    #endregion

  }
}
