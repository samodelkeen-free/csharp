﻿#region CopyRight
//This code was originally developed 2012-07-13 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание записи в таблице иерархии элементов
  /// </summary>
  abstract public class CTreeItem
  {
    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    protected object m_pIdentifier;
	  /// <summary>
    /// Идентификатор записи родительского элемента в БД
    /// </summary>
    protected object m_pParentIdentifier;

    /// <summary>
    /// Конструктор класса
    /// </summary>
    protected CTreeItem()
    {
      Saved = true;
      Deleted = false;
      ErrorDescription = null;
    }

    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    abstract public object Identifier
    {
      get;
      set;
    }

	  /// <summary>
    /// Идентификатор записи родительского элемента в БД
    /// </summary>
    abstract public object ParentIdentifier
    {
      get;
      set;
    }

    /// <summary>
    /// Элемент из таблицы-справочника
    /// </summary>
    public CGridItem GridItem
    {
      get;
      set;
    }

    /// <summary>
    /// Признак сохранения записи в БД
    /// </summary>
    virtual public bool Saved
    {
      get;
      set;
    }
    
    /// <summary>
    /// Признак удаления записи из БД
    /// </summary>
    virtual public bool Deleted
    {
      get;
      set;
    }

    /// <summary>
    /// Информационное поле
    /// </summary>
    virtual public string ErrorDescription
    {
      get;
      set;
    }

    /// <summary>
    /// Метод сравнения объектов. Заменяет оригинальный для корректной работы с комбобоксом
    /// </summary>
    public override bool Equals(object obj)
    {
      if (obj.GetType().BaseType == typeof(CTreeItem))
        return Equals(((CTreeItem)obj).Identifier, this.Identifier);
      return base.Equals(obj);
    }

    /// <summary>
    /// 
    /// </summary>
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

  }
}
