﻿#region CopyRight
//This code was originally developed 2012-03-15 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;
using Aist.Engine.Data.Db;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание списка формы выбора элементов
  /// </summary>
  public class CSelectionFormList : List<CSelectionItem>
  {
    /// <summary>
    /// Документ базы данных
    /// </summary>
    private CDbCommonDoc m_pDbDoc;
    /// <summary>
    /// Название формы списка элементов
    /// </summary>
    private string m_sFormName;
    /// <summary>
    /// Описание списка элементов
    /// </summary>
    private string m_sTitle;
    /// <summary>
    /// Шаблон фильтра на запрос к БД
    /// </summary>
    private string m_sSqlFilterTemplate;
    /// <summary>
    /// Итоговый фильтр на запрос к БД
    /// </summary>
    private string m_sSqlFilter = string.Empty;

    /// <summary>
    /// Конструктор
    /// </summary>
    public CSelectionFormList(CDbCommonDoc pDbDoc)
    {
      m_pDbDoc = pDbDoc;
      SqlFilter = string.Empty;
      EmptyElementMode = false;
      ListFormClass = "Aist.Windows.Forms.CListForm";
    }

    /// <summary>
    /// Флаг выбора только одного элемента из списка
    /// </summary>
    public bool OnlyElementMode
    {
      get;
      set;
    }
    /// <summary>
    /// Флаг возможности выбора "пустого" элемента
    /// </summary>
    public bool EmptyElementMode
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на извлечение данных из БД
    /// </summary>
    public string Sql
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на модификацию данных в БД
    /// </summary>
    public string SqlExec
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на получение идентификатора из таблицы-справочника
    /// </summary>
    public string SqlIdentifier
    {
      get;
      set;
    }
    /// <summary>
    /// Условие (фильтр) запроса на извлечение данных
    /// </summary>
    public string SqlFilter
    {
      get { return m_sSqlFilter; }
      set { m_sSqlFilterTemplate = value; }
    }
    /// <summary>
    /// Наименование поля-идентификатора в БД
    /// </summary>
    public string IdentifyField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование вспомогательного поля в БД
    /// </summary>
    public string ObjectField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование первого поля в БД
    /// </summary>
    public string FirstField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование второго поля в БД
    /// </summary>
    public string SecondField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование первого поля на форме
    /// </summary>
    public string FirstFieldTitle
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование второго поля на форме
    /// </summary>
    public string SecondFieldTitle
    {
      get;
      set;
    }
    /// <summary>
    /// Название формы выбора элементов
    /// </summary>
    public string FormName
    {
      get { return string.Format("{0}. Найдено записей: {1}", m_sFormName, Count); }
      set { m_sFormName = value; }
    }
    /// <summary>
    /// Описание списка элементов
    /// </summary>
    public string Title
    {
      get { return m_sTitle; }
      set { m_sTitle = value; }
    }
    /// <summary>
    /// Название класса формы справочника элементов
    /// </summary>
    public string ListFormClass
    {
      get;
      set;
    }
    /// <summary>
    /// Описание объекта типа DataGridView формы справочника элементов
    /// </summary>
    public CGridDescription GridDescription
    {
      get;
      set;
    }

    /// <summary>
    /// Метод выбора данных из базы для списка
    /// </summary>
    /// <param name="sFilter">Фильтр для выборки (выбирает записи из БД + оставляет помеченные)</param>
    public void DbSelect(string sFilter)
    {
      if (!string.IsNullOrEmpty(sFilter)) {
        if (m_sSqlFilterTemplate == string.Empty) {
          m_sSqlFilter = string.Format("AND ([{0}] LIKE '%{2}%' OR [{1}] LIKE '%{2}%')", FirstField, SecondField, sFilter);
        }
        else {
          m_sSqlFilter = string.Format(m_sSqlFilterTemplate, sFilter);
        }
      }
      else {
        m_sSqlFilter = string.Empty;
      }
      m_pDbDoc.SelectionListFill(this);
    }

    /// <summary>
    /// Метод корректировки данных в БД
    /// </summary>
    /// <param name="pParameter">Список параметров запроса</param>
    /// <param name="sErrorMessage">Информационное сообщение</param>
    /// <returns>Флаг успешности запроса</returns>
    public bool DbEdit(object[] pParameter, out string sErrorMessage)
    {
      m_pDbDoc.ExecuteQueryList(new string[] {string.Format(SqlExec, pParameter)}, out sErrorMessage);
      return string.IsNullOrEmpty(sErrorMessage);
    }

    /// <summary>
    /// Поиск элемента в списке по его идентификатору
    /// </summary>
    /// <param></param>
    public CSelectionItem FindIdentifier(object pIdentifier)
    {
      CSelectionItem result = Find(delegate(CSelectionItem pSelectionItem) { return pSelectionItem.Identifier.Equals(pIdentifier); });
      if (result != null) {
        return result;
      }
      return null;
    }

    /// <summary>
    /// Пометка элемента в списке по его идентификатору
    /// </summary>
    /// <param></param>
    public CSelectionItem CheckIdentifier(object pIdentifier)
    {
      CSelectionItem result = Find(delegate(CSelectionItem pSelectionItem) { return pSelectionItem.Identifier.Equals(pIdentifier); });
      if (result != null) {
        result.Checked = true;
        return result;
      }
      return null;
    }

    /// <summary>
    /// Снимает отметки со всех элементов списка
    /// </summary>
    /// <param></param>
    public void UnCheckAll()
    {
      foreach (CSelectionItem pSelectionItem in this) {
        pSelectionItem.Checked = false;
      }
    }

    /// <summary>
    /// Удаление всех не отмеченных элементов
    /// </summary>
    /// <param></param>
    public void RemoveNoChecked()
    {
      CSelectionItem pSelectionItem;
      while ((pSelectionItem = h_findNoChecked()) != null) {
        Remove(pSelectionItem);
      }
    }

    /// <summary>
    /// Поиск первого отмеченного элемента в списке
    /// </summary>
    /// <param></param>
    public CSelectionItem FindChecked()
    {
      CSelectionItem result = Find(delegate(CSelectionItem pSelectionItem) { return pSelectionItem.Checked; });
      if (result != null) {
        return result;
      }
      return null;
    }

    /// <summary>
    /// Подсчет отмеченных элементов
    /// </summary>
    /// <param></param>
    public int CheckedCount()
    {
      int result = 0;
      foreach (CSelectionItem pSelectionItem in this) {
        if (pSelectionItem.Checked) {
          result++;
        }
      }
      return result;
    }

    /// <summary>
    /// Получение идентификатора элемента в таблице-справочнике по идентификатору в таблице истории/иерархии и пр.
    /// </summary>
    /// <param name="pIdentifier">Идентификатор в таблице истории/иерархии и пр.</param>
    public object GetIdentifier(object pIdentifier)
    {
      if (pIdentifier == null) return null;
      return m_pDbDoc.ExecuteQueryWithOneField(string.Format(SqlIdentifier, pIdentifier));
    }

    /// <summary>
    /// Поиск первого не отмеченного элемента в списке
    /// </summary>
    /// <param></param>
    private CSelectionItem h_findNoChecked()
    {
      CSelectionItem result = Find(delegate(CSelectionItem pSelectionItem) { return !pSelectionItem.Checked; });
      if (result != null) {
        return result;
      }
      return null;
    }

  }
}
