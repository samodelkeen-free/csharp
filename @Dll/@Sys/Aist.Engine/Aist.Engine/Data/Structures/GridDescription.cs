﻿#region CopyRight
//This code was originally developed 2012-03-27 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;
using Aist.Engine.Data.Db;
using Sfo.Sys.Config;

#endregion

namespace Aist.Engine.Data.Structures
{
  /* TODO: Убрать!
  /// <summary>
  /// Функция-делегат для выборки данных
  /// </summary>
  public delegate void FSelect(string sFilter);
  /// <summary>
  /// Функция-делегат для добавления и редактирования данных
  /// </summary>
  public delegate void FEdit(object pRecordId);
  /// <summary>
  /// Функция-делегат для удаления данных
  /// </summary>
  public delegate void FDelete(object pRecordId);
  */

  /// <summary>
  /// Описание объекта типа DataGridView
  /// </summary>
  public abstract class CGridDescription : List<object>
  {
    /// <summary>
    /// Документ базы данных
    /// </summary>
    protected CDbCommonDoc m_pDbDoc;
    /// <summary>
    /// Конфигуратор
    /// </summary>
    protected CAppConfig m_pAppConfig;
    /// <summary>
    /// Название класса формы редактирования элемента
    /// </summary>
    protected string m_sEditFormClass;
    /// <summary>
    /// Название поля-идентификатора в БД
    /// </summary>
    protected string m_sIdentifyField;
    /// <summary>
    /// Название формы списка элементов
    /// </summary>
    protected string m_sFormName;
    /// <summary>
    /// Описание списка элементов
    /// </summary>
    protected string m_sTitle;
    /// <summary>
    /// Количество элементов в БД
    /// </summary>
    protected int m_iDbCount = 0;
    /// <summary>
    /// Максимальное количество записей на странице
    /// </summary>
    protected int m_iMaxPageSize = 100;
    /// <summary>
    /// Текущее количество записей на странице
    /// </summary>
    protected int m_iPageSize = 100;
    /// <summary>
    /// Текущий фильтр отбора записей из БД
    /// </summary>
    protected string m_sSqlFilter;
    /// <summary>
    /// Признак внесения изменений в БД
    /// </summary>
    protected bool m_bChanged = false;
    /// <summary>
    /// Запрос к БД (выборка записей)
    /// </summary>
    protected string m_sSqlSelect;
    /// <summary>
    /// Запрос к БД (подсчет записей)
    /// </summary>
    protected string m_sSqlCount;
    /// <summary>
    /// Запрос к БД (поиск записей)
    /// </summary>
    protected string m_sSqlSearch;
    /// <summary>
    /// Запрос к БД (удаление записей)
    /// </summary>
    protected string m_sSqlDelete;
    /// <summary>
    /// Запрос к БД (модификация записей)
    /// </summary>
    protected string m_sSqlUpdate;

    /// <summary>
    /// Конструктор
    /// </summary>
    protected CGridDescription(CDbCommonDoc pDbDoc)
    {
      m_pDbDoc = pDbDoc;
      m_pAppConfig = pDbDoc.AppConfig;
      GridColumn = new List<CGridColumn>();
      FoundIdentifierList = new CIdentifierList();
      Param = null;
    }

    /// <summary>
    /// Описание колонок объекта типа DataGridView
    /// </summary>
    abstract public List<CGridColumn> GridColumn
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование идентификатора в БД
    /// </summary>
    abstract public string IdentifyField
    {
      get;
      set;
    }
    /// <summary>
    /// Название класса формы редактирования
    /// </summary>
    abstract public string EditFormClass
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос к БД (выборка записей)
    /// </summary>
    virtual public string SqlSelect
    {
      get { return m_sSqlSelect; }
      set { m_sSqlSelect = value; }
    }
    /// <summary>
    /// Запрос к БД (подсчет записей)
    /// </summary>
    virtual public string SqlCount
    {
      get { return m_sSqlCount; }
      set { m_sSqlCount = value; }
    }
    /// <summary>
    /// Запрос к БД (поиск записей)
    /// </summary>
    virtual public string SqlSearch
    {
      get { return m_sSqlSearch; }
      set { m_sSqlSearch = value; }
    }
    /// <summary>
    /// Запрос к БД (удаление записей)
    /// </summary>
    virtual public string SqlDelete
    {
      get { return m_sSqlDelete; }
      set { m_sSqlDelete = value; }
    }
    /// <summary>
    /// Запрос к БД (модификация записей)
    /// </summary>
    virtual public string SqlUpdate
    {
      get { return m_sSqlUpdate; }
      set { m_sSqlUpdate = value; }
    }
    /// <summary>
    /// Описание списка найденных идентификаторов
    /// </summary>
    public CIdentifierList FoundIdentifierList
    {
      get;
      set;
    }
    /// <summary>
    /// Название формы списка элементов
    /// </summary>
    public string FormName
    {
      get { return string.Format("{0}. Отфильтровано записей: {1}", m_sFormName, m_iDbCount); }
      set { m_sFormName = value; }
    }
    /// <summary>
    /// Описание списка элементов
    /// </summary>
    public string Title
    {
      get { return m_sTitle; }
      set { m_sTitle = value; }
    }
    /// <summary>
    /// Максимальное количество записей на странице
    /// </summary>
    public int MaxPageSize
    {
      get { return m_iMaxPageSize; }
      set { m_iMaxPageSize = value; }
    }
    /// <summary>
    /// Признак внесения изменений в БД
    /// </summary>
    public bool Changed
    {
      get { return m_bChanged; }
      set { m_bChanged = value; }
    }
    /// <summary>
    /// Хранит текущий фильтр выборки
    /// </summary>
    public string Filter 
    { 
      get;
      set;
    }
    /// <summary>
    /// Список параметров запроса
    /// </summary>
    public object[] Param
    {
      get;
      set;
    }

    /// <summary>
    /// Возвращает страницу, содержащую запись с указанным порядковым номером
    /// </summary>
    /// <param name="iRowRank">Порядковый номер записи в выборке</param>
    /// <returns></returns>
    public int GetElementPage(int iRowRank)
    {
      return iRowRank / m_iPageSize + ((iRowRank % m_iPageSize) > 0 ? 1 : 0);
    }

    /// <summary>
    /// Добавление элемента в список
    /// </summary>
    /// <param></param>
    abstract public int AddItem();

    /// <summary>
    /// Поиск первого не сохраненного элемента в списке
    /// </summary>
    /// <param></param>
    protected CGridItem h_findNotSaved()
    {
      object result = Find(delegate(object pGridItem) { return ((CGridItem)pGridItem).Saved == false; });
      if (result != null) {
        return (CGridItem) result;
      }
      return null;
    }

    /// <summary>
    /// Запись всех не сохраненных элементов в БД с удалением тех, что невозможно записать
    /// </summary>
    /// <param></param>
    virtual public int SaveListWithRemove()
    {
      int ii = 0;
      CGridItem pGridItem;
      while ((pGridItem = h_findNotSaved()) != null) {
        if (DbEdit(pGridItem)) {
          ii++;
        }
        else {
          Remove(pGridItem);
        }
      }
      return ii;
    }

    /// <summary>
    /// Поиск элемента в списке по его идентификатору
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента</param>
    /// <returns>Найденный элемент, либо Null, если элемент не найден</returns>
    public CGridItem FindItemByIdentifier(object pIdentifier)
    {
      object result = Find(delegate(object pGridItem) { return ((CGridItem)pGridItem).Identifier.Equals(pIdentifier); });
      if (result != null) {
        return (CGridItem)result;
      }
      return null;
    }

    /// <summary>
    /// Запись всех не сохраненных элементов в БД
    /// </summary>
    /// <param></param>
    virtual public int DbSave()
    {
      int ii = 0;
      foreach (CGridItem pGridItem in this) {
        if (!pGridItem.Saved) {
          if (DbEdit(pGridItem)) {
          }
          else {
            ii++;
          }
        }
      }
      return ii;
    }

    /// <summary>
    /// Метод для выборки данных
    /// </summary>
    /// <param name="sFilter">Фильтр для выборки</param>
    /// <param name="iSize">Размер страницы (в записях)</param>
    /// <param name="iPage">Номер страницы в выборке (возвращаемый)</param>
    /// <returns>Общее количество страниц в выборке</returns>
    abstract public int DbSelect(string sFilter, int iSize, ref int iPage);

    /// <summary>
    /// Метод для поиска по БД
    /// </summary>
    /// <param name="sSearch">Значение для поиска</param>
    /// <returns></returns>
    abstract public void DbSearch(string sSearch);

    /// <summary>
    /// Метод для добавления и редактирования данных
    /// </summary>
    /// <param name="pRecord">Объект строки</param>
    abstract public bool DbEdit(object pRecord);

    /// <summary>
    /// Метод для удаления данных
    /// </summary>
    /// <param name="pRecord">Объект строки</param>
    /// <returns>Флаг успешности операции удаления</returns>
    abstract public bool DbDelete(object pRecord);

    /// <summary>
    /// Метод для совершения произвольного действия над записью
    /// </summary>
    /// <param name="pRecord">Объект строки</param>
    /// <param name="sCode">Команда</param>
    /// <returns></returns>
    virtual public void DoSomething(object pRecord, string sCode)
    {
      
    }

    /// <summary>
    /// Метод предобработчика списка элементов для создания формы выбора значения из списка/дерева
    /// </summary>
    /// <param name="sFieldName">Наименование поля</param>
    /// <param name="pGridItem">Объект строки грида</param>
    /// <param name="pSelectionFormList">Список элементов</param>
    /// <returns></returns>
    virtual public void GetSelectionFormList(string sFieldName, object pGridItem, object pSelectionFormList)
    {

    }

    /// <summary>
    /// Метод постобработчика списка элементов формы выбора значения из списка/дерева
    /// </summary>
    /// <param name="sFieldName">Наименование поля</param>
    /// <param name="pGridItem">Объект строки грида</param>
    /// <param name="pSelectionFormList">Объект списка значений</param>
    /// <returns></returns>
    virtual public void GetSelectionFormItem(string sFieldName, object pGridItem, object pSelectionFormList)
    {

    }

    /// <summary>
    /// Метод для поиска элемента в БД (используется для проверки наличия элемента в списках выбора)
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента</param>
    /// <param name="pValue">Значение элемента</param>
    /// <param name="pSelectionFormList">Объект списка значений</param>
    /// <returns>Идентификатор элемента списка</returns>
    virtual public object DbSearch(object pIdentifier, object pValue, object pSelectionFormList)
    {
      return pIdentifier;
    }

    /// <summary>
    /// Проверка списка элементов на признак сохранения
    /// </summary>
    virtual public bool IsSaved()
    {
      return h_findNotSaved() == null;
    }

    /// <summary>
    /// Текстовая интерпретация содержимого записи
    /// </summary>
    virtual public string GetElementText(object pRecord)
    {
      return "ВЫДЕЛЕННЫЙ ЭЛЕМЕНТ";
    }

    #region CGridDescription public service properties
    /// <summary>
    /// Получает Документ базы данных
    /// </summary>
    public CDbCommonDoc DbDoc
    {
      get
      {
        return m_pDbDoc;
      }
    }

    #endregion

  }
}
