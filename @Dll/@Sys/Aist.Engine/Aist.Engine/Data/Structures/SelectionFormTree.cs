﻿#region CopyRight
//This code was originally developed 2012-03-21 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;
using Aist.Engine.Data.Db;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Класс описания дерева элементов для формы выбора элемента
  /// </summary>
  public class CSelectionFormTree
  {
    /// <summary>
    /// Документ базы данных
    /// </summary>
    private CDbCommonDoc m_pDbDoc;
    /// <summary>
    /// Название формы дерева элементов
    /// </summary>
    private string m_sFormName;

    /// <summary>
    /// Конструктор
    /// </summary>
    public CSelectionFormTree(CDbCommonDoc pDbDoc)
    {
      m_pDbDoc = pDbDoc;
      //SelectionTree = new CSelectionTree();
      SelectionTreeList = new List<CSelectionTree>();
      SqlFilter = string.Empty;
      TreeFormClass = "Aist.Windows.Forms.CTreeForm";
    }
    /* (EVV: Исключил. Видимо, не понадобится...)
    /// <summary>
    /// Дерево элементов
    /// </summary>
    public CSelectionTree SelectionTree
    {
      get;
      set;
    }
     */
    /// <summary>
    /// Список деревьев корневых элементов
    /// </summary>
    public List<CSelectionTree> SelectionTreeList
    {
      get;
      set;
    }
    /// <summary>
    /// Флаг выбора только одного элемента из списка
    /// </summary>
    public bool OnlyElementMode
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на извлечение родительского элемента из БД
    /// </summary>
    public string SqlParent
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на извлечение дочерних элементов из БД
    /// </summary>
    public string SqlChild
    {
      get;
      set;
    }
    /// <summary>
    /// Условие (фильтр) на запросы
    /// </summary>
    public string SqlFilter
    {
      get;
      set;
    }
    /// <summary>
    /// Запрос на получение идентификатора из таблицы-справочника
    /// </summary>
    public string SqlIdentifier
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование идентификатора в БД
    /// </summary>
    public string IdentifyField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование первого поля в БД
    /// </summary>
    public string FirstField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование второго поля в БД
    /// </summary>
    public string SecondField
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование первого поля на форме
    /// </summary>
    public string FirstFieldTitle
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование второго поля на форме
    /// </summary>
    public string SecondFieldTitle
    {
      get;
      set;
    }
    /// <summary>
    /// Флаг необходимости отображения дополнительной информации (второго поля) на дереве
    /// </summary>
    public bool UseInfo
    {
      get;
      set;
    }
    /// <summary>
    /// Название формы деревьев элементов
    /// </summary>
    public string FormName
    {
      get { return string.Format("{0}. Найдено записей: {1}", m_sFormName, h_selectedCount(SelectionTreeList)); }
      set { m_sFormName = value; }
    }
    /// <summary>
    /// Название класса формы дерева элементов
    /// </summary>
    public string TreeFormClass
    {
      get;
      set;
    }
    /// <summary>
    /// Описание объекта типа TreeView формы дерева элементов
    /// </summary>
    public CTreeDescription TreeDescription
    {
      get;
      set;
    }

    /// <summary>
    /// Метод выбора данных из базы для деревьев
    /// </summary>
    /// <param name="sFilter">Строка-фильтр</param>
    public void DbSelect(string sFilter)
    {
      // Заполняет деревья информацией из БД:
      m_pDbDoc.SelectionTreeFill(this);
      // Фильтрует элементы деревьев в соответствии со строкой-фильтром:
      h_filterTreeList(SelectionTreeList, sFilter);
    }

    /// <summary>
    /// Метод фильтрации данных в деревьях
    /// </summary>
    /// <param name="sFilter">Строка-фильтр</param>
    public void Filter(string sFilter)
    {
      // Фильтрует элементы деревьев в соответствии со строкой-фильтром:
      h_filterTreeList(SelectionTreeList, sFilter);
    }

    /// <summary>
    /// Поиск первого отмеченного элемента
    /// </summary>
    /// <param></param>
    public CSelectionItem FindChecked()
    {
      List<CSelectionItem> pCheckedItemList = h_getCheckedItemList(SelectionTreeList);
      if (pCheckedItemList != null) {
        return pCheckedItemList[0];
      }
      return null;
    }

    /// <summary>
    /// Поиск элемента по его идентификатору
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    public CSelectionItem FindIdentifier(object pIdentifier)
    {
      CSelectionItem result = h_getItemByIdentifier(SelectionTreeList, pIdentifier);
      return result;
    }

    /// <summary>
    /// Пометка элемента по его идентификатору
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    public CSelectionItem CheckIdentifier(object pIdentifier)
    {
      CSelectionItem result = h_getItemByIdentifier(SelectionTreeList, pIdentifier);
      if (result != null) {
        result.Checked = true;
      }
      return result;
    }

    /// <summary>
    /// Снимает отметки выбора со всех элементов дерева
    /// </summary>
    /// <param></param>
    public void UnCheckAll()
    {
      foreach (CSelectionTree pSelectionTree in SelectionTreeList) {
        h_unCheckAll(pSelectionTree);
      }
    }

    /// <summary>
    /// Подсчет выбранных элементов
    /// </summary>
    /// <param></param>
    public int CheckedCount()
    {
      return h_checkedCount(SelectionTreeList);
    }

    /// <summary>
    /// Получение идентификатора элемента в таблице-справочнике по идентификатору в таблице истории
    /// </summary>
    /// <param name="pIdentifier">Идентификатор в таблице истории</param>
    public object GetIdentifier(object pIdentifier)
    {
      if (pIdentifier == null) return null;
      return m_pDbDoc.ExecuteQueryWithOneField(string.Format(SqlIdentifier, pIdentifier));
    }

    /// <summary>
    /// Фильтрация списка деревьев по строке
    /// </summary>
    /// <param name="pSelectionTreeList">Список деревьев элементов</param>
    /// <param name="sFilter">Строка-фильтр</param>
    private static void h_filterTreeList(List<CSelectionTree> pSelectionTreeList, string sFilter)
    {
      sFilter = sFilter.Trim().ToUpper();
      foreach (CSelectionTree pSelectionTree in pSelectionTreeList) {
        h_findByFilter(pSelectionTree, sFilter);
      }
    }

    /// <summary>
    /// Подсчет выделенных элементов в списке деревьев
    /// </summary>
    /// <param name="pSelectionTreeList">Список деревьев элементов</param>
    private static int h_selectedCount(List<CSelectionTree> pSelectionTreeList)
    {
      int result = 0;
      foreach (CSelectionTree pSelectionTree in pSelectionTreeList) {
        result += h_findSelected(pSelectionTree);
      }
      return result;
    }

    /// <summary>
    /// Подсчет выбранных элементов в списке деревьев
    /// </summary>
    /// <param name="pSelectionTreeList">Список деревьев элементов</param>
    private static int h_checkedCount(List<CSelectionTree> pSelectionTreeList)
    {
      int result = 0;
      foreach (CSelectionTree pSelectionTree in pSelectionTreeList) {
        result += h_findChecked(pSelectionTree);
      }
      return result;
    }

    /// <summary>
    /// Формирование списка выбранных элементов
    /// </summary>
    /// <param name="pSelectionTreeList">Список деревьев элементов</param>
    private static List<CSelectionItem> h_getCheckedItemList(List<CSelectionTree> pSelectionTreeList)
    {
      List<CSelectionItem> result = null;
      foreach (CSelectionTree pSelectionTree in pSelectionTreeList) {
        h_findChecked(pSelectionTree, ref result);
      }
      return result;
    }

    /// <summary>
    /// Поиск элемента по идентификатору в списке деревьев
    /// </summary>
    /// <param name="pSelectionTreeList">Список деревьев элементов</param>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    private static CSelectionItem h_getItemByIdentifier(List<CSelectionTree> pSelectionTreeList, object pIdentifier)
    {
      CSelectionItem result = null;
      foreach (CSelectionTree pSelectionTree in pSelectionTreeList) {
        result = h_findItemByIdentifier(pSelectionTree, pIdentifier);
        if (result != null) {
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// Рекурсивный поиск элементов дерева по строке фильтра
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    /// <param name="sFilter">Строка-фильтр</param>
    private static bool h_findByFilter(CSelectionTree pSelectionTree, string sFilter)
    {
      bool result = pSelectionTree.SelectionItem.Name.ToUpper().Contains(sFilter) ||
        pSelectionTree.SelectionItem.Info.ToUpper().Contains(sFilter);
      pSelectionTree.SelectionItem.Selected = result;
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          if (h_findByFilter(pItem, sFilter)) {
            // Действие, если искомый элемент присутствует в ветви:
            result = true;
          }
        }
      }
      pSelectionTree.SelectionItem.Visible = result;
      return result;
    }

    /// <summary>
    /// Рекурсивный подсчет выделенных элементов
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    private static int h_findSelected(CSelectionTree pSelectionTree)
    {
      int result = 0;
      if (pSelectionTree.SelectionItem.Selected) {
        result++;
      }
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          result += h_findSelected(pItem);
        }
      }
      return result;
    }

    /// <summary>
    /// Рекурсивный подсчет выбранных элементов
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    private static int h_findChecked(CSelectionTree pSelectionTree)
    {
      int result = 0;
      if (pSelectionTree.SelectionItem.Checked) {
        result++;
      }
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          result += h_findChecked(pItem);
        }
      }
      return result;
    }

    /// <summary>
    /// Рекурсивный поиск выбранных элементов
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    /// <param name="pCheckedItemList">Список найденных элементов</param>
    private static void h_findChecked(CSelectionTree pSelectionTree, ref List<CSelectionItem> pCheckedItemList)
    {
      if (pSelectionTree.SelectionItem.Checked) {
        if (pCheckedItemList == null) {
          pCheckedItemList = new List<CSelectionItem>();
        }
        pCheckedItemList.Add(pSelectionTree.SelectionItem);
      }
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          h_findChecked(pItem, ref pCheckedItemList);
        }
      }
    }

    /// <summary>
    /// Рекурсивный поиск элемента по идентификатору в дереве
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    private static CSelectionItem h_findItemByIdentifier(CSelectionTree pSelectionTree, object pIdentifier)
    {
      CSelectionItem result = null;
      if (pSelectionTree.SelectionItem.Identifier.Equals(pIdentifier)) {
        return pSelectionTree.SelectionItem;
      }
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          result = h_findItemByIdentifier(pItem, pIdentifier);
          if (result != null) {
            break;
          }
        }
      }
      return result;
    }

    /// <summary>
    /// Рекурсивно снимает отметки выбора со всех элементов в дереве
    /// </summary>
    /// <param name="pSelectionTree">Дерево элементов</param>
    private static void h_unCheckAll(CSelectionTree pSelectionTree)
    {
      pSelectionTree.SelectionItem.Checked = false;
      if (pSelectionTree.SelectionList != null) {
        foreach (CSelectionTree pItem in pSelectionTree.SelectionList) {
          h_unCheckAll(pItem);
        }
      }
    }

  }
}