﻿#region CopyRight
//This code was originally developed 2012-03-21 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Структура дерева элементов
  /// </summary>
  public class CSelectionTree
  {
    /// <summary>
    /// Конструктор
    /// </summary>
    public CSelectionTree()
    {
      SelectionItem = new CSelectionItem();
    }

    /// <summary>
    /// Описание элемента
    /// </summary>
    public CSelectionItem SelectionItem
    {
      get;
      set;
    }
    /// <summary>
    /// Список элементов, входящих в состав этого элемента
    /// </summary>
    public List<CSelectionTree> SelectionList
    {
      get;
      set;
    }

  }
}