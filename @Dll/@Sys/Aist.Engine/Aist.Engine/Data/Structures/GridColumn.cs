﻿#region CopyRight
//This code was originally developed 2012-03-28 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание колонки объекта типа DataGridView (и подобных)
  /// </summary>
  public class CGridColumn
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CGridColumn()
    {
      ColumnType = EGridColumnType.ColumnTypeNone;
    }

    /// <summary>
    /// Тип колонки
    /// </summary>
    public object ColumnType
    {
      get;
      set;
    }
    /// <summary>
    /// Заголовок колонки
    /// </summary>
    public object HeaderText
    {
      get;
      set;
    }
    /// <summary>
    /// Название колонки
    /// </summary>
    public object ColumnName
    {
      get;
      set;
    }
    /// <summary>
    /// Название поля источника данных для колонки
    /// </summary>
    public object DataPropertyName
    {
      get;
      set;
    }
    /// <summary>
    /// Минимальная ширина
    /// </summary>
    public object MinimumWidth
    {
      get;
      set;
    }
    /// <summary>
    /// Цвет шрифта ячеек
    /// </summary>
    public object CellForeColor
    {
      get;
      set;
    }
    /// <summary>
    /// Выравнивание заголовка
    /// </summary>
    public object HeaderCellAlignment
    {
      get;
      set;
    }
    /// <summary>
    /// Перенос строки в ячейках
    /// </summary>
    public object CellWrapMode
    {
      get;
      set;
    }
    /// <summary>
    /// Видимость колонки
    /// </summary>
    public object Visible
    {
      get;
      set;
    }
    /// <summary>
    /// Класс для редактирования содержимого
    /// </summary>
    public object EditFormClass
    {
      get;
      set;
    }

  }
}
