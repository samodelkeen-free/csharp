﻿namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Тип колонки в таблице
  /// </summary>
  public enum EGridColumnType
  {
    /// <summary>
    /// По умолчанию (текстовая колонка)
    /// </summary>
    ColumnTypeNone,
    /// <summary>
    /// Текстовая колонка
    /// </summary>
    ColumnTypeText,
    /// <summary>
    /// Булева колонка
    /// </summary>
    ColumnTypeBool,
    /// <summary>
    /// Колонка даты-времени
    /// </summary>
    ColumnTypeDate,
    /// <summary>
    /// Колонка целого числа
    /// </summary>
    ColumnTypeInt,
    /// <summary>
    /// Колонка плавающего числа
    /// </summary>
    ColumnTypeFloat,
    /// <summary>
    /// Колонка выбора из списка флажков
    /// </summary>
    ColumnTypeCheck
  }
}