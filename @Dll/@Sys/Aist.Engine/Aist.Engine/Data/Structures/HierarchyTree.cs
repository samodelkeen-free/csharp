﻿#region CopyRight
//This code was originally developed 2012-05-09 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings
using System.Collections.Generic;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Структура дерева иерархии
  /// </summary>
  abstract public class CHierarchyTree
  {
    /// <summary>
    /// Описание элемента
    /// </summary>
    abstract public CTreeItem TreeItem
    {
      get;
      set;
    }
    /// <summary>
    /// Список ветвей, входящих в состав этого элемента
    /// </summary>
    abstract public List<CHierarchyTree> TreeList
    {
      get;
      set;
    }
    /// <summary>
    /// Текстовая интерпретация элемента ветви
    /// </summary>
    abstract public string TreeText
    {
      get;
    }
    /// <summary>
    /// Признак видимости ветви
    /// </summary>
    public bool Visible
    {
      get;
      set;
    }
    /// <summary>
    /// Признак выделения ветви
    /// </summary>
    public bool Selected
    {
      get;
      set;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    protected CHierarchyTree()
    {
      Visible = true;
      Selected = false;
    }

    #region Static methods
    /// <summary>
    /// Удаление ветви (по идентификатору в дереве)
    /// </summary>
    public static void Delete(CHierarchyTree pHierarchyTree, object pTreeIdentifier)
    {
      h_delete(pHierarchyTree, ref pTreeIdentifier);
    }

    /// <summary>
    /// Удаление ветви (напрямую)
    /// </summary>
    public static void Delete(CHierarchyTree pHierarchyTree, CHierarchyTree pChildTree)
    {
      pHierarchyTree.TreeList.Remove(pChildTree);
    }

    /// <summary>
    /// Удаление всех ветвей, помеченных на удаление
    /// </summary>
    public static void Delete(CHierarchyTree pHierarchyTree)
    {
      h_delete(pHierarchyTree);
    }

    /// <summary>
    /// Добавление ветви (с поиском родителя)
    /// </summary>
    public static void Add(CHierarchyTree pHierarchyTree, object pTreeIdentifier, CHierarchyTree pChildTree)
    {
      h_add(pHierarchyTree, ref pTreeIdentifier, pChildTree);
    }

    /// <summary>
    /// Добавление ветви (напрямую)
    /// </summary>
    public static void Add(CHierarchyTree pHierarchyTree, CHierarchyTree pChildTree)
    {
      if (pHierarchyTree.TreeList == null) {
        pHierarchyTree.TreeList = new List<CHierarchyTree>();
      }
      pHierarchyTree.TreeList.Add(pChildTree);
    }

    /// <summary>
    /// Пометка всех ветвей дерева как не сохраненных в БД
    /// </summary>
    public static void MarkNoSaved(CHierarchyTree pHierarchyTree)
    {
      pHierarchyTree.TreeItem.Saved = false;
      pHierarchyTree.TreeItem.Identifier = null;
      pHierarchyTree.TreeItem.ParentIdentifier = null;
      if (pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          MarkNoSaved(pItem);
        }
      }
    }

    /// <summary>
    /// Пометка всех ветвей дерева как уже удаленных в БД
    /// </summary>
    public static void MarkDeleted(CHierarchyTree pHierarchyTree)
    {
      pHierarchyTree.TreeItem.Deleted = true;
      pHierarchyTree.TreeItem.Saved = true;
      if (pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          MarkDeleted(pItem);
        }
      }
    }

    /// <summary>
    /// Проверка признака сохранения ветви
    /// </summary>
    public static bool IsSaved(CHierarchyTree pHierarchyTree)
    {
      bool result = true;
      h_findNoSaved(pHierarchyTree, ref result);
      return result;
    }

    /// <summary>
    /// Рекурсивный поиск первой не сохраненной ветви
    /// </summary>
    private static void h_findNoSaved(CHierarchyTree pHierarchyTree, ref bool bSaved)
    {
      if (!pHierarchyTree.TreeItem.Saved || !pHierarchyTree.TreeItem.GridItem.Saved) {
        bSaved = false;
        return;
      }
      if (pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          h_findNoSaved(pItem, ref bSaved);
          if (!bSaved) break;
        }
      }
    }

    /// <summary>
    /// Рекурсивный поиск и удаление ветви (по идентификатору в дереве)
    /// </summary>
    private static void h_delete(CHierarchyTree pHierarchyTree, ref object pIdentifier)
    {
      if (pIdentifier == null) {
        return;
      }
      if (pHierarchyTree.TreeItem.Identifier.Equals(pIdentifier)) {
        pHierarchyTree.TreeItem.Identifier = null;
        pIdentifier = null;
      }
      else {
        if (pHierarchyTree.TreeList != null) {
          foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
            h_delete(pItem, ref pIdentifier);
            if (pIdentifier == null) break;
          }
        }
      }
    }

    /// <summary>
    /// Рекурсивный поиск и удаление всех дочерних ветвей, помеченных на удаление
    /// </summary>
    private static void h_delete(CHierarchyTree pHierarchyTree)
    {
      if (pHierarchyTree.TreeList == null) return;
      int ii = 0;
      while (ii < pHierarchyTree.TreeList.Count) {
        if (pHierarchyTree.TreeList[ii].TreeItem.Deleted && pHierarchyTree.TreeList[ii].TreeItem.Saved) {
          pHierarchyTree.TreeList.RemoveAt(ii);
        }
        else {
          h_delete(pHierarchyTree.TreeList[ii]);
          ii++;
        }
      }
    }

    /// <summary>
    /// Рекурсивный поиск и добавление ветви
    /// </summary>
    private static void h_add(CHierarchyTree pHierarchyTree, ref object pIdentifier, CHierarchyTree pChildTree)
    {
      if (pIdentifier == null) {
        return;
      }
      if (pHierarchyTree.TreeItem.Identifier != null && pHierarchyTree.TreeItem.Identifier.Equals(pIdentifier)) {
        if (pHierarchyTree.TreeList == null) {
          pHierarchyTree.TreeList = new List<CHierarchyTree>();
        }
        pHierarchyTree.TreeList.Add(pChildTree);
        pIdentifier = null;
      }
      else {
        if (pHierarchyTree.TreeList != null) {
          foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
            h_add(pItem, ref pIdentifier, pChildTree);
            if (pIdentifier == null) break;
          }
        }
      }
    }

    /// <summary>
    /// Рекурсивный поиск первой ветви по идентификатору в дереве
    /// </summary>
    /// <param name="pHierarchyTree">Дерево элементов</param>
    /// <param name="pIdentifier">Идентификатор элемента дерева</param>
    /// <param name="bRecursive">Флаг поиска по дочерним ветвям</param>
    /// <returns>Ветвь, содержащая искомый идентификатор</returns>
    public static CHierarchyTree FindNodeByIdentifier(CHierarchyTree pHierarchyTree, object pIdentifier, bool bRecursive)
    {
      CHierarchyTree result = null;
      if (pHierarchyTree.TreeItem.Identifier != null && pHierarchyTree.TreeItem.Identifier.Equals(pIdentifier)) {
        return pHierarchyTree;
      }
      if (bRecursive && pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          result = FindNodeByIdentifier(pItem, pIdentifier, bRecursive);
          if (result != null) {
            break;
          }
        }
      }
      return result;
    }

    /// <summary>
    /// Подсчет выделенных элементов в списке деревьев
    /// </summary>
    /// <param name="pHierarchyTreeList">Список деревьев элементов</param>
    public static int SelectedCount(List<CHierarchyTree> pHierarchyTreeList)
    {
      int result = 0;
      foreach (CHierarchyTree pHierarchyTree in pHierarchyTreeList) {
        result += h_findSelected(pHierarchyTree);
      }
      return result;
    }

    /// <summary>
    /// Рекурсивный подсчет выделенных элементов в дереве
    /// </summary>
    /// <param name="pHierarchyTree">Дерево элементов</param>
    private static int h_findSelected(CHierarchyTree pHierarchyTree)
    {
      int result = 0;
      if (pHierarchyTree.Selected) {
        result++;
      }
      if (pHierarchyTree.TreeList != null) {
        foreach (CHierarchyTree pItem in pHierarchyTree.TreeList) {
          result += h_findSelected(pItem);
        }
      }
      return result;
    }

    #endregion

  }
}