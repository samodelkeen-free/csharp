﻿#region CopyRight
//This code was originally developed 2012-08-13 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System;
using System.ComponentModel;

#endregion

namespace Aist.Engine.Data.Structures
{
  #region CSelectionProperty object
  /// <summary>
  /// Описание свойства выбора из списка
  /// </summary>
  [TypeConverter(typeof(SelectionPropertyConverter))]
  public class CSelectionProperty
  {
    private object m_pValue;

    /// <summary>
    /// Creates a new instance of CSelectionProperty
    /// </summary>
    /// <param name="pIdentifier">Identifier of selection</param>
    /// <param name="pValue">Value of selection</param>
    public CSelectionProperty(object pIdentifier, object pValue)
    {
      Identifier = pIdentifier;
      Value = pValue;
    }

    /// <summary>
    /// Creates a new instance of "default" Selection
    /// </summary>
    public CSelectionProperty() : this(null, null) { }

    /// <summary>
    /// Идентификатор
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Значение
    /// </summary>
    public virtual object Value
    {
      get
      {
        return m_pValue;
      }
      set 
      { 
        m_pValue = value; 
      }
    }

  }
  #endregion

  #region SelectionPropertyConverter
  /// <summary>
  /// Конвертер типов для свойства выбора из списка
  /// </summary>
  public class SelectionPropertyConverter : TypeConverter
  {
    private static readonly string DEFAULT_FORMAT_STRING = "{0}";

    /// <summary>
    /// Overrides the ConvertTo method of TypeConverter.
    /// </summary>
    public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
    {
      if (destinationType == typeof(string)) {
        CSelectionProperty pSelection = value as CSelectionProperty;
        return string.Format(DEFAULT_FORMAT_STRING, pSelection.Value != null ? pSelection.Value : "");
      }
      return base.ConvertTo(context, culture, value, destinationType);
    }
  }
  #endregion

}
