﻿#region CopyRight
//This code was originally developed 2012-03-27 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание элемента списка объекта типа DataGridView
  /// </summary>
  abstract public class CGridItem
  {
    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    protected object m_pIdentifier;

    /// <summary>
    /// Конструктор класса
    /// </summary>
    protected CGridItem()
    {
      m_pIdentifier = null;
      Saved = true;
      Active = true;
      Selected = false;
      ErrorDescription = null;
    }

    /// <summary>
    /// Деструктор класса
    /// </summary>
    ~CGridItem()
    {
      m_pIdentifier = null;
    }

    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    abstract public object Identifier
    {
      get;
      set;
    }

    /// <summary>
    /// Признак сохранения записи в БД
    /// </summary>
    public bool Saved
    {
      get;
      set;
    }

    /// <summary>
    /// Признак действующей записи (служебное)
    /// </summary>
    virtual public bool Active
    {
      get;
      set;
    }

    /// <summary>
    /// Признак выбранной записи (служебное)
    /// </summary>
    virtual public bool Selected
    {
      get;
      set;
    }

    /// <summary>
    /// Информационное поле
    /// </summary>
    public string ErrorDescription
    {
      get;
      set;
    }

    /// <summary>
    /// Номер по порядку
    /// </summary>
    public int RowRank
    {
      get;
      set;
    }

  }
}
