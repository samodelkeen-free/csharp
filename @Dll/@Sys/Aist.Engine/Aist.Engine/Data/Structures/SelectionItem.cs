﻿#region CopyRight
//This code was originally developed 2012-03-15 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Описание элемента в списке формы выбора элементов
  /// </summary>
  public class CSelectionItem
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CSelectionItem()
    {
      Visible = false;
      Checked = false;
      Selected = false;
      Object = null;
    }

    /// <summary>
    /// Конструктор для копирования экземпляра класса
    /// </summary>
    public CSelectionItem(CSelectionItem other)
    {
      this.Identifier = other.Identifier;
      this.Name = other.Name;
      this.Info = other.Info;
      this.Checked = other.Checked;
      this.Visible = other.Visible;
      this.Selected = other.Selected;
      this.Object = null;
    }

    /// <summary>
    /// Деструктор класса
    /// </summary>
    ~CSelectionItem()
    {
      Object = null;
    }

    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование элемента (первое поле)
    /// </summary>
    public string Name
    {
      get;
      set;
    }
    /// <summary>
    /// Дополнительная информация (второе поле)
    /// </summary>
    public string Info
    {
      get;
      set;
    }
    /// <summary>
    /// Признак выбора элемента
    /// </summary>
    public bool Checked
    {
      get;
      set;
    }
    /// <summary>
    /// Признак видимости элемента
    /// </summary>
    public bool Visible
    {
      get;
      set;
    }
    /// <summary>
    /// Признак выделения элемента
    /// </summary>
    public bool Selected
    {
      get;
      set;
    }

    /// <summary>
    /// Прикрепленный объект
    /// </summary>
    public object Object
    {
      get;
      set;
    }

  }
}
