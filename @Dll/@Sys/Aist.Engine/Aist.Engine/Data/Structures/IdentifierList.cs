﻿#region CopyRight
//This code was originally developed 2012-04-09 by Egorov V.V.
//(c)2012 Аналитические информационные системы и технологии
#endregion

#region Usings

using System.Collections.Generic;

#endregion

namespace Aist.Engine.Data.Structures
{
  /// <summary>
  /// Список идентификаторов для временного хранения
  /// </summary>
  public class CIdentifierList : List<object>
  {
    /// <summary>
    /// Поиск идентификатора в списке с последующим удалением
    /// </summary>
    /// <param></param>
    public bool IsFindAndRemoveIdentifier(object pIdentifier)
    {
      object pFound = Find(delegate(object pItem) { return pItem.Equals(pIdentifier); });
      if (pFound != null) {
        Remove(pFound);
        return true;
      }
      else {
        return false;
      }
    }

    /// <summary>
    /// Поиск записи типа CIdentifierRankItem по полю идентификатора
    /// </summary>
    /// <param>Индекс найденной записи, либо -1</param>
    public int FindIdentifierRankItem(object pIdentifier)
    {
      return FindIndex(delegate(object pItem) { return ((CIdentifierRankItem)pItem).Identifier.Equals(pIdentifier); });
    }

  }

  /// <summary>
  /// Структура для хранения идентификатора записи в БД и порядкового номера записи в выборке
  /// </summary>
  public struct CIdentifierRankItem
  {
    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Порядковый номер записи в выборке
    /// </summary>
    public int Rank
    {
      get;
      set;
    }

  }

}
