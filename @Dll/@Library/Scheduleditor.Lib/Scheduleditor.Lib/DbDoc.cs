﻿#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using
using System;
using System.Collections.Generic;
using Aist.Engine.Data.Db;
using Aist.Engine.Data.Structures;
using Aist.Engine.Windows.Controls;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Storages;
using Sfo.Sys.Config;
using Sfo.Sys.Log;

#endregion

namespace Scheduleditor.Lib
{
  /// <summary>
  /// Класс для работы с БД
  /// </summary>
  public class CDbDoc : CDbCommonDoc
  {
    #region Private variables
    /*
    /// <summary>
    /// Объект строки соединения с БД (EVV: Здесь пока это не нужно)
    /// </summary>
    protected CConnectionStringItem m_pConnectionStringItem;
    */

    /// <summary>
    /// Объект взаимодействия с Хранилищем
    /// </summary>
    private IDataStorage m_pDataStorage;

    /// <summary>
    /// Объект описания редактируемой задачи Cron
    /// </summary>
    private CCronTask m_pCronTask;

    #endregion

    #region Constructors

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    public CDbDoc(CAppConfig pAppConfig) : base(pAppConfig)
    {
      //m_pConnectionStringItem = new CConnectionStringItem();
      //m_pConnectionStringItem.ConnectionString = m_pAppConfig.AppParam.ConnectionString;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="pLogWriter">Объект протоколирования</param>
    public CDbDoc(CAppConfig pAppConfig, ILogWriter pLogWriter) : base(pAppConfig, pLogWriter)
    {
      //m_pConnectionStringItem = new CConnectionStringItem();
      //m_pConnectionStringItem.ConnectionString = m_pAppConfig.AppParam.ConnectionString;
      // TODO: Брать тип хранилища из конфигурации
      m_pDataStorage = CTextDataStorage.GetStorage();
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="pLogWriter">Объект протоколирования</param>
    /// <param name="pUser">Идентификатор пользователя</param>
    public CDbDoc(CAppConfig pAppConfig, ILogWriter pLogWriter, Guid pUser) : base(pAppConfig, pLogWriter, pUser)
    {
      //m_pUser = pUser;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="pAppConfig">Объект конфигурации</param>
    /// <param name="sConnectionString">Строка соединения</param>
    public CDbDoc(CAppConfig pAppConfig, string sConnectionString) : base(pAppConfig, sConnectionString)
    {
      //m_pConnectionStringItem = new CConnectionStringItem();
      //m_pConnectionStringItem.ConnectionString = sConnectionString;
    }

    #endregion

    #region Public methods

    /// <summary>
    /// Заполняет объект класса CSelectionFormCheckBox (описания списка элементов-флажков для формы выбора) содержимым Хранилища
    /// </summary>
    /// <param name="pSelectionCheckBox">Описание списка элементов-флажков</param>
    /// <returns></returns>
    override public void SelectionCheckBoxFill(CSelectionFormCheckBox pSelectionCheckBox)
    {
      pSelectionCheckBox.RemoveNoChecked();
      switch (pSelectionCheckBox.SourceIdentifier) {
        case CScheduleditorLibConst.SourceMinute:
          for (int ii = 0; ii < 60; ++ii) {
            CCBoxItem pItem = new CCBoxItem(ii.ToString(), ii);
            pItem.Identifier = ii;
            h_placeItem(pItem, pSelectionCheckBox);
          }
          break;
        case CScheduleditorLibConst.SourceHour:
          for (int ii = 0; ii < 24; ++ii) {
            CCBoxItem pItem = new CCBoxItem(ii.ToString(), ii);
            pItem.Identifier = ii;
            h_placeItem(pItem, pSelectionCheckBox);
          }
          break;
        case CScheduleditorLibConst.SourceMonthDay:
          for (int ii = 0; ii < 31; ++ii) {
            CCBoxItem pItem = new CCBoxItem((ii+1).ToString(), ii);
            pItem.Identifier = ii;
            h_placeItem(pItem, pSelectionCheckBox);
          }
          break;
        case CScheduleditorLibConst.SourceMonth:
          for (int ii = 0; ii < CScheduleditorLibConst.MonthArray.Length; ++ii) {
            CCBoxItem pItem = new CCBoxItem(CScheduleditorLibConst.MonthArray[ii], ii);
            pItem.Identifier = ii;
            h_placeItem(pItem, pSelectionCheckBox);
          }
          break;
        case CScheduleditorLibConst.SourceWeek:
          for (int ii = 0; ii < CScheduleditorLibConst.WeekArray.Length; ++ii) {
            CCBoxItem pItem = new CCBoxItem(CScheduleditorLibConst.WeekArray[ii], ii);
            pItem.Identifier = ii;
            h_placeItem(pItem, pSelectionCheckBox);
          }
          break;
        default:
          // EVV: Выбор того же самого, но из БД SQL-запросом
          break;
      }
    }

    /// <summary>
    /// Возвращает объект задачи Cron, считанной из Хранилища по ее идентификатору
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Сообщение о результате обращения к Хранилищу</param>
    /// <returns>Объект с информацией о задаче в формате Cron</returns>
    public CCronTask GetTask(object pIdentifier, out string sMessage)
    {
      m_pCronTask = m_pDataStorage.ReadTask(pIdentifier, out sMessage);
      return m_pCronTask;
    }

    /// <summary>
    /// Создает объект задачи Cron
    /// </summary>
    /// <returns>Объект с информацией о задаче в формате Cron</returns>
    public CCronTask NewTask()
    {
      m_pCronTask = new CCronTask();
      return m_pCronTask;
    }

    /// <summary>
    /// Записывает текущий объект задачи Cron в Хранилище
    /// </summary>
    /// <param name="sMessage">Сообщение о результатах сохранения</param>
    /// <returns>Результат записи в Хранилище (true/false)</returns>
    public bool SaveTask(out string sMessage)
    {
      return m_pDataStorage.WriteTask(m_pCronTask, out sMessage);
    }

    /// <summary>
    /// Удаляет задачу Cron из Хранилища
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Сообщение о результатах операции удаления</param>
    /// <returns>Результат операции в Хранилище (true/false)</returns>
    public bool DeleteTask(object pIdentifier, out string sMessage)
    {
      return m_pDataStorage.DeleteTask(pIdentifier, out sMessage);
    }

    /// <summary>
    /// Ищет задачи Cron в Хранилище по условию поиска
    /// </summary>
    /// <param name="pFoundTaskList">Объект списка задач</param>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="iStartRow">Номер первой записи в выборке</param>
    /// <param name="iNumber">Максимальное количество возвращаемых записей</param>
    /// <param name="sMessage">Сообщение о результатах операции поиска в Хранилище</param>
    /// <returns></returns>
    public void SearchTask(List<CFoundTaskItem> pFoundTaskList, CTaskFilter pTaskFilter, int iStartRow, int iNumber, out string sMessage)
    {
      m_pDataStorage.SearchTask(pFoundTaskList, pTaskFilter, out sMessage);
    }

    /// <summary>
    /// Получает количество задач Cron в Хранилище по условию поиска
    /// </summary>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="sMessage">Сообщение о результатах операции подсчета в Хранилище</param>
    /// <returns>Количество задач в Хранилище, удовлетворяющих условию поиска</returns>
    public int SearchTaskCount(CTaskFilter pTaskFilter, out string sMessage)
    {
      return m_pDataStorage.SearchTaskCount(pTaskFilter, out sMessage);
    }

    /// <summary>
    /// Получает строку SQL-условия для строки выборки элементов из Хранилища (актуально для SQL-хранилища)
    /// </summary>
    /// <param name="sFilter"></param>
    /// <returns>Строка условия для выборки</returns>
    public string GetIntervalFilter(string sFilter)
    {
      return sFilter;
    }

    /// <summary>
    /// Получает строку SQL-условия для строки поиска элементов в Хранилище (актуально для SQL-хранилища)
    /// </summary>
    /// <param name="sSearch">Строка для поиска</param>
    /// <returns>Строка условия для поиска</returns>
    public string GetIntervalSearch(string sSearch)
    {
      return sSearch;
    }

    /// <summary>
    /// Получает количество интервалов в редактируемом поле задачи Cron
    /// </summary>
    /// <param name="sSqlFilter">Строка условия для выборки</param>
    /// <returns>Количество элементов, удовлетворяющих условию выборки</returns>
    public int GetIntervalCount(string sSqlFilter)
    {
      return 0;
    }

    /// <summary>
    /// Заполняет список объектов грида для редактирования интервалов поля задачи Cron
    /// </summary>
    /// <param name="pGridDescription">Описание объекта типа DataGridView</param>
    /// <param name="sSqlFilter">Строка условия для выборки</param>
    /// <param name="iStartRow">Номер первой записи в выборке</param>
    /// <param name="iNumber">Максимальное количество возвращаемых записей</param>
    /// <returns></returns>
    public void IntervalListFill(CIntervalGridDescription pGridDescription, string sSqlFilter, int iStartRow, int iNumber)
    {
      pGridDescription.Clear();
      int rr = 0;
      if (m_pCronTask.Period[pGridDescription.Field] != null)
        foreach (CCronFieldInterval pCronFieldInterval in m_pCronTask.Period[pGridDescription.Field]) {
          if (pCronFieldInterval.End != null) { // EVV: Забираем только интервалы.
            CIntervalGridItem pGridItem = new CIntervalGridItem();
            pGridItem.Begin.Identifier = pCronFieldInterval.Begin;
            pGridItem.End.Identifier = pCronFieldInterval.End;
            switch (pGridDescription.Field) {
              case 2:
                pGridItem.Begin.Value = pCronFieldInterval.Begin + 1;
                pGridItem.End.Value = pCronFieldInterval.End + 1;
                break;
              case 3:
                pGridItem.Begin.Value = CScheduleditorLibConst.MonthArray[(int) pCronFieldInterval.Begin];
                pGridItem.End.Value = CScheduleditorLibConst.MonthArray[(int) pCronFieldInterval.End];
                break;
              case 4:
                pGridItem.Begin.Value = CScheduleditorLibConst.WeekArray[(int) pCronFieldInterval.Begin];
                pGridItem.End.Value = CScheduleditorLibConst.WeekArray[(int) pCronFieldInterval.End];
                break;
              default:
                pGridItem.Begin.Value = pCronFieldInterval.Begin;
                pGridItem.End.Value = pCronFieldInterval.End;
                break;
            }
            pGridItem.Step = pCronFieldInterval.Step;
            pGridItem.Identifier = pCronFieldInterval.Identifier;
            pGridItem.RowRank = ++rr;

            pGridDescription.Add(pGridItem);
          }
        }
    }

    /// <summary>
    /// Заполняет список идентификаторов интервалов поля задачи Cron (актуально для SQL-хранилища)
    /// </summary>
    /// <param name="pIdentifierList">Список идентификаторов</param>
    /// <param name="sSqlFilter">Строка условия для выборки</param>
    /// <param name="sSqlSearch">Строка условия для поиска</param>
    /// <returns></returns>
    public void IntervalIdentifierListFill(CIdentifierList pIdentifierList, string sSqlFilter, string sSqlSearch)
    {
      pIdentifierList.Clear();
      IdentifierListFill(pIdentifierList, string.Format("", sSqlFilter, sSqlSearch));
    }

    /// <summary>
    /// Добавляет/редактирует временной интервал
    /// </summary>
    /// <param name="pGridItem">Объект строки грида</param>
    /// <param name="iField">Поле для добавления/редактирования интервала</param>
    public void EditInterval(CIntervalGridItem pGridItem, int iField)
    {
      try {
        object pNewIdentifier = null;
        if (pGridItem.Identifier == null) {
          //pNewIdentifier = (m_pCronTask.Period[iField] != null ? m_pCronTask.Period[iField].Count : 0); EVV: Используем GUID
          pNewIdentifier = Guid.NewGuid();
          CCronFieldInterval pCronFieldInterval = new CCronFieldInterval();
          pCronFieldInterval.Begin = (int) pGridItem.Begin.Identifier;
          pCronFieldInterval.End = (int) pGridItem.End.Identifier;
          pCronFieldInterval.Step = pGridItem.Step;
          pCronFieldInterval.Identifier = pNewIdentifier;
          m_pCronTask.Period[iField].Add(pCronFieldInterval);
        }
        else {
          pNewIdentifier = pGridItem.Identifier;
          CCronFieldInterval pCronFieldInterval = m_pCronTask.Period[iField].Find(delegate(CCronFieldInterval pInterval) { return pNewIdentifier.Equals(pInterval.Identifier); });
          pCronFieldInterval.Begin = (int) pGridItem.Begin.Identifier;
          pCronFieldInterval.End = (int) pGridItem.End.Identifier;
          pCronFieldInterval.Step = pGridItem.Step;
        }
        if (pNewIdentifier != DBNull.Value && pNewIdentifier != null) { // EVV: Оставим на случай работы с SQL-хранилищем.
          if (pGridItem.Identifier == null) {
            pGridItem.Identifier = pNewIdentifier;
            pGridItem.ErrorDescription = "Добавлен интервал";
          }
          else {
            pGridItem.ErrorDescription = "Изменен интервал";
          }
          pGridItem.Saved = true;
        }
        else {
          pGridItem.Saved = false;
        }
      }
      catch (Exception ex) {
        pGridItem.ErrorDescription = ex.Message;
        pGridItem.Saved = false;
      }
    }

    /// <summary>
    /// Удаляет временной интервал
    /// </summary>
    /// <param name="pIdentifier">Идентификатор интервала</param>
    /// <param name="iField">Поле для добавления/редактирования интервала</param>
    /// <param name="sErrorMessage">Сообщение Хранилища</param>
    public bool DeleteInterval(object pIdentifier, int iField, out string sErrorMessage)
    {
      sErrorMessage = String.Empty;
      CCronFieldInterval pCronFieldInterval = m_pCronTask.Period[iField].Find(delegate(CCronFieldInterval pInterval) { return pIdentifier.Equals(pInterval.Identifier); });
      m_pCronTask.Period[iField].Remove(pCronFieldInterval);
      return true;
    }

    #endregion

    #region Private methods

    /// <summary>
    /// Заносит элемент в список выбора, если такового там нет, либо обновляет существующий элемент
    /// </summary>
    /// <param name="pItem">Размещаемый элемент</param>
    /// <param name="pSelectionCheckBox">Список для размещения</param>
    private static void h_placeItem(CCBoxItem pItem, CSelectionFormCheckBox pSelectionCheckBox)
    {
      CCBoxItem pItemOld = pSelectionCheckBox.FindIdentifier(pItem.Identifier);
      if (pItemOld != null) {
        pItemOld.Text = pItem.Text;
        pItemOld.Value = pItem.Value;
      } else {
        pSelectionCheckBox.Add(pItem);
      }
    }

    #endregion

    #region Public properties
    /* EVV: Здесь это пока не нужно.
    /// <summary>
    /// Получает объект строки соединения с БД
    /// </summary>
    public CConnectionStringItem ConnectionStringItem
    {
      get
      {
        return m_pConnectionStringItem;
      }
    }
    */

    /// <summary>
    /// Получает/устанавливает объект текущей редактируемой задачи
    /// </summary>
    public CCronTask CronTask
    {
      get
      {
        return m_pCronTask;
      }
      set
      {
        m_pCronTask = value;
      }
    }

    #endregion

    #region CDoc methods

    /// <summary>
    /// Открывает документ.
    /// </summary>
    /// <param name="pUserData"></param>
    protected override void ObjOpen(object pUserData)
    {
      
    }

    /// <summary>
    /// Закрывает документ.
    /// </summary>
    /// <param name="bDisposing"></param>
    protected override void ObjClose(bool bDisposing)
    {

    }

    #endregion
  }

}
