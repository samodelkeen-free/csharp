#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 �� �����, ������ � ��������� ����������� ������
#endregion

#region Using
using System;
using System.ComponentModel;
using System.Drawing.Design;
using Sfo.Sys;
using Sfo.Sys.Config;
using Sfo.Sys.Props;
using Sfo.Sys.Props.Editor;

#endregion

namespace Scheduleditor.Lib.Config
{
  #region CScheduleditorParam class
  /// <summary>
  /// ����� ���������� ��������� ����������
  /// </summary>
  public class CScheduleditorParam : CAppParam
  {
    #region Private variables
    /// <summary>
    /// ������ ���������� � �� ��������� (EVV: ���� ��� �� ����� �����)
    /// </summary>
    //private string m_sConnectionString; 
    /// <summary>
    /// ������������ ���������� ������� �� �������� ������� �� ��
    /// </summary>
    private int m_iMaxPageSize = 100;
    /// <summary>
    /// ���������� ��� �������� ����-���� �� ���������
    /// </summary>
    private string m_sDirectoryDefault = String.Empty;
    /// <summary>
    /// ���������� ���� �� �������������� ������ � ��
    /// </summary>
    private int m_iLockPeriod;

    #endregion

    #region Constructors
    /// <summary>
    /// ����������� �������
    /// </summary>
    /// <param name="pAppConfig"></param>
    public CScheduleditorParam(CAppConfig pAppConfig) :
      base(pAppConfig)
    {
    }

    #endregion

    #region Virtual overrides
    /// <summary>
    ///
    /// </summary>
    protected override void ObjInitAppParam()
    {
      base.ObjInitAppParam();
    }

    /// <summary>
    /// 
    /// </summary>
    protected override void ObjCheckAppParam()
    {
      base.ObjCheckAppParam();
      try {
        CValueCheckTool.CheckDirectoryExists(m_sDirectoryDefault, "DirectoryDefault");
      }
      catch {
        m_sDirectoryDefault = AppConfig.DataPath;
      }
    }

    #endregion

    #region Public parameters
    /* EVV: ���� ��� �� ����� �����.
    /// <summary>
    /// ��������/������ ������ ���������� � ����� ������ ���������
    /// </summary>
    [Category(CScheduleditorConst.PARAM_SECTION_DIR)]
    [Browsable(true)]
    [DisplayName("������ ���������� � �� ���������")]
    [Editor(typeof(CConnectionStringTypeEditor), typeof(UITypeEditor))]
    [Description("������ ���������� � ����� ������ ���������")]
    [CProperty("������ ���������� � ��",
      DefaultValue = "",
      IsRequired = true,
      OldNameList = new string[] { "ConnectionString" })]
    //[StringValidator(InvalidCharacters = CConst.InvalidFileNameChars, MinLength = 4, MaxLength = 30)]
    public string ConnectionString
    {
      get
      {
        return m_sConnectionString;
      }
      set
      {
        m_sConnectionString = value;
      }
    }
    */
    /// <summary>
    /// ��������/������ ������������ ���������� ������� �� �������� ������� �� ��
    /// </summary>
    [Category(CScheduleditorConst.PARAM_SECTION_GENERAL)]
    [Browsable(true)]
    [Description("������������ ���������� ������� �� �������� ������� �� ��")]
    [CProperty("������������ ���������� ������� �� ��������",
      DefaultValue = 100,
      IsRequired = true,
      OldNameList = new string[] { "MaxPageSize" })]
    public int MaxPageSize
    {
      get
      {
        return m_iMaxPageSize;
      }
      set
      {
        m_iMaxPageSize = value;
      }
    }

    /// <summary>
    /// ��������/������ ���������� ��� �������� ����-���� �� ���������
    /// </summary>
    [Category(CScheduleditorConst.PARAM_SECTION_DIR)]
    [Browsable(true)]
    [Description("��������/������ ���������� ��� �������� ����-���� �� ���������")]
    [Editor(typeof(CDirectorySelectTypeEditor), typeof(UITypeEditor))]
    [CProperty("���������� ��� �������� �������� ���������� �� ���������",
      DefaultValue = "",
      IsRequired = true)]
    public string DirectoryTemplate
    {
      get
      {
        return m_sDirectoryDefault;
      }
      set
      {
        m_sDirectoryDefault = value;
      }
    }

    /// <summary>
    /// ��������/������ ���������� ���� �� �������������� ������ � ��
    /// </summary>
    [Category(CScheduleditorConst.PARAM_SECTION_GENERAL)]
    [Browsable(true)]
    [Description("���������� ���� �� �������������� ������ � ���� ������ (0 - ������� ���������)")]
    [CProperty("���������� ���� �� �������������� ������ � ��",
      DefaultValue = 0,
      IsRequired = true,
      OldNameList = new string[] { "LockPeriod" })]
    public int LockPeriod
    {
      get
      {
        return m_iLockPeriod;
      }
      set
      {
        m_iLockPeriod = value;
      }
    }

    #endregion

    #region Public methods

    #endregion

    #region Help methods

    #endregion

  }

  #endregion
}
