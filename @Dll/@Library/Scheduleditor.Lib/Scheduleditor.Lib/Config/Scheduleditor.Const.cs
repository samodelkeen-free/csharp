#region CopyRight
//This code was originally developed 2012-02-28 by Egorov V.V.
//(c) 2016 �� �����, ������ � ��������� ����������� ������
#endregion

#region Using
#endregion

namespace Scheduleditor.Lib.Config
{
  #region CScheduleditorConst class
  /// <summary>
  /// ����� �������� ��������� ����������
  /// </summary>
  public class CScheduleditorConst
  {
    #region Private constansts
    /// <summary>
    /// ������������ ������ ��������
    /// </summary>
    internal const string PARAM_SECTION_GENERAL = "1. ��������� ����������";
    /// <summary>
    /// ������������ ������ �������� ���������
    /// </summary>
    internal const string PARAM_SECTION_DIR = "2. ��������� �� � ���������";
    /// <summary>
    /// ������������ ������ �������� �������
    /// </summary>
    internal const string PARAM_SECTION_ACCESS = "3. ��������� �������";
    
    /// <summary>
    /// ��� ��������� ��� �������� ������ ���������� � ��
    /// </summary>
    internal const string PARAM_CONNECTION_STRING = "ConnectionString";

    /// <summary>
    /// MapFileName = "Scheduleditor"
    /// </summary>
    internal const string MapFileName = "Scheduleditor";

    /// <summary>
    /// ServiceExeFileName ="ScheduleditorService.exe"
    /// </summary>
    internal const string ServiceExeFileName = "ScheduleditorService.exe";
    /// <summary>
    /// ServiceName ="GNIVC Scheduleditor"
    /// </summary>
    internal const string ServiceName = "GNIVC Scheduleditor";
    /// <summary>
    /// ServiceDisplayName ="�������� ������������ ����� (������)"
    /// </summary>
    internal const string ServiceDisplayName = "�������� ������������ ����� (������)";
    /// <summary>
    /// ServiceDescription ="�������� ������������ �����";
    /// </summary>
    internal const string ServiceDescription = "�������� ������������ �����";
    /// <summary>
    /// DisplayName = "�������� ������������ �����"
    /// </summary>
    internal const string DisplayName = "�������� ������������ �����";

    #endregion

    #region Constructors
    #endregion

    #region Private methods
    #endregion

    #region Protected methods
    #endregion

    #region Public methods
    #endregion

  };

  #endregion
}
