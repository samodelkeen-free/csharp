#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 �� �����, ������ � ��������� ����������� ������
#endregion

#region Using

using Sfo.Sys.Config;
using Sfo.Sys.Service;

#endregion

namespace Scheduleditor.Lib.Config
{
  #region CScheduleditorConfig class
  ///<summary>
  /// ����� ������������ ����������
	///</summary>
  public class CScheduleditorConfig : CAppConfig
  {		
		#region Variable declarations
    /// <summary>
    /// �������� �������
    /// </summary>
    private CServiceItem m_pServiceItem;

		#endregion

		#region Constructors
    /// <summary>
    /// ����������� �� �������� ����� � ����� ����������
    /// </summary>
    /// <param name="pMainForm"></param>
    /// <param name="sInstanceName"></param>
    public CScheduleditorConfig(object pMainForm, string sInstanceName)
      : base(pMainForm, CScheduleditorConst.MapFileName, sInstanceName)
    {
      h_CreateServiceItem(sInstanceName);
    }

		#endregion

    #region CAppConfig methods
    /// <summary>
    /// �������� ������ ����������
    /// </summary>
    /// <returns></returns>
    protected override CAppParam ObjGetAppParam()
    {
      return new CScheduleditorParam(this);
    }

    /// <summary>
    /// �������� ������������ ��� ����������
    /// </summary>
    /// <returns></returns>
    protected override string ObjGetDisplayName()
    {
      string result = CScheduleditorConst.DisplayName;
      if (string.IsNullOrEmpty(this.InstanceName)) {
        return result;
      }
      return string.Format("{0}: {1}", this.InstanceName, result);
    }

    #endregion

		#region Private methods
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sInstanceName"></param>
    private void h_CreateServiceItem(string sInstanceName)
    {
      /*
      string sExeName = RootPath + CScheduleditorConst.ServiceExeFileName;
      m_pServiceItem = new CServiceItem(CScheduleditorConst.ServiceName,
        CScheduleditorConst.ServiceDisplayName, sExeName, sInstanceName, 
        CScheduleditorConst.ServiceDescription);
      */
    }

		#endregion

		#region Protected methods
    #endregion

		#region Public methods
    /// <summary>
    /// ���������, �c�������� �� ������?
    /// </summary>
    /// <returns></returns>
    public bool IsServiceInstalled()
    {
      return CServiceTool.IsServiceInstalled(m_pServiceItem.ServiceName);
    }

    /// <summary>
    /// ������������� ������
    /// </summary>
    public void InstallService()
    {
      CServiceTool.InstallService(m_pServiceItem.ServiceName, m_pServiceItem.ServiceExeName);
    }

    /// <summary>
    /// �������������� ������
    /// </summary>
    public void UninstallService()
    {
      CServiceTool.UnInstallService(m_pServiceItem.ServiceName, m_pServiceItem.ServiceExeName);
    }

		#endregion

    #region Public properties
    /// <summary>
    /// ������ � ������� � �����������
    /// </summary>
    new public CScheduleditorParam AppParam
    {
      get
      {
        return (CScheduleditorParam)base.AppParam;
      }
    }
    /// <summary>
    /// �������� �������� �������
    /// </summary>
    public CServiceItem ServiceItem
    {
      get
      {
        return m_pServiceItem;
      }
    }

    #endregion
  }

  #endregion
}
