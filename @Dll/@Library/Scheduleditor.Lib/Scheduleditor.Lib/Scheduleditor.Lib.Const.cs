﻿#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

#endregion

namespace Scheduleditor.Lib
{
  #region CScheduleditorLibConst public static class
  /// <summary>
  /// Общие константы приложения
  /// </summary>
  public static class CScheduleditorLibConst
  {
    #region Идентификаторы источников данных

    /// <summary>
    /// Источник данных: минуты
    /// </summary>
    public const int SourceMinute = 0;
    /// <summary>
    /// Источник данных: часы
    /// </summary>
    public const int SourceHour = 1;
    /// <summary>
    /// Источник данных: дни месяца
    /// </summary>
    public const int SourceMonthDay = 2;
    /// <summary>
    /// Источник данных: месяцы
    /// </summary>
    public const int SourceMonth = 3;
    /// <summary>
    /// Источник данных: дни недели
    /// </summary>
    public const int SourceWeek = 4;

    #endregion

    #region Массивы описания интервалов

    /// <summary>
    /// Месяцы
    /// </summary>
    public static readonly string[] MonthArray = { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };

    /// <summary>
    /// Дни недели
    /// </summary>
    public static readonly string[] WeekArray = { "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье" };

    #endregion

    #region Шаблоны строк

    public const string TEMPLATE_CRON_TASK = "{0} {1} {2} {3}";

    #endregion

  }
  
  #endregion
}
