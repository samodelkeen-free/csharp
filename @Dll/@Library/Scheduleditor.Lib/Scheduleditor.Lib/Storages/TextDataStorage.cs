﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Scheduleditor.Lib.Classes;
using Sfo.Sys;

#endregion

namespace Scheduleditor.Lib.Storages
{

  #region CTextDataStorage class
  
  /// <summary>
  /// Предоставляет класс для хранения задач планировщика Cron в текстовом файле
  /// </summary>
  internal class CTextDataStorage : IDataStorage
  {
    /// <summary>
    /// Файл планировщика Cron
    /// </summary>
    private readonly string m_sFileName;
    /// <summary>
    /// Объект для блокировки
    /// </summary>
    private readonly Object m_pLockObject;

    private CTextDataStorage()
    {
      // TODO: Брать расположение файла задач Cron из конфигурации
      // EVV: Пока создаем/используем файл в папке с приложением
      m_sFileName = Path.GetDirectoryName(Application.ExecutablePath) + Path.DirectorySeparatorChar + "cron.txt";
      m_pLockObject = new Object();
    }

    private static CTextDataStorage m_pInstance;

    /// <summary>
    /// Метод получения экземпляра класса
    /// </summary>
    /// <returns>Ссылка на экземпляр класса CTextDataStorage</returns>
    public static CTextDataStorage GetStorage()
    {
      if (m_pInstance == null)
        m_pInstance = new CTextDataStorage();
      return m_pInstance;
    } 

    /// <summary>
    /// Создает/редактирует запись о задаче Cron в файле
    /// </summary>
    /// <param name="pCronTask">Объект описания задачи</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Результат работы (true/false)</returns>
    public bool WriteTask(CCronTask pCronTask, out string sMessage)
    {
      bool result = h_writeRecord(pCronTask.ToString(), out sMessage);
      return result;
    }

    /// <summary>
    /// Удаляет запись о задаче Cron из файла
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Результат работы (true/false)</returns>
    public bool DeleteTask(object pIdentifier, out string sMessage)
    {
      bool result = h_deleteRecord(pIdentifier.ToString(), out sMessage);
      return result;
    }

    /// <summary>
    /// Читает запись о задаче Cron из Хранилища
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Объект задачи Cron</returns>
    public CCronTask ReadTask(object pIdentifier, out string sMessage)
    {
      string sCronTask = h_readRecord(pIdentifier.ToString(), out sMessage);
      if (!String.IsNullOrEmpty(sCronTask)) {
        return new CCronTask(sCronTask);
      }
      return null;
    }

    /// <summary>
    /// Возвращает список задач Cron из Хранилища-текстового файла, соответствующих условию фильтрации
    /// </summary>
    /// <param name="pFoundTaskList">Объект списка задач</param>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns></returns>
    public void SearchTask(List<CFoundTaskItem> pFoundTaskList, CTaskFilter pTaskFilter, out string sMessage)
    {
      pFoundTaskList.Clear();
      List<string> pFoundRecordList = h_searchRecord(pTaskFilter, out sMessage);
      int ii = 0;
      foreach (string sRecord in pFoundRecordList) {
        CFoundTaskItem pFoundTaskItem = new CFoundTaskItem(sRecord);
        pFoundTaskItem.RowRank = ++ii;
        pFoundTaskList.Add(pFoundTaskItem);
      }
    }

    /// <summary>
    /// Возвращает количество задач Cron в Хранилище-текстовом файле, соответствующих условию фильтрации
    /// </summary>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Количество задач Cron</returns>
    public int SearchTaskCount(CTaskFilter pTaskFilter, out string sMessage)
    {
      return h_searchRecordCount(pTaskFilter, out sMessage);
    }

    #region Private methods

    /// <summary>
    /// Сохраняет запись о задаче в файле планировщика
    /// </summary>
    /// <param name="sRecord">Строка задачи</param>
    /// <param name="sMessage">Сообщение в случае ошибки чтения или сохранения</param>
    /// <returns>Флаг успешности операции (true/false)</returns>
    private bool h_writeRecord(string sRecord, out string sMessage)
    {  
      sMessage = "";

      if (String.IsNullOrEmpty(sRecord)) {
        return true;
      }

      bool result = false;
      bool bReplaced = false;
      string sIdentifier = sRecord.Split(' ')[6];

      try {
        lock (m_pLockObject) {
          //string sTempFile = Path.GetTempFileName();
          string sTempFile = Path.GetDirectoryName(Application.ExecutablePath) + Path.DirectorySeparatorChar + Path.GetRandomFileName();
          using (StreamReader pStreamReader = File.Exists(m_sFileName) ? new StreamReader(m_sFileName, Encoding.Default) : StreamReader.Null)
          using (StreamWriter pStreamWriter = CFileTool.TryOpenFile(sTempFile, FileAccess.Write, Encoding.Default, 3000))
          {
            string sLine;
            while ((sLine = pStreamReader.ReadLine()) != null) {
              if (sLine.IndexOf(sIdentifier) == -1) { // TODO: Переделать с использованием sLine.Split(' ')[6]
                pStreamWriter.WriteLine(sLine);
              }
              else {
                pStreamWriter.WriteLine(sRecord);
                bReplaced = true;
              }
            }

            if (!bReplaced) {
              //pStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
              pStreamWriter.WriteLine(sRecord);
            }

            pStreamReader.Close();
            pStreamWriter.Flush();
            pStreamWriter.Close();
            result = true;
          }

          //File.Delete(m_sFileName);
          if (!File.Exists(m_sFileName)) File.Move(sTempFile, m_sFileName);
          else File.Replace(sTempFile, m_sFileName, null, false);
        }
      }
      catch (Exception ex) {
        CTool.ReportException(ex);
        sMessage = ex.Message;
      }

      return result;
    }

    /// <summary>
    /// Удаляет запись о задаче из файла планировщика
    /// </summary>
    /// <param name="sIdentifier">Идентификатор удаляемой задачи</param>
    /// <param name="sMessage">Сообщение в случае ошибки чтения или сохранения</param>
    /// <returns>Флаг успешности операции (true/false)</returns>
    private bool h_deleteRecord(string sIdentifier, out string sMessage)
    {
      sMessage = "";

      if (String.IsNullOrEmpty(sIdentifier.Trim())) {
        sMessage = "Пустой идентификатор задачи.";
        return false;
      }

      bool result = false;
      bool bDeleted = false;

      try {
        lock (m_pLockObject) {
          //string sTempFile = Path.GetTempFileName();
          string sTempFile = Path.GetDirectoryName(Application.ExecutablePath) + Path.DirectorySeparatorChar + Path.GetRandomFileName();
          using (StreamReader pStreamReader = File.Exists(m_sFileName) ? new StreamReader(m_sFileName, Encoding.Default) : StreamReader.Null)
          using (StreamWriter pStreamWriter = CFileTool.TryOpenFile(sTempFile, FileAccess.Write, Encoding.Default, 3000))
          {
            string sLine;
            while ((sLine = pStreamReader.ReadLine()) != null) {
              if (sLine.IndexOf(sIdentifier) == -1) { // TODO: Переделать с использованием sLine.Split(' ')[6]
                pStreamWriter.WriteLine(sLine);
              }
              else {
                bDeleted = true;
              }
            }

            pStreamReader.Close();
            pStreamWriter.Flush();
            pStreamWriter.Close();
          }

          if (!bDeleted) {
            sMessage = "Запись о задаче с таким идентификатором не найдена.";
            File.Delete(sTempFile);
          }
          else {
            result = true;
            //File.Delete(m_sFileName);
            if (!File.Exists(m_sFileName)) File.Move(sTempFile, m_sFileName);
            else File.Replace(sTempFile, m_sFileName, null, false);
          }
        }
      }
      catch (Exception ex) {
        CTool.ReportException(ex);
        sMessage = ex.Message;
      }

      return result;
    }

    /// <summary>
    /// Читает запись о задаче из файла планировщика
    /// </summary>
    /// <param name="sIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Сообщение в случае ошибки чтения и проч.</param>
    /// <returns>Строка задачи из файла Cron</returns>
    private string h_readRecord(string sIdentifier, out string sMessage)
    {
      sMessage = "";
      string result = String.Empty;
      bool bFound = false;

      try {
        lock (m_pLockObject) {
          using (StreamReader pStreamReader = new StreamReader(m_sFileName, Encoding.Default)) {
            string sLine;
            while ((sLine = pStreamReader.ReadLine()) != null) {
              if (sLine.IndexOf(sIdentifier) > -1) { // TODO: Переделать с использованием sLine.Split(' ')[6]
                result = sLine;
                bFound = true;
                break;
              }
            }

            pStreamReader.Close();
          }

          if (!bFound) {
            sMessage = "Запись о задаче с таким идентификатором не надена.";
          }

        }
      }
      catch (Exception ex) {
        CTool.ReportException(ex);
        sMessage = ex.Message;
      }

      return result;
    }

    /// <summary>
    /// Осуществляет поиск подходящих по условию задач в текстовом файле
    /// </summary>
    /// <param name="pTaskFilter">Условие поиска</param>
    /// <param name="sMessage">Сообщение в случае ошибки чтения и проч.</param>
    /// <returns>Список строк, удовлетворяющих условию поиска</returns>
    private List<string> h_searchRecord(CTaskFilter pTaskFilter, out string sMessage)
    {
      sMessage = "";
      List<string> result = new List<string>();

      try {
        lock (m_pLockObject) {
          using (StreamReader pStreamReader = new StreamReader(m_sFileName, Encoding.Default)) {
            string sLine;
            while ((sLine = pStreamReader.ReadLine()) != null) {
              if (pTaskFilter == null || true /* + Метод парсинга строки задачи и сравнения с условием */) { // TODO: Сделать обработку условия фильтрации (пока забираем всё)
                result.Add(sLine);
              }
            }

            pStreamReader.Close();
          }
        }
      }
      catch (Exception ex) {
        CTool.ReportException(ex);
        sMessage = ex.Message;
      }

      return result;
    }

    /// <summary>
    /// Осуществляет поиск подходящих по условию задач Cron в текстовом файле
    /// </summary>
    /// <param name="pTaskFilter">Условие поиска</param>
    /// <param name="sMessage">Сообщение в случае ошибки чтения и проч.</param>
    /// <returns>Количество строк, удовлетворяющих условию поиска</returns>
    private int h_searchRecordCount(CTaskFilter pTaskFilter, out string sMessage)
    {
      sMessage = "";
      int result = 0;

      try {
        lock (m_pLockObject) {
          using (StreamReader pStreamReader = new StreamReader(m_sFileName, Encoding.Default)) {
            string sLine;
            while ((sLine = pStreamReader.ReadLine()) != null) {
              if (pTaskFilter == null || true /* + Метод парсинга строки задачи и сравнения с условием */) { // TODO: Сделать обработку условия фильтрации (пока забираем всё)
                result++;
              }
            }

            pStreamReader.Close();
          }
        }
      }
      catch (Exception ex) {
        CTool.ReportException(ex);
        sMessage = ex.Message;
      }

      return result;
    }

    #endregion

  }
  
  #endregion
}
