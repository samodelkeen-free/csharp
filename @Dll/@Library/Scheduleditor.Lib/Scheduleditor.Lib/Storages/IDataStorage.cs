﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

using System.Collections.Generic;
using Scheduleditor.Lib.Classes;

#endregion

namespace Scheduleditor.Lib.Storages
{

  #region IDataStorage interface
  
  /// <summary>
  /// Предоставляет интерфейс для произвольных Хранилищ данных под задачи планировщика Cron
  /// </summary>
  internal interface IDataStorage
  {
    /// <summary>
    /// Создает/редактирует запись о задаче Cron в Хранилище
    /// </summary>
    /// <param name="pCronTask">Объект описания задачи</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Результат работы (true/false)</returns>
    bool WriteTask(CCronTask pCronTask, out string sMessage);

    /// <summary>
    /// Удаляет запись о задаче Cron из Хранилища
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Результат работы (true/false)</returns>
    bool DeleteTask(object pIdentifier, out string sMessage);

    /// <summary>
    /// Читает запись о задаче Cron из Хранилища
    /// </summary>
    /// <param name="pIdentifier">Идентификатор задачи в Хранилище</param>
    /// <param name="sMessage">Ответ Хранилища</param>
    /// <returns>Объект задачи Cron</returns>
    CCronTask ReadTask(object pIdentifier, out string sMessage);

    /// <summary>
    /// Поиск задач Cron в Хранилище по условию
    /// </summary>
    /// <param name="pFoundTaskList">Объект списка задач</param>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="sMessage">Сообщение о результатах операции поиска задач в Хранилище</param>
    /// <returns></returns>
    void SearchTask(List<CFoundTaskItem> pFoundTaskList, CTaskFilter pTaskFilter, out string sMessage);

    /// <summary>
    /// Получает количество задач Cron в Хранилище, соответствующих условию поиска
    /// </summary>
    /// <param name="pTaskFilter">Объект фильтра задач</param>
    /// <param name="sMessage">Сообщение о результатах операции подсчета задач в Хранилище</param>
    /// <returns>Количество задач в Хранилище, удовлетворяющих условию поиска</returns>
    int SearchTaskCount(CTaskFilter pTaskFilter, out string sMessage);

  }
  
  #endregion
}