﻿#region CopyRight
//This code was originally developed 2016-12-16 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Using

#endregion

using System.Drawing;
using System.Windows.Forms;
using Aist.Engine.Data.Structures;
using Scheduleditor.Lib.Classes;
using Scheduleditor.Lib.Config;

namespace Scheduleditor.Lib
{
  /// <summary>
  /// Инструментарий приложения
  /// </summary>
  public static class CScheduleditorLib
  {
    #region CScheduleditorLib public methods

    /// <summary>
    /// Возвращает объект типа DataGridView для таблицы редактирования списка интервалов запуска задачи
    /// </summary>
    /// <param name="pDbDoc">Объект БД</param>
    /// <param name="iSourceInterval">Идентификатор источника интервалов</param>
    public static CIntervalGridDescription GetIntervalGridDescription(CDbDoc pDbDoc, int iSourceInterval)
    {
      CIntervalGridDescription pGridDescription = new CIntervalGridDescription(pDbDoc);
      pGridDescription.Field = iSourceInterval;
      pGridDescription.GridColumn.Add(new CGridColumn
      {
        ColumnName = "Identifier",
        HeaderText = "Идентификатор",
        DataPropertyName = "Identifier",
        MinimumWidth = 90,
        CellForeColor = Color.Black,
        HeaderCellAlignment = DataGridViewContentAlignment.MiddleCenter,
        CellWrapMode = DataGridViewTriState.True,
        Visible = false
      });
      pGridDescription.GridColumn.Add(new CGridColumn
      {
        ColumnType = EGridColumnType.ColumnTypeCheck,
        ColumnName = "Begin",
        HeaderText = "Начало интервала",
        DataPropertyName = "Begin",
        MinimumWidth = 90,
        CellForeColor = Color.Black,
        HeaderCellAlignment = DataGridViewContentAlignment.MiddleCenter,
        CellWrapMode = DataGridViewTriState.True,
        Visible = true,
        EditFormClass = GetInterval(pDbDoc, iSourceInterval)
      });
      pGridDescription.GridColumn.Add(new CGridColumn
      {
        ColumnType = EGridColumnType.ColumnTypeCheck,
        ColumnName = "End",
        HeaderText = "Конец интервала",
        DataPropertyName = "End",
        MinimumWidth = 90,
        CellForeColor = Color.Black,
        HeaderCellAlignment = DataGridViewContentAlignment.MiddleCenter,
        CellWrapMode = DataGridViewTriState.True,
        Visible = true,
        EditFormClass = GetInterval(pDbDoc, iSourceInterval)
      });
      pGridDescription.GridColumn.Add(new CGridColumn
      {
        ColumnType = EGridColumnType.ColumnTypeInt,
        ColumnName = "Step",
        HeaderText = "Шаг повторения",
        DataPropertyName = "Step",
        MinimumWidth = 90,
        CellForeColor = Color.Black,
        HeaderCellAlignment = DataGridViewContentAlignment.MiddleCenter,
        CellWrapMode = DataGridViewTriState.True,
        Visible = true
      });
      pGridDescription.GridColumn.Add(new CGridColumn
      {
        ColumnName = "ErrorDescription",
        HeaderText = "Состояние",
        DataPropertyName = "ErrorDescription",
        MinimumWidth = 90,
        CellForeColor = Color.Gray,
        HeaderCellAlignment = DataGridViewContentAlignment.MiddleCenter,
        CellWrapMode = DataGridViewTriState.True,
        Visible = true
      });
      pGridDescription.MaxPageSize = ((CScheduleditorConfig)pDbDoc.AppConfig).AppParam.MaxPageSize;
      return pGridDescription;
    }

    /// <summary>
    /// Получает описание формы выбора интервалов выполнения задачи
    /// </summary>
    /// <param name="pDbDoc">Объект БД</param>
    /// <param name="iSourceInterval">Идентификатор источника интервалов</param>
    /// <returns>Результирующий список</returns>
    public static CSelectionFormCheckBox GetInterval(CDbDoc pDbDoc, int iSourceInterval)
    {
      CSelectionFormCheckBox result = new CSelectionFormCheckBox(pDbDoc);
      switch (iSourceInterval) {
        case CScheduleditorLibConst.SourceMinute:
          result.OnlyElementMode = true;
          result.SourceIdentifier = iSourceInterval;
          result.FormName = "Выбор минуты";
          break;
        case CScheduleditorLibConst.SourceHour:
          result.OnlyElementMode = true;
          result.SourceIdentifier = iSourceInterval;
          result.FormName = "Выбор часа";
          break;
        case CScheduleditorLibConst.SourceMonthDay:
          result.OnlyElementMode = true;
          result.SourceIdentifier = iSourceInterval;
          result.FormName = "Выбор дня месяца";
          break;
        case CScheduleditorLibConst.SourceMonth:
          result.OnlyElementMode = true;
          result.SourceIdentifier = iSourceInterval;
          result.FormName = "Выбор месяца";
          break;
        case CScheduleditorLibConst.SourceWeek:
          result.OnlyElementMode = true;
          result.SourceIdentifier = iSourceInterval;
          result.FormName = "Выбор дня недели";
          break;
        default:
          break;
      }
      return result;
    }

    #endregion

    #region CScheduleditorLib public properties

    #endregion

  }
}
