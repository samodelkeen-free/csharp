﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

using System.Collections.Generic;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Класс элемента для хранения информации о поле Cron
  /// </summary>
  public class CCronField : List<CCronFieldInterval>
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CCronField()
    {
      Separate = true;
    }

    /// <summary>
    /// Флаг использования значений и поддиапазонов
    /// </summary>
    public bool Separate
    {
      get;
      set;
    }

  }
}