﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Класс элемента для хранения информации о временнОм интервале Cron
  /// </summary>
  public class CCronFieldInterval
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CCronFieldInterval()
    {

    }

    /// <summary>
    /// Идентификатор записи в Хранилище
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }

    /// <summary>
    /// Начало интервала
    /// </summary>
    public int? Begin
    {
      get;
      set;
    }
    /// <summary>
    /// Конец интервала
    /// </summary>
    public int? End
    {
      get;
      set;
    }
    /// <summary>
    /// Шаг выполнения задачи в единицах интервала
    /// </summary>
    public int? Step
    {
      get;
      set;
    }

  }
}