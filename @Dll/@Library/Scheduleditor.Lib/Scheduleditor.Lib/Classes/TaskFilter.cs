﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

using System;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Класс фильтра для поиска задачи в Хранилище
  /// </summary>
  public class CTaskFilter
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CTaskFilter()
    {

    }

    /// <summary>
    /// Идентификатор задачи
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование задачи
    /// </summary>
    public string Name
    {
      get;
      set;
    }
    /// <summary>
    /// Время запуска (начало интервала)
    /// </summary>
    public DateTime? StartTimeFrom
    {
      get;
      set;
    }
    /// <summary>
    /// Время запуска (конец интервала)
    /// </summary>
    public DateTime? StartTimeTo
    {
      get;
      set;
    }
    /// <summary>
    /// Продолжительность выполнения (начало интервала)
    /// </summary>
    public int DurationFrom
    {
      get;
      set;
    }
    /// <summary>
    /// Продолжительность выполнения (конец интервала)
    /// </summary>
    public int DurationTo
    {
      get;
      set;
    }

  }
}