﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

using Aist.Engine.Data.Structures;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Описание элемента списка объекта типа DataGridView для отображения временнЫх интервалов планировщика
  /// </summary>
  public class CIntervalGridItem : CGridItem
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CIntervalGridItem() : base()
    {
      Begin = new CSelectionProperty(null, "");
      End = new CSelectionProperty(null, "");
    }

    /// <summary>
    /// Идентификатор записи в БД
    /// </summary>
    override public object Identifier
    {
      get { return m_pIdentifier; }
      set { m_pIdentifier = value; }  
    }
	
    /// <summary>
    /// Начало периода
    /// </summary>
    public CSelectionProperty Begin
    {
      get;
      set;
    }
	
    /// <summary>
    /// Конец периода
    /// </summary>
    public CSelectionProperty End
    {
      get;
      set;
    }

    /// <summary>
    /// Шаг выполнения задачи
    /// </summary>
    public int? Step
    {
      get;
      set;
    }

  }
}
