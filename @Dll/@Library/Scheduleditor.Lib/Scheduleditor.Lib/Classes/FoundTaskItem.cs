﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

using System;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Класс элемента для хранения информации о задаче, найденной в Хранилище
  /// </summary>
  public class CFoundTaskItem
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CFoundTaskItem()
    {
	    
    }

    /// <summary>
    /// Конструктор класса по строковому представлению задачи
    /// </summary>
    public CFoundTaskItem(string sTask)
    {
      string[] arCronTask = sTask.Split(new char[] { ' ' }, 8);

      Period = arCronTask[0] + " " + arCronTask[1] + " " + arCronTask[2] + " " + arCronTask[3] + " " + arCronTask[4];
      Duration = Int32.Parse(arCronTask[5]);
      Identifier = arCronTask[6];
      Name = arCronTask[7];

      // EVV: TODO: Возможно, стоит перенести это в геттеры соответствующих свойств?
      NextStartTime = CCronTask.GetNextStartTime(sTask);
      //LastStartTime = CCronTask.GetLastStartTime(sTask);
      Description = CCronTask.GetDescription(sTask);
    }

    /// <summary>
    /// Номер по порядку в списке на отображение
    /// </summary>
    public int RowRank
    {
      get;
      set;
    }
    /// <summary>
    /// Идентификатор задачи
    /// </summary>
    public object Identifier
    { 
      get;
      set;
    }
    /// <summary>
    /// Наименование задачи
    /// </summary>
    public string Name
    {
      get;
      set;
    }
    /// <summary>
    /// Описание задачи
    /// </summary>
    public string Description
    {
      get;
      set;
    }
    /// <summary>
    /// Периодичность запуска (строка задачи в Cron?)
    /// </summary>
    public string Period
    {
      get;
      set;
    }
    /// <summary>
    /// Максимальная продолжительность выполнения (минуты)
    /// </summary>
    public int Duration
    {
      get;
      set;
    }
    /// <summary>
    /// Дата-время следующего запуска
    /// </summary>
    public DateTime? NextStartTime
    {
      get;
      set;
    }
    /// <summary>
    /// Дата-время последнего запуска
    /// </summary>
    public DateTime? LastStartTime
    {
      get;
      set;
    }
    /// <summary>
    /// Дата-время модификации записи о задаче в Хранилище
    /// </summary>
    public DateTime? ModifyDate
    {
      get;
      set;
    }

  }
}