﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings

using System;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Класс элемента для хранения информации о задаче в формате Cron
  /// </summary>
  public class CCronTask
  {
    /// <summary>
    /// Конструктор класса
    /// </summary>
    public CCronTask()
    {
      Period = new CCronField[5];
      Period[0] = new CCronField();
      Period[1] = new CCronField();
      Period[2] = new CCronField();
      Period[3] = new CCronField();
      Period[4] = new CCronField();
      Identifier = Guid.NewGuid();
    }

    /// <summary>
    /// Конструктор класса по строке задачи из файла Cron
    /// </summary>
    /// <param name="sCronTask">Строковое представление задачи</param>
    public CCronTask(string sCronTask)
    {
      string[] arCronTask = sCronTask.Split(new char[] {' '}, 8);

      Period = new CCronField[5];
      Period[0] = new CCronField();
      Period[1] = new CCronField();
      Period[2] = new CCronField();
      Period[3] = new CCronField();
      Period[4] = new CCronField();

      for (int iField = 0; iField < 5; ++iField) {
        string[] arCronField = arCronTask[iField].Split(',');
        foreach (string sCronInterval in arCronField) {
          if (sCronInterval.StartsWith("*") || sCronInterval.IndexOf('-') > 0) { // Если * или интервал
            string[] arCronInterval = sCronInterval.Split('/'); // Получаем интервал и шаг
            if (arCronInterval[0] == "*") {
              Period[iField].Add(new CCronFieldInterval { Step = (arCronInterval.Length > 1 ? Int32.Parse(arCronInterval[1]) : (int?)null) });
              Period[iField].Separate = false;
            } else {
              string[] arCronIntervalEdge = arCronInterval[0].Split('-'); // Получаем границы интервала
              switch (iField) {
                case 2:
                case 3:
                case 4:
                  Period[iField].Add(new CCronFieldInterval {
                    Begin = Int32.Parse(arCronIntervalEdge[0]) - 1,
                    End = Int32.Parse(arCronIntervalEdge[1]) - 1,
                    Step = (arCronInterval.Length > 1 ? Int32.Parse(arCronInterval[1]) : (int?)null),
                    Identifier = Guid.NewGuid()
                  });
                  break;
                default:
                  Period[iField].Add(new CCronFieldInterval {
                    Begin = Int32.Parse(arCronIntervalEdge[0]),
                    End = Int32.Parse(arCronIntervalEdge[1]),
                    Step = (arCronInterval.Length > 1 ? Int32.Parse(arCronInterval[1]) : (int?)null),
                    Identifier = Guid.NewGuid()
                  });
                  break;
              }
            }
          } else { // Если единичное значение
            switch (iField) {
              case 2:
              case 3:
              case 4:
                Period[iField].Add(new CCronFieldInterval {Begin = Int32.Parse(sCronInterval) - 1});
                break;
              default:
                Period[iField].Add(new CCronFieldInterval {Begin = Int32.Parse(sCronInterval)});
                break;
            }
          }
        }
      }

      Duration = Int32.Parse(arCronTask[5]);
      Identifier = arCronTask[6];
      Name = arCronTask[7];
    }

    /// <summary>
    /// Идентификатор задачи
    /// </summary>
    public object Identifier
    {
      get;
      set;
    }
    /// <summary>
    /// Наименование задачи
    /// </summary>
    public string Name
    {
      get;
      set;
    }
    /// <summary>
    /// Периодичность запуска (массив из пяти элементов: минуты, часы, дни месяца, месяцы, дни недели)
    /// </summary>
    public CCronField[] Period
    {
      get;
      set;
    }
    /// <summary>
    /// Максимальная продолжительность выполнения (минуты)
    /// </summary>
    public int Duration
    {
      get;
      set;
    }

    /// <summary>
    /// Признак модификации задачи
    /// </summary>
    public bool Modified
    {
      get;
      set;
    }

    /// <summary>
    /// Строковое представление задачи Cron
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      // Пример строки задачи Cron:
      //return "* * * * * 10 92eda528-d0ca-466d-9e27-ddb8fcf565db Тестовая задача";
      string sPeriod = String.Empty;
      for (int iField = 0; iField < 5; ++iField) {
        string sField = String.Empty;
        foreach (CCronFieldInterval pCronFieldInterval in Period[iField]) {
          if (pCronFieldInterval.End != null) // Это интервалы:
            switch (iField) {
              case 2:
              case 3:
              case 4:
                sField += (!String.IsNullOrEmpty(sField) ? "," : String.Empty) + (pCronFieldInterval.Begin + 1) + "-" + (pCronFieldInterval.End + 1) + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step : String.Empty);
                break;
              default:
                sField += (!String.IsNullOrEmpty(sField) ? "," : String.Empty) + pCronFieldInterval.Begin + "-" + pCronFieldInterval.End + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step : String.Empty);
                break;
            }
          else { // Это не интервалы:
            if (pCronFieldInterval.Begin == null) { // Это '*' в Cron
              sField = "*" + (pCronFieldInterval.Step != null ? "/" + pCronFieldInterval.Step : String.Empty);
            }
            else { // Это единичные временнЫе позиции:
              switch (iField) {
                case 2:
                case 3:
                case 4:
                  sField += (!String.IsNullOrEmpty(sField) ? "," : String.Empty) + (pCronFieldInterval.Begin + 1);
                  break;
                default:
                  sField += (!String.IsNullOrEmpty(sField) ? "," : String.Empty) + pCronFieldInterval.Begin;
                  break;
              }
            }
          }
        }
        sPeriod += (!String.IsNullOrEmpty(sField) ? sField : "*") + " ";
      }
      string sDuration = Duration.ToString();
      string sIdentifier = Identifier.ToString();
      string sName = !String.IsNullOrEmpty(Name) ? Name : "Без названия";
      return string.Format(CScheduleditorLibConst.TEMPLATE_CRON_TASK, sPeriod.Trim(), sDuration, sIdentifier, sName);
    }

    /// <summary>
    /// Возвращает описание задачи для объекта задачи Cron
    /// </summary>
    /// <param name="pCronTask"></param>
    /// <returns></returns>
    public static string GetDescription(CCronTask pCronTask)
    {
      // Поля для текущей даты-времени
      DateTime dtNow = DateTime.Now;
      int iMinute = dtNow.Minute;
      int iHour = dtNow.Hour;
      int iDay = dtNow.Day - 1;
      int iMonth = dtNow.Month - 1;
      int iWeek = (int)dtNow.DayOfWeek > 0 ? (int)dtNow.DayOfWeek - 1 : 6;
      int iYear = dtNow.Year;

      int[] arFieldArray = { iMinute + 1, iHour, iDay, iMonth, iYear };
      h_processField(pCronTask, 3, arFieldArray);
      /*
      // Поля для следующего запуска
      int iNextMinute;
      int iNextHour;
      int iNextDay;
      int iNextMonth;
      int iNextWeek;

      // Проверяется соответствие интервалам для текущей даты и вычисляются поля для следующего запуска:
      bool isMinuteEqual = h_isFieldEqual(pCronTask, 0, iMinute, out iNextMinute);
      bool isHourEqual = h_isFieldEqual(pCronTask, 1, iHour, out iNextHour);
      bool isDayEqual = h_isFieldEqual(pCronTask, 2, iDay, out iNextDay);
      bool isMonthEqual = h_isFieldEqual(pCronTask, 3, iMonth, out iNextMonth);
      bool isWeekEqual = h_isFieldEqual(pCronTask, 4, iWeek, out iNextWeek);

      // Поля для ближайшего запуска
      int iNearestMinute;
      int iNearestHour;
      int iNearestDay;
      int iNearestMonth;
      int iNearestWeek;

      // Вычисляется дата-время ближайшего запуска:
      if (h_isFieldEqual(pCronTask, 3, iMonth, out iNextMonth)) { // Если совпал месяц
        iNearestMonth = iMonth;
        
      } else { // Если месяц не совпал
        iNearestMinute = h_isFieldEqual(pCronTask, 0, 0, out iNextMinute) ? 0 : iNextMinute;
        iNearestHour = h_isFieldEqual(pCronTask, 1, 0, out iNextHour) ? 0 : iNextHour;
        iNearestDay = h_isFieldEqual(pCronTask, 2, 0, out iNextDay) ? 0 : iNextDay;
        iNearestMonth = iNextMonth;
      }

      iNearestMinute = iNextMinute;
      iNearestHour = iNearestMinute < iMinute || !isHourEqual ? iNextHour : iHour;
      iNearestDay = iNearestHour < iHour || !isDayEqual ? iNextDay : iDay;
      iNearestMonth = iNearestDay < iDay || !isMonthEqual ? iNextMonth : iMonth;
      */
      string result = string.Empty;
      /*
      result += "(" + isMinuteEqual + " " + iNextMinute + ") ";
      result += "(" + isHourEqual + " " + iNextHour + ") ";
      result += "(" + isDayEqual + " " + (iNextDay + 1) + ") ";
      result += "(" + isMonthEqual + " " + CScheduleditorLibConst.MonthArray[iNextMonth] + ") ";
      result += "(" + isWeekEqual + " " + CScheduleditorLibConst.WeekArray[iNextWeek] + ") ";
      result = string.Format("{0} {1} {2} {3}", iNearestMinute, iNearestHour, iNearestDay + 1, CScheduleditorLibConst.MonthArray[iNearestMonth]);
      */
      result = string.Format("{0} {1} {2} {3} {4}", arFieldArray[0], arFieldArray[1], arFieldArray[2] + 1, CScheduleditorLibConst.MonthArray[arFieldArray[3]], arFieldArray[4]);
      return result;
    }

    /// <summary>
    /// Возвращает описание задачи для строки задачи Cron
    /// </summary>
    /// <param name="sCronTask"></param>
    /// <returns></returns>
    public static string GetDescription(string sCronTask)
    {
      return GetDescription(new CCronTask(sCronTask));
    }

    /// <summary>
    /// Возвращает время следующего запуска для объекта задачи Cron
    /// </summary>
    /// <param name="pCronTask"></param>
    /// <returns></returns>
    public static DateTime GetNextStartTime(CCronTask pCronTask)
    {
      DateTime dtNow = DateTime.Now;
      int iMinute = dtNow.Minute;
      int iHour = dtNow.Hour;
      int iDay = dtNow.Day - 1;
      int iMonth = dtNow.Month - 1;
      int iWeek = (int)dtNow.DayOfWeek > 0 ? (int)dtNow.DayOfWeek - 1 : 6;
      int iYear = dtNow.Year;

      int[] arFieldArray = { iMinute + 1, iHour, iDay, iMonth, iYear };
      h_processField(pCronTask, 3, arFieldArray);

      DateTime result = new DateTime(arFieldArray[4], arFieldArray[3] + 1, arFieldArray[2] + 1, arFieldArray[1], arFieldArray[0], 0, 0, DateTimeKind.Local);
      return result;
    }

    /// <summary>
    /// Возвращает время следующего запуска для строки задачи Cron
    /// </summary>
    /// <param name="sCronTask"></param>
    /// <returns></returns>
    public static DateTime GetNextStartTime(string sCronTask)
    {
      return GetNextStartTime(new CCronTask(sCronTask));
    }

    /// <summary>
    /// Рекурсивная обработка полей таймера задачи. Поиск полей ближайшего по времени запуска. TODO: Чистая магия - надо оптимизировать!
    /// </summary>
    /// <param name="pCronTask">Объект задачи Cron</param>
    /// <param name="iField">Номер поля Cron</param>
    /// <param name="arFieldArray">Массив полей Cron</param>
    private static void h_processField(CCronTask pCronTask, int iField, int[] arFieldArray)
    {
      int iNextValue;
      int iPrevValue = arFieldArray[iField];
      if (h_isFieldEqual(pCronTask, iField, iPrevValue, out iNextValue)) {    // Проверяем соответствие текущего поля даты планировщику и возвращаем значение этого поля для следующего запуска:
        if (iField > 0) h_processField(pCronTask, iField - 1, arFieldArray);  // Если совпало с планировщиком, то переходим к обработке предыдущего поля даты
      } else {                                                                // Если поле даты не совпало с соответствующим полем планировщика:
        for (int ii = 0; ii < iField; ++ii) arFieldArray[ii] = 0;             // Обнуляем все поля слева от обрабатываемого поля
        arFieldArray[iField] = iNextValue;                                    // Присваиваем обрабатываемому полю значение времени следующего запуска
        if (iPrevValue > iNextValue) {                                        // Если значение поля уменьшилось, т.е. повторить в текущем родительском периоде уже нельзя...  
          arFieldArray[iField + 1] ++;                                        // ...то увеличиваем поле родительского периода на единицу
          if (iField < 4) {                                                   // Проверяем выход за пределы массива полей Cron
            h_processField(pCronTask, iField + 1, arFieldArray);              // Запускаем обработку для увеличенного родительского периода
          }
        } else {
          if (iField > 0) h_processField(pCronTask, iField - 1, arFieldArray);// Восстанавливаем значения обнуленных дочерних периодов
        }
      }
    }

    /// <summary>
    /// Проверяет соответствие поля текущей даты полю задачи планировщика Cron TODO: Избавиться от циклов!
    /// </summary>
    /// <param name="pCronTask">Объект задачи Cron</param>
    /// <param name="iField">Поле задачи Cron</param>
    /// <param name="iFieldValue">Значение для проверки</param>
    /// <param name="iNextValue">Значение поля для следующего запланированного запуска задачи Cron</param>
    /// <returns>Результат проверки (true/false)</returns>
    private static bool h_isFieldEqual(CCronTask pCronTask, int iField, int iFieldValue, out int iNextValue)
    {
      // EVV: Значение iField = 4 (дни недели) - не обрабатывается пока. Вместо него поставил заглушку "годы":
      if (iField == 4) {
        iNextValue = iFieldValue;
        return true;
      }

      bool result = false;
      iNextValue = 99;
      int iEnd = 0;
      switch (iField) {
        case 0:
          iEnd = 59;
          break;
        case 1:
          iEnd = 23;
          break;
        case 2:
          iEnd = 30;
          break;
        case 3:
          iEnd = 11;
          break;
        case 4:
          iEnd = 6;
          break;
      }
      int iMinValue = iEnd;
      foreach (CCronFieldInterval pCronFieldInterval in pCronTask.Period[iField]) {
        if (pCronFieldInterval.End != null) { // Это интервалы:
          int iStep = pCronFieldInterval.Step != null ? (int) pCronFieldInterval.Step : 1;
          for (int ii = (int) pCronFieldInterval.Begin; ii <= pCronFieldInterval.End && ii <= iEnd; ii += iStep) {
            if (iFieldValue == ii) result = true;
            else if (ii > iFieldValue && iNextValue > ii) iNextValue = ii;
            if (iMinValue > ii) iMinValue = ii;
          }
          // Исключаем правую границу, если не попадает в делитель:
          //if (pCronFieldInterval.End > iFieldValue && iNextValue > pCronFieldInterval.End) iNextValue = (int)pCronFieldInterval.End;
        }
        else { // Это не интервалы:
          if (pCronFieldInterval.Begin == null) { // Это '*' в Cron
            int iStep = pCronFieldInterval.Step != null ? (int)pCronFieldInterval.Step : 1;
            for (int ii = 0; ii <= iEnd; ii += iStep) {
              if (iFieldValue == ii) result = true;
              else if (ii > iFieldValue && iNextValue > ii) iNextValue = ii;
              if (iMinValue > ii) iMinValue = ii;
            }
          }
          else { // Это единичные временнЫе позиции:
            if (iFieldValue == pCronFieldInterval.Begin) result = true;
            else if (pCronFieldInterval.Begin > iFieldValue && iNextValue > pCronFieldInterval.Begin) iNextValue = (int)pCronFieldInterval.Begin;
            if (iMinValue > pCronFieldInterval.Begin) iMinValue = (int)pCronFieldInterval.Begin;
          }
        }
      }
      if (iNextValue == 99) iNextValue = iMinValue;
      else
        if (iNextValue > iEnd) iNextValue -= (iEnd + 1);
      return result;
    }

  }
}