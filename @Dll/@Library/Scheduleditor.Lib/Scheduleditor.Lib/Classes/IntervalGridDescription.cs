﻿#region CopyRight
//This code was originally developed 2016-12-15 by Egorov V.V.
//(c) 2016 АО ГНИВЦ, Филиал в Сибирском федеральном округе
#endregion

#region Usings
using System.Collections.Generic;
using Aist.Engine.Data.Structures;

#endregion

namespace Scheduleditor.Lib.Classes
{
  /// <summary>
  /// Описание объекта типа DataGridView для таблицы интервалов запуска задачи
  /// </summary>
  public class CIntervalGridDescription : CGridDescription
  {
    /// <summary>
    /// Конструктор
    /// </summary>
    public CIntervalGridDescription(CDbDoc pDbDoc) : base(pDbDoc)
    {
      m_sEditFormClass = "Scheduleditor.Forms.CIntervalForm"; // EVV: Не будем здесь использовать отдельную форму для редактирования
      m_sIdentifyField = "Identifier";
      m_sFormName = "Интервалы запуска задачи";
      m_sTitle = "Пополнение и корректировка списка интервалов запуска задачи";
    }

    /// <summary>
    /// Номер поля, содержащего список интервалов для редактирования
    /// </summary>
    public int Field
    {
      get;
      set;
    }

    /// <summary>
    /// Описание колонок объекта типа DataGridView
    /// </summary>
    override public List<CGridColumn> GridColumn
    {
      get;
      set;
    }
    /// <summary>
    /// Название поля-идентификатора в БД
    /// </summary>
    override public string IdentifyField
    {
      get { return m_sIdentifyField; }
      set { m_sIdentifyField = value; }
    }
    /// <summary>
    /// Название класса формы редактирования элемента
    /// </summary>
    override public string EditFormClass
    {
      get { return m_sEditFormClass; }
      set { m_sEditFormClass = value; } 
    }

    /// <summary>
    /// Добавление элемента в список
    /// </summary>
    /// <param></param>
    override public int AddItem()
    {
      CIntervalGridItem pGridItem = new CIntervalGridItem { Saved = false, ErrorDescription = "Пустая запись" };
      Add(pGridItem);
      return IndexOf(pGridItem);
    }

    /*
    /// <summary>
    /// Запись несохраненных элементов в БД
    /// </summary>
    /// <param></param>
    override public int SaveList()
    {
      int ii = 0;
      CIntervalGridItem pGridItem;
      while ((pGridItem = (CIntervalGridItem)FindNotSaved()) != null)
      {
        if (DbEdit(pGridItem)) {
          ii++;
        }
        else {
          Remove(pGridItem);
        }
      }
      return ii;
    }
    */

    /// <summary>
    /// Метод для выборки данных
    /// </summary>
    /// <param name="sFilter">Фильтр для выборки</param>
    /// <param name="iSize">Размер страницы (в записях)</param>
    /// <param name="iPage">Номер страницы в выборке (возвращаемый)</param>
    /// <returns>Общее количество страниц в выборке</returns>
    override public int DbSelect(string sFilter, int iSize, ref int iPage)
    {
      // Вызов метода выборки данных из Хранилища:
      Filter = sFilter;
      if (!string.IsNullOrEmpty(sFilter)) {
        m_sSqlFilter = (m_pDbDoc as CDbDoc).GetIntervalFilter(sFilter);
      }
      else {
        m_sSqlFilter = string.Empty;
      }
      m_iDbCount = (m_pDbDoc as CDbDoc).GetIntervalCount(m_sSqlFilter);
      m_iPageSize = iSize;
      while (((1 + iSize * (iPage - 1)) > m_iDbCount) && iPage > 1) {
        iPage--;
      }
      (m_pDbDoc as CDbDoc).IntervalListFill(this, m_sSqlFilter, 1 + iSize * (iPage - 1), iSize);
      return m_iDbCount / iSize + (((m_iDbCount % iSize) > 0) ? 1 : 0);
    }

    /// <summary>
    /// Метод для поиска по БД
    /// </summary>
    /// <param name="sSearch">Значение для поиска</param>
    /// <returns></returns>
    override public void DbSearch(string sSearch)
    {
      if (!string.IsNullOrEmpty(sSearch)) {
        sSearch = (m_pDbDoc as CDbDoc).GetIntervalSearch(sSearch);
      }
      else {
        sSearch = string.Empty;
      }
      (m_pDbDoc as CDbDoc).IntervalIdentifierListFill(FoundIdentifierList, m_sSqlFilter, sSearch);
    }

    /// <summary>
    /// Метод для добавления и редактирования данных
    /// </summary>
    /// <param name="pRecord">Объект строки</param>
    override public bool DbEdit(object pRecord)
    {
      (m_pDbDoc as CDbDoc).EditInterval((CIntervalGridItem)pRecord, Field);
      if (((CGridItem)pRecord).Saved) m_bChanged = true;
      return ((CGridItem)pRecord).Saved;
    }

    /// <summary>
    /// Метод для удаления данных
    /// </summary>
    /// <param name="pRecord">Объект строки</param>
    /// <returns>Флаг успешности операции удаления</returns>
    override public bool DbDelete(object pRecord)
    {
      // Вызов метода удаления элемента из БД:
      string sErrorMessage;
      bool result = (m_pDbDoc as CDbDoc).DeleteInterval(((CGridItem)pRecord).Identifier, Field, out sErrorMessage);
      ((CGridItem) pRecord).ErrorDescription = sErrorMessage;
      if (result) m_bChanged = true;
      return result;
    }

    /// <summary>
    /// Метод для поиска элемента в БД (используется для проверки наличия элемента в списках выбора)
    /// </summary>
    /// <param name="pIdentifier">Идентификатор элемента</param>
    /// <param name="pValue">Значение элемента</param>
    /// <param name="pSelectionFormList">Объект списка значений</param>
    /// <returns>Идентификатор элемента списка</returns>
    override public object DbSearch(object pIdentifier, object pValue, object pSelectionFormList)
    {
      if (/*pIdentifier == null*/true) {
        ((CSelectionFormCheckBox) pSelectionFormList).DbSelect("");
        return ((CSelectionFormCheckBox) pSelectionFormList).FindElement(pValue.ToString());
      }
      /*return null;*/
    }

  }
}
