﻿Тестовая задача "Редактор расписания задач"

Редактор расписания задач позволяет задавать расписание, хранить расписание в текстовом формате. 
Формат хранения расписания задач может быть реализован на базе текстового файла формата cron 
(см. http://www.nncron.ru/nncronlt/help/RU/working/cron-format.htm), 
либо своего формата, расширенных обязательными полями:
- наименование задачи;
- максимальная продолжительность запуска (в минутах).

Взаимодействие с системой-потребителем реализуется двумя способами:
- добавление задания (наименование для отображения, уникальный код);
- получение уникального кода ближайшего задания к исполнению.

В приложении должны быть реализованы две функции:
- пользовательский оконный интерфейс редактора расписания
	функции просмотра, добавления, удаления, изменения расписания заданий; функции работы с файлом (считывание и сохранение)
	Обязательные реквизиты пользовательского интерфейса можно взять с стандартного планировщика задач Windows 
	(https://technet.microsoft.com/en-us/library/cc721931(v=ws.11).aspx)
 
- интерфейс тестовой функции, позволяющий проверить работу разработанных классов:
	Входные данные: "текущие" дата и время.
	Выходные данные: дата и время запуска ближайшего задания.
	
Приложение должно быть разработано на C# с использованием объектно-ориентированного подхода. 
Для  демонстрации должно быть подготовлено тестовое расписание и программа тестирования в неформальном виде 
(перечень проверок для предложенного тестового расписания). 
Бизнес-логика должна быть реализована самостоятельно без использования существующих библиотек.

TODO:
1. Фильтрация списка задач по условиям.
2. Проверка корректности заполнения временнЫх интервалов, проверка пересечений интервалов, 
исключение дублирования единичных значений и интервалов в полях Cron.
3. Деактивация ненужных кнопок и полей ввода в форме создания списка интервалов.
4. В строке статуса формы редактирования задачи отображать человекочитаемое описание периодичности задачи.
5. Избавиться о циклов в приватном методе проверки поля Cron и возврата следующего значения поля.
6. Придумать, как считать ближайший запуск с учетом дней недели (пока не учитываются в расчете).
7. 